// ==UserScript==
// @name        Kindroid - Test bundle
// @description New features for Kindroid
// @namespace   https://gitlab.com/breatfr
// @match       https://kindroid.ai/*
// @version     1.0.0
// @homepageURL https://gitlab.com/breatfr/kindroid
// @downloadURL https://gitlab.com/breatfr/kindroid/-/raw/main/js/test_bundle.user.js
// @updateURL   https://gitlab.com/breatfr/kindroid/-/raw/main/js/test_bundle.user.js
// @supportURL  https://discord.com/channels/1116127115574779905/1195415564101886102
// @author      BreatFR
// @copyright   2023, BreatFR (https://breat.fr)
// @grant       none
// @icon        https://gitlab.com/breatfr/kindroid/-/raw/main/images/icon_kindroid.png
// @license     BY-NC-ND; https://creativecommons.org/licenses/by-nc-nd/4.0/

// @require     https://gitlab.com/breatfr/Kindroid/-/raw/main/js/kindroid_chat_page_new_features.user.js
// @require     https://gitlab.com/breatfr/Kindroid/-/raw/main/js/kindroid_selfies_page_new_features.user.js
// @require     https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_notepad.user.js
// @require     https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_tv.user.js
// ==/UserScript==
