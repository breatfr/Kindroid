// ==UserScript==
// @name        Kindroid - TV
// @description Kindroid TV feature
// @namespace   https://gitlab.com/breatfr
// @match       https://kindroid.ai/home*
// @match       https://kindroid.ai/groupchat*
// @match       https://kindroid.ai/call*
// @version     2.0.3
// @homepageURL https://gitlab.com/breatfr/kindroid
// @downloadURL https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_tv.user.js
// @updateURL   https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_tv.user.js
// @author      BreatFR
// @copyright   2024, BreatFR (https://breat.fr)
// @grant       none
// @icon        https://gitlab.com/breatfr/kindroid/-/raw/main/images/icon_kindroid.png
// @license     BY-NC-ND; https://creativecommons.org/licenses/by-nc-nd/4.0/
// ==/UserScript==

(function() {
    'use strict';

    console.log("Kindroid TV script running");

    // Design
    var style = document.createElement('style');
    style.textContent = `
        /* Hide icons at the right of header */
        .css-x3odei > div:nth-of-type(2),
        .css-z0osps > div > div:nth-of-type(2) {
          display: none;
          height: 0;
          width: 0;
        }

        #tv-script-button {
          background-color: transparent;
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'%3E%3Crect x='32' y='96' width='448' height='272' rx='32.14' ry='32.14' fill='none' stroke='%23cbcbcb' stroke-linejoin='round' stroke-width='32'/%3E%3Cpath stroke='%23cbcbcb' stroke-linecap='round' stroke-miterlimit='10' stroke-width='32' d='M128 416h256'/%3E%3C/svg%3E");
          background-repeat: no-repeat;
          background-position: center center;
          border: none;
          cursor: pointer;
          height: 40px;
          position: fixed;
          top: 10px;
          -o-transition: opacity 0.3s ease;
          -moz-transition: opacity 0.3s ease;
          -ms-transition: opacity 0.3s ease;
          -webkit-transition: opacity 0.3s ease;
          transition: opacity 0.3s ease;
          width: 40px;
        }

        #tv-script-button:hover {
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'%3E%3Crect x='32' y='96' width='448' height='272' rx='32.14' ry='32.14' fill='none' stroke='url(%23A)' stroke-linejoin='round' stroke-width='32'/%3E%3Cpath stroke='url(%23A)' stroke-linecap='round' stroke-miterlimit='10' stroke-width='32' d='M128 416h256'/%3E%3Cdefs%3E%3ClinearGradient id='A' x1='1.6' y1='25.2' x2='29.2' y2='0' gradientUnits='userSpaceOnUse'%3E%3Cstop offset='.208' stop-color='%238b6dff'/%3E%3Cstop offset='1' stop-color='%23fe8484'/%3E%3C/linearGradient%3E%3C/defs%3E%3C/svg%3E");
        }

        .tv-block {
          background-color: black;
          border: 10px solid black;
          border-bottom: 0;
          border-radius: 1em;
          overflow: hidden;
          position: fixed;
          right: 16px;
          top: 10%;
          z-index: 10000;
        }

        .tv-iframe {
          border: none;
          width: 100%;
        }

        .tv-block-footer {
          align-items: center;
          background-color: black;
          cursor: move;
          display: flex;
          height: 30px;
          justify-content: center;
        }

        @media screen and (max-width: 1400px), (orientation: portrait) {
          #tv-script-button {
            height: 36px;
            top: 5px;
            width: 36px;
          }
        }
    `;
    document.head.appendChild(style);

    // Créer le bouton
    const tvButton = document.createElement('button');
    tvButton.id = 'tv-script-button';
    tvButton.ariaLabel = 'Kindroid TV';
    tvButton.title = 'Kindroid TV';
    tvButton.dataset.priority = 2;
    document.body.appendChild(tvButton);

    // Fonction pour ajouter une icône
    function addIcon(iconId, priority) {
        const isMobile = window.matchMedia("(max-width: 1400px), (orientation: portrait)").matches;
        // Récupérer le bouton existant par son ID
        const iconButton = document.getElementById(iconId);
        if (!iconButton) {
            console.error(`Le bouton avec l'ID ${iconId} n'existe pas.`);
            return;
        }

        // Créer ou mettre à jour l'icône dans un tableau des priorités
        let icons = [];

        // Récupérer tous les boutons existants
        const buttons = document.querySelectorAll('button[id]');
        buttons.forEach(button => {
            const buttonId = button.id;
            const buttonPriority = parseInt(button.dataset.priority) || 0; // Récupère la priorité ou 0 par défaut
            icons.push({ id: buttonId, priority: buttonPriority });
        });

        // Vérifier si l'icône actuelle est déjà dans le tableau, et la mettre à jour
        const existingIconIndex = icons.findIndex(icon => icon.id === iconId);
        if (existingIconIndex !== -1) {
            icons[existingIconIndex].priority = priority; // Met à jour la priorité si déjà existante
        } else {
            icons.push({ id: iconId, priority: priority }); // Sinon, ajouter l'icône
        }

        // Trier les icônes par priorité croissante (priorité 1 est la plus à droite)
        icons.sort((a, b) => a.priority - b.priority);

        // Calculer la position basée sur l'ordre trié
        let occupiedSpace = isMobile ? -11 * window.innerWidth / 100 : 16; // 11% de la largeur de l'écran pour mobile
        const spacing = isMobile ? 10 : 20; // Espacement de 10px pour mobile, 20px pour desktop
        console.log("Initial occupiedSpace: ", occupiedSpace);

        icons.forEach(icon => {
            const button = document.getElementById(icon.id);
            if (button) {
                console.log(`Placement pour ${icon.id} avant ajustement: ${occupiedSpace}px`);
                button.style.right = isMobile ? `${Math.abs(occupiedSpace)}px` : `${occupiedSpace}px`; // Positionner le bouton
                occupiedSpace += 40 + spacing; // 40px pour l'icône + espacement dynamique
                console.log(`OccupiedSpace après ${icon.id}: ${occupiedSpace}px`);
            }
        });
    }
    addIcon("tv-script-button", 2);

    let isPlaying = false;
    let tvBlock;

    const lastUrl = localStorage.getItem('lastEmbedUrl') || '';
    const videoHistory = JSON.parse(localStorage.getItem('videoHistory')) || [];

    tvButton.addEventListener('click', () => {
        if (isPlaying) {
            if (tvBlock) {
                tvBlock.remove();
                isPlaying = false;
            }
        } else {
            // Créer un prompt pour l'URL
            const embedUrl = prompt(`Enter the embed URL (last: ${lastUrl}):`, lastUrl);
            if (!embedUrl) return; // Si l'utilisateur annule, ne rien faire

            // Créer un prompt pour la taille
            let width, height;

            // Vérifier si l'utilisateur est sur mobile
            if (window.matchMedia("(max-width: 1400px), (orientation: portrait)").matches) {
                // Définir les dimensions automatiques sur mobile
                width = window.innerWidth * 0.9; // 90% de la largeur de l'écran
                height = width * 9 / 16; // Respecter un ratio 16:9
            } else {
                // Créer un prompt pour la taille sur les grands écrans
                const size = prompt("Enter the size (width x height), e.g., 640x360:", "640x360");
                if (!size) return; // Si l'utilisateur annule, ne rien faire

                [width, height] = size.split('x').map(Number);

                // Vérifier si la taille dépasse celle de l'écran
                const screenWidth = window.innerWidth;
                const screenHeight = window.innerHeight;

                if (width > screenWidth || height > screenHeight) {
                    alert(`The specified dimension exceeds the screen size.\nYour screen: ${screenWidth}x${screenHeight}\nDimensions: ${width}x${height}\nPlease enter a smaller size.`);
                    return; // Ne pas continuer si les dimensions sont trop grandes
                }
            }

            // Mettre à jour l'historique
            if (!videoHistory.includes(embedUrl)) {
                videoHistory.unshift(embedUrl);
                if (videoHistory.length > 5) {
                    videoHistory.pop();
                }
                localStorage.setItem('videoHistory', JSON.stringify(videoHistory));
            }
            localStorage.setItem('lastEmbedUrl', embedUrl);

            // Créer le div de la télévision
            tvBlock = document.createElement('div');
            tvBlock.className = 'tv-block';
            tvBlock.style.height = `${height + 45}px`;
            tvBlock.style.width = `${width}px`;

            // Créer l'iframe pour la vidéo
            const iframe = document.createElement('iframe');
            iframe.className = 'tv-iframe';
            iframe.src = embedUrl;
            iframe.height = `${height}px`;
            iframe.allowFullscreen = true;

            // Ajouter l'iframe au div de la télévision
            tvBlock.appendChild(iframe);
            document.body.appendChild(tvBlock);
            isPlaying = true;

            // Ajout de la barre en bas (pour le texte)
            let footerDiv = document.createElement("div");
            footerDiv.className = 'tv-block-footer';
            tvBlock.appendChild(footerDiv);

            // Création de l'élément de texte
            let tvBlockFooter = document.createElement("span");
            tvBlockFooter.textContent = "Kindroid TV";
            tvBlockFooter.style.backgroundImage = "linear-gradient(88.55deg, rgb(139, 109, 255) 22.43%, rgb(254, 132, 132) 92.28%)";
            tvBlockFooter.style.backgroundClip = "text";
            tvBlockFooter.style.color = "transparent";
            tvBlockFooter.style.fontWeight = "bold";
            tvBlockFooter.style.userSelect = "none";
            footerDiv.appendChild(tvBlockFooter);

            // Activer le déplacement
            enableDragging(tvBlock, footerDiv);
        }
    });

    // Fonction pour déplacer l'élément
    function enableDragging(tvBlock, footerDiv) {
        let shiftX, shiftY;

        function moveAt(pageX, pageY) {
            tvBlock.style.left = pageX - shiftX + 'px';
            tvBlock.style.top = pageY - shiftY + 'px';
        }

        function onMouseMove(event) {
            moveAt(event.pageX, event.pageY);
        }

        function onTouchMove(event) {
            moveAt(event.touches[0].pageX, event.touches[0].pageY);
        }

        footerDiv.addEventListener("mousedown", function (e) {
            shiftX = e.clientX - tvBlock.getBoundingClientRect().left;
            shiftY = e.clientY - tvBlock.getBoundingClientRect().top;

            document.addEventListener("mousemove", onMouseMove);

            footerDiv.onmouseup = function () {
                document.removeEventListener("mousemove", onMouseMove);
                footerDiv.onmouseup = null;
            };
        });

        footerDiv.addEventListener("touchstart", function (e) {
            e.preventDefault();
            shiftX = e.touches[0].clientX - tvBlock.getBoundingClientRect().left;
            shiftY = e.touches[0].clientY - tvBlock.getBoundingClientRect().top;

            document.addEventListener("touchmove", onTouchMove);

            footerDiv.ontouchend = function () {
                document.removeEventListener("touchmove", onTouchMove);
                footerDiv.ontouchend = null;
            };
        });
    }
})();
