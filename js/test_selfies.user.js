// ==UserScript==
// @name        Test - Selfies
// @description New features for Kindroid's selfies page
// @namespace   https://gitlab.com/breatfr
// @match       https://kindroid.ai/selfies
// @match       https://kindroid.ai/selfies/*
// @version     1.08.4
// @homepageURL https://gitlab.com/breatfr/kindroid
// @supportURL  https://discord.gg/fSgDHmekfG
// @author      BreatFR
// @copyright   2023, BreatFR (https://breat.fr)
// @grant       none
// @icon        https://gitlab.com/breatfr/kindroid/-/raw/main/images/icon_kindroid.png
// @license     BY-NC-ND; https://creativecommons.org/licenses/by-nc-nd/4.0/
// ==/UserScript==

(function() {
    'use strict';

    // Design
    var style = document.createElement('style');
    style.textContent = `
        /* Hide icons at the right of header */
        .css-z0osps > div > div:nth-of-type(2) {
          display: none;
          height: 0;
          width: 0;
        }

        #selfies-script-button {
          background-color: transparent;
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' fill='none' stroke='%23cbcbcb' stroke-width='32' stroke-linejoin='round'%3E%3Cpath d='M368 128h80m-384 0h240m64 256h80m-384 0h240m-96-128h240m-384 0h80' stroke-linecap='round'/%3E%3Ccircle cx='336' cy='128' r='32'/%3E%3Ccircle cx='176' cy='256' r='32'/%3E%3Ccircle cx='336' cy='384' r='32'/%3E%3C/svg%3E");
          background-repeat: no-repeat;
          background-position: center center;
          border: none;
          cursor: pointer;
          display: block;
          height: 40px;
          position: fixed;
          top: 10px;
          transition: opacity 0.3s ease;
          width: 40px;
        }

        #selfies-script-button:hover {
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'%3E%3Cg fill='none' stroke='url(%23A)' stroke-width='32' stroke-linejoin='round'%3E%3Cpath d='M368 128h80M64 128h240M368 384h80M64 384h240M208 256h240M64 256h80' stroke-linecap='round'/%3E%3Ccircle cx='336' cy='128' r='32'/%3E%3Ccircle cx='176' cy='256' r='32'/%3E%3Ccircle cx='336' cy='384' r='32'/%3E%3C/g%3E%3Cdefs%3E%3ClinearGradient id='A' x1='1.6' y1='25.2' x2='29.2' y2='0' gradientUnits='userSpaceOnUse'%3E%3Cstop offset='.208' stop-color='%238b6dff'/%3E%3Cstop offset='1' stop-color='%23fe8484'/%3E%3C/linearGradient%3E%3C/defs%3E%3C/svg%3E");
        }

        #selfies-script-menu {
          background-color: var(--chakra-colors-secondaryBlack);
          border-radius: 0 0 1em 1em;
          box-shadow: 0px 0px 5px rgba(139, 109, 255, 0.8),
                      0px 4px 10px rgba(139, 109, 255, 0.6),
                      0px 8px 15px rgba(139, 109, 255, 0.4),
                      0px 12px 20px rgba(254, 132, 132, 0.4),
                      0px 16px 25px rgba(254, 132, 132, 0.3);
          color: var(--chakra-colors-secondaryWhite);
          display: none;
          font-size: 1.2rem;
          height: auto;
          padding: 10px 0;
          position: fixed;
          right: 16px;
          text-align: left;
          top: 60px;
          width: max-content;
          z-index: 99999;
        }

        .selfies-script-item {
          align-items: center;
          cursor: pointer;
          display: flex;
          justify-content: space-between;
          padding: 5px 10px;
          width: 100%;
        }
        .selfies-script-item:hover {
          background: linear-gradient(88.55deg, rgb(139, 109, 255) 22.43%, rgb(254, 132, 132) 92.28%);
          background-clip: text;
          -webkit-background-clip: text;
          color: transparent;
        }
        .toggle-icon {
          height: 40px;
          margin-left: 10px;
          vertical-align: middle;
          width: auto;
        }

        .download-icon {
          background-color: transparent;
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'%3E%3Cpath d='M320 336h76c55 0 100-21.21 100-75.6s-53-73.47-96-75.6C391.11 99.74 329 48 256 48c-69 0-113.44 45.79-128 91.2-60 5.7-112 35.88-112 98.4S70 336 136 336h56M192 400.1l64 63.9 64-63.9M256 224v224.03' fill='none' stroke='%23cbcbcb' stroke-linecap='round' stroke-linejoin='round' stroke-width='32'/%3E%3C/svg%3E");
          background-repeat: no-repeat;
          background-position: center center;
          border: none;
          cursor: pointer;
          display: inline-block;
          height: 25px;
          margin-left: 10px;
          margin-right: 5px;
          transition: opacity 0.3s ease;
          vertical-align: middle;
          width: 25px;
        }

        .selfies-script-item:hover .download-icon {
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'%3E%3Cpath d='M320 336h76c55 0 100-21.21 100-75.6s-53-73.47-96-75.6C391.11 99.74 329 48 256 48c-69 0-113.44 45.79-128 91.2-60 5.7-112 35.88-112 98.4S70 336 136 336h56M192 400.1l64 63.9 64-63.9M256 224v224.03' fill='none' stroke='url(%23A)' stroke-linecap='round' stroke-linejoin='round' stroke-width='32'/%3E%3Cdefs%3E%3ClinearGradient id='A' x1='1.6' y1='25.2' x2='29.2' y2='0' gradientUnits='userSpaceOnUse'%3E%3Cstop offset='.208' stop-color='%238b6dff'/%3E%3Cstop offset='1' stop-color='%23fe8484'/%3E%3C/linearGradient%3E%3C/defs%3E%3C/svg%3E");
        }

        .progress-container {
          background-color: #eee;
          border-radius: 5px;
          box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);
          display: none;
          height: 20px;
          position: fixed;
          right: 150px;
          top: 20px;
          width: 200px;
        }

        .progress-bar {
          background-color: #4caf50;
          border-radius: 5px;
          height: 100%;
          position: relative;
          width: 0;
        }

        .progress-percents {
          color: black;
          left: 50%;
          position: absolute;
          top: 50%;
          transform: translate(-50%, -50%);
        }

        @media screen and (max-width: 1400px), (orientation: portrait) {
          #selfies-script-button {
            height: 36px;
            top: 5px;
            width: 36px;
          }

          #selfies-script-menu {
            right: 16px !important;
            top: 48px;
            width: calc(100% - 32px) !important;
          }

          .progress-container {
            height: 16px;
            left: 50px;
            right: auto;
            top: 16px;
          }
        }
    `;
    document.head.appendChild(style);

    // Function to set data in localStorage
    function setLocalStorage(name, value) {
        localStorage.setItem(name, value);
    }

    // Function to get data from localStorage
    function getLocalStorage(name) {
        return localStorage.getItem(name);
    }

    // Load user preferences from localStorage
    var seeAllImagesEnabled = getLocalStorage('seeAllImagesEnabled') === 'true';

    // Variables globales
    let selfiesMenuVisible = false;
    let toggleIcons = {
        toggleOffUri: "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' height='30px' viewBox='0 0 1000 1000' fill='%23c5000f'%3E%3Cpath d='M753.4 287.4H246.7c-101.8 0-184.2 95.2-184.2 212.8S145 712.9 246.7 712.9h506.6c101.7 0 184.1-95.2 184.1-212.7s-82.4-212.8-184-212.8zm-22.8 389.1H269.4c-92.6 0-167.7-78.9-167.7-176.3s75.1-176.3 167.7-176.3h461.3c92.7 0 167.7 78.9 167.7 176.3s-75.1 176.3-167.8 176.3zM441.2 500.1c0 73.9-59.9 133.9-133.9 133.9s-133.9-59.9-133.9-133.9c0-73.9 59.9-133.9 133.9-133.9s133.9 60 133.9 133.9zM634 449.8c-3.8-6.3-8.7-11.2-14.7-14.8-6-3.5-13-5.3-21.2-5.3s-15.3 1.8-21.2 5.3c-6 3.5-10.9 8.5-14.7 14.8s-6.7 13.8-8.6 22.5-2.8 18-2.8 28.1.9 19.4 2.8 28c1.9 8.5 4.7 15.9 8.6 22.2 3.8 6.3 8.7 11.2 14.7 14.7s13 5.3 21.2 5.3 15.3-1.8 21.2-5.3c6-3.5 10.9-8.4 14.7-14.7s6.7-13.6 8.6-22.2c1.9-8.5 2.8-17.9 2.8-28s-.9-19.5-2.8-28.1c-1.9-8.7-4.8-16.2-8.6-22.5zm-36 97.6c-15.8 0-23.7-15.7-23.7-47s7.9-47 23.8-47c8 0 13.9 3.9 17.7 11.8 3.8 7.8 5.7 19.6 5.7 35.2 0 31.4-7.8 47-23.5 47zM663.7 432v136.3c3.7.8 7.5 1.1 11.2 1.1 3.4 0 7.1-.4 10.9-1.1v-57h31.5c.6-3.9.9-7.9.9-11.9 0-3.8-.3-7.7-.9-11.7h-31.5v-32h39.7c.6-3.8.9-7.8.9-11.9a74.58 74.58 0 0 0-.9-11.6h-61.8v-.2zm76.8 0v136.3c3.7.8 7.5 1.1 11.2 1.1 3.4 0 7.1-.4 10.9-1.1v-57h31.5c.6-3.9.9-7.9.9-11.9 0-3.8-.3-7.7-.9-11.7h-31.5v-32h39.7c.6-3.8.9-7.8.9-11.9a74.58 74.58 0 0 0-.9-11.6h-61.8v-.2z'/%3E%3C/svg%3E",
        toggleOnUri: "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' height='30px' viewBox='0 0 1000 1000' fill='%232dd55b'%3E%3Cpath d='M753.3 287.4H246.6c-101.7 0-184.1 95.2-184.1 212.8 0 117.5 82.5 212.7 184.1 212.7h506.6c101.8 0 184.2-95.2 184.2-212.7.1-117.6-82.4-212.8-184.1-212.8zm-22.7 389.1H269.4c-92.7 0-167.7-78.9-167.7-176.3s75-176.3 167.7-176.3h461.3c92.6 0 167.7 79 167.7 176.3s-75.2 176.3-167.8 176.3zm-38-310.2c-73.9 0-133.9 59.9-133.9 133.9 0 73.9 59.9 133.9 133.9 133.9s133.9-59.9 133.9-133.9-59.9-133.9-133.9-133.9zm-386.4 78.2c-3.8-6.3-8.7-11.2-14.7-14.8-6-3.5-13-5.3-21.2-5.3s-15.3 1.8-21.2 5.3c-6 3.5-10.9 8.5-14.7 14.8s-6.7 13.8-8.6 22.5-2.8 18-2.8 28.1.9 19.4 2.8 28c1.9 8.5 4.7 15.9 8.6 22.2 3.8 6.3 8.7 11.2 14.7 14.7s13 5.3 21.2 5.3 15.3-1.8 21.2-5.3c6-3.5 10.9-8.4 14.7-14.7s6.7-13.6 8.6-22.2c1.9-8.5 2.8-17.9 2.8-28s-.9-19.5-2.8-28.1-4.7-16.1-8.6-22.5zm-35.9 97.7c-15.8 0-23.7-15.7-23.7-47s7.9-47 23.8-47c8 0 13.9 3.9 17.7 11.8 3.8 7.8 5.7 19.6 5.7 35.2 0 31.3-7.8 47-23.5 47zm126.1-115.5v86.8l-42.1-86.8c-3.2-.8-6.4-1.1-9.4-1.1-2.8 0-5.8.4-9 1.1V563c3.1.8 6.4 1.1 9.7 1.1s6.6-.4 9.9-1.1v-86.6L398 563c3.3.8 6.4 1.1 9.2 1.1 2.6 0 5.6-.4 8.8-1.1V426.7c-2.9-.8-6-1.1-9.1-1.1-3.3 0-6.8.4-10.5 1.1z'/%3E%3C/svg%3E"
    };

    // Fonction pour créer le bouton des paramètres
    function createSettingsButton() {
        const settingsBtn = document.createElement('button');
        settingsBtn.ariaLabel = 'Selfies script settings';
        settingsBtn.id = 'selfies-script-button';
        settingsBtn.title = 'Selfies script settings';
        settingsBtn.dataset.priority = 1;
        document.body.appendChild(settingsBtn);

        settingsBtn.addEventListener('click', toggleMenu);
        settingsBtn.addEventListener('touchstart', function (e) {
            e.preventDefault();
            toggleMenu();
        });

        return settingsBtn;
    }

    // Fonction pour ajouter une icône
    function addIcon(iconId, priority) {
        const isMobile = window.matchMedia("(max-width: 1400px), (orientation: portrait)").matches;
        // Récupérer le bouton existant par son ID
        const iconButton = document.getElementById(iconId);
        if (!iconButton) {
            console.error(`Le bouton avec l'ID ${iconId} n'existe pas.`);
            return;
        }

        // Créer ou mettre à jour l'icône dans un tableau des priorités
        let icons = [];

        // Récupérer tous les boutons existants
        const buttons = document.querySelectorAll('button[id]');
        buttons.forEach(button => {
            const buttonId = button.id;
            const buttonPriority = parseInt(button.dataset.priority) || 0; // Récupère la priorité ou 0 par défaut
            icons.push({ id: buttonId, priority: buttonPriority });
        });

        // Vérifier si l'icône actuelle est déjà dans le tableau, et la mettre à jour
        const existingIconIndex = icons.findIndex(icon => icon.id === iconId);
        if (existingIconIndex !== -1) {
            icons[existingIconIndex].priority = priority; // Met à jour la priorité si déjà existante
        } else {
            icons.push({ id: iconId, priority: priority }); // Sinon, ajouter l'icône
        }

        // Trier les icônes par priorité croissante (priorité 1 est la plus à droite)
        icons.sort((a, b) => a.priority - b.priority);

        // Calculer la position basée sur l'ordre trié
        let occupiedSpace = isMobile ? -11 * window.innerWidth / 100 : 16; // 11% de la largeur de l'écran pour mobile
        const spacing = isMobile ? 10 : 20; // Espacement de 10px pour mobile, 20px pour desktop
        console.log("Initial occupiedSpace: ", occupiedSpace);

        icons.forEach(icon => {
            const button = document.getElementById(icon.id);
            if (button) {
                console.log(`Placement pour ${icon.id} avant ajustement: ${occupiedSpace}px`);
                button.style.right = isMobile ? `${Math.abs(occupiedSpace)}px` : `${occupiedSpace}px`; // Positionner le bouton
                occupiedSpace += 40 + spacing; // 40px pour l'icône + espacement dynamique
                console.log(`OccupiedSpace après ${icon.id}: ${occupiedSpace}px`);
            }
        });
    }
    addIcon("selfies-script-button", 1);

    // Fonction pour gérer la visibilité du menu
    function toggleMenu() {
        const dropdownMenu = document.getElementById('selfies-script-menu');
        selfiesMenuVisible = !selfiesMenuVisible;
        dropdownMenu.style.display = selfiesMenuVisible ? 'block' : 'none';
    }

    // Fonction pour créer le menu déroulant et ses options
    function createDropdownMenu() {
        const dropdownMenu = document.createElement('div');
        dropdownMenu.id = 'selfies-script-menu';

        const options = [
            {
                action: (enabled) => { seeAllImagesEnabled = enabled; seeAllImages(); },
                key: 'seeAllImagesEnabled',
                label: 'See All Images'
            },
            {
                action: (enabled) => { downloadAllImagesEnabled = enabled; downloadAllImages(); },
                isDownload: true,
                key: 'downloadAllImagesEnabled',
                label: 'Download All Images',
                title: 'Please make sure to enable "See All Images" before downloading.'
            }
        ];

        options.forEach(option => {
            const item = document.createElement('div');
            item.className = 'selfies-script-item';

            // Gestionnaire d'événements pour l'élément item
            item.addEventListener('click', (e) => {
                // Empêcher la propagation de l'événement si l'utilisateur clique sur l'icône
                if (e.target !== item) {
                    return; // Si on clique sur l'icône, on ne veut pas déclencher à nouveau le click sur l'item
                }
                // Active le téléchargement
                option.action(true);
            });

            if (option.isDownload) {
                const downloadIcon = document.createElement('div');
                downloadIcon.className = 'download-icon';
                downloadIcon.alt = 'Download';
                downloadIcon.title = option.title;
                downloadIcon.addEventListener('click', () => option.action());
                item.title = option.title;

                // Gestionnaire d'événements pour l'icône
                downloadIcon.addEventListener('click', (e) => {
                    e.stopPropagation(); // Empêche la propagation pour éviter d'exécuter l'événement sur l'item
                    option.action(true); // Active le téléchargement
                });

                const labelText = document.createTextNode(option.label);

                item.appendChild(labelText);
                item.appendChild(downloadIcon);
            } else {
                // Pour les autres options, utilisez le toggle normal
                item.innerText = option.label;

                const currentValue = getLocalStorage(option.key) === 'true';
                const toggleIcon = document.createElement('img');
                toggleIcon.setAttribute('aria-label', currentValue ? 'Enabled' : 'Disabled');
                toggleIcon.alt = 'Toggle';
                toggleIcon.className = 'toggle-icon';
                toggleIcon.src = currentValue ? toggleIcons.toggleOnUri : toggleIcons.toggleOffUri;

                item.appendChild(toggleIcon);
                item.addEventListener('click', () => handleOptionClick(option, toggleIcon));
            }

            dropdownMenu.appendChild(item);
        });

        document.body.appendChild(dropdownMenu);
    }

    // Fonction pour gérer le clic sur une option
    function handleOptionClick(option, toggleIcon) {
        const currentValue = getLocalStorage(option.key) === 'true';
        const newValue = !currentValue;
        setLocalStorage(option.key, newValue);
        toggleIcon.setAttribute('aria-label', newValue ? 'Enabled' : 'Disabled');
        toggleIcon.src = newValue ? toggleIcons.toggleOnUri : toggleIcons.toggleOffUri;

        if (typeof option.action === 'function') {
            option.action(newValue);
        }
        console.log(`${option.label}: ${newValue}`);
    }

    // Fonction pour gérer les clics en dehors du menu
    function handleOutsideClick(settingsBtn, dropdownMenu) {
        document.addEventListener('click', function (event) {
            const isClickInsideMenu = dropdownMenu.contains(event.target);
            const isClickInsideButton = settingsBtn.contains(event.target);
            if (!isClickInsideMenu && !isClickInsideButton) {
                selfiesMenuVisible = false;
                dropdownMenu.style.display = 'none';
            }
        });
    }

    // Initialisation de l'interface utilisateur
    (function initSelfiesPCUI() {
        const settingsBtn = createSettingsButton();
        addIcon("selfies-script-button", 1);
        createDropdownMenu();
        const dropdownMenu = document.getElementById('selfies-script-menu');
        handleOutsideClick(settingsBtn, dropdownMenu);
        console.log("Interface utilisateur créée et ajoutée au document.");
    })();

    // See All Images
    function seeAllImages() {
      if (seeAllImagesEnabled) {
        const button = document.querySelector('button.chakra-button.css-1m67823');
            if (button) {
                button.click(); // Cliquez sur le bouton
                console.log('Le bouton a été cliqué.');
            } else {
                console.log('Le bouton n\'a pas été trouvé.');
            }
        } else {
            console.log('L\'option n\'est pas active.');
        }
    }
    seeAllImages();

    window.onload = function() {
        setTimeout(seeAllImages, 3000); // Appel de la fonction après 3 secondes
    };

    // Import JSZip library
    var script = document.createElement('script');
    script.src = 'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js';
    document.head.appendChild(script);

    // Download all images
    let downloadAllImagesEnabled = false;

    function downloadAllImages() {
        console.log('Download All Images');
        const images = document.querySelectorAll('.css-3vu8ng img.css-1regj17');
        console.log('Found Images:', images);
        if (images.length === 0) {
            alert('No images found.');
            return;
        }

        // Créer une barre de progression
        const progressContainer = document.createElement('div');
        progressContainer.className = 'progress-container';

        const progressBar = document.createElement('div');
        progressBar.className = 'progress-bar';

        // Élément pour afficher le pourcentage
        const progressText = document.createElement('div');
        progressText.className = 'progress-percents';

        progressContainer.appendChild(progressBar);
        progressBar.appendChild(progressText); // Ajouter le texte à la barre de progression
        document.body.appendChild(progressContainer);

        const zip = new JSZip();
        let count = 0;
        const totalImages = images.length;
        progressContainer.style.display = 'block';

        images.forEach(function(image, index) {
            fetch(image.src)
                .then(response => response.blob())
                .then(blob => {
                    const filename = ('0000' + (totalImages - index)).slice(-4); // Reverse numbering
                    zip.file(`${filename}.jpg`, blob);
                    const prompt = image.alt;
                    const promptFile = new Blob([prompt], { type: 'text/plain' });
                    zip.file(`${filename}.txt`, promptFile);
                    ++count;

                    // Mettre à jour la barre de progression
                    const progressPercentage = Math.round((count / totalImages) * 100); // Calculer le pourcentage
                    progressBar.style.width = `${progressPercentage}%`;
                    progressText.innerText = `${progressPercentage}%`;

                    if (count === totalImages) {
                        zip.generateAsync({ type: 'blob' })
                            .then(content => {
                                // Trigger file download
                                const a = document.createElement('a');
                                a.href = URL.createObjectURL(content);
                                a.download = 'Kindroid.zip';
                                a.click();

                                // Réinitialiser la barre de progression
                                progressContainer.style.display = 'none'; // Cacher la barre de progression
                                progressBar.style.width = '0'; // Réinitialiser la largeur
                            });
                    }
                })
                .catch(err => {
                    console.error('Error fetching image:', err); // Afficher l'erreur dans la console
                });
        });
    }

    // Function to check if the device is mobile
    function isMobileDevice() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    }

    // Function to parse and format date
    function formatDate(dateString) {
        console.log(`Initial date string: ${dateString}`); // Log the initial date string

        // Use a regular expression to match the date pattern "7 03 '24"
        let dateMatch = dateString.match(/(\d{1,2}) (\d{2}) '(\d{2})/);
        console.log(`Date match result: ${dateMatch}`); // Log the match result

        // If the pattern is not found, return the original string (to avoid "Invalid Date")
        if (!dateMatch) {
            console.log("No match found. Returning original string.");
            return dateString;
        }

        // Extract the day, month, and year
        let day = dateMatch[2];
        let month = dateMatch[1];
        let year = "20" + dateMatch[3]; // Convert the 2-digit year to 4-digit
        console.log(`Parsed date - Day: ${day}, Month: ${month}, Year: ${year}`); // Log parsed values

        // Create a new date object using the parsed values
        let date = new Date(`${year}-${month}-${day}`);
        console.log(`Created date object: ${date}`); // Log the date object

        // Check if the date is valid
        if (isNaN(date)) {
            console.log("Invalid date. Returning original string.");
            return dateString;
        }

        // Get the browser's language
        let language = navigator.language;
        console.log(`Browser language: ${language}`); // Log the browser's language

        // Format the date with time
        let options = { year: 'numeric', month: 'long', day: 'numeric' };
        let formattedDate = date.toLocaleDateString(language, options);
        console.log(`Formatted date: ${formattedDate}`); // Log the formatted date
        return formattedDate;
    }

    // Function to format dates in modal
    function formatModalDates() {
        let modal = document.querySelector('.css-fskrnz');
        if (!modal) return;

        // Get all date elements
        let dateElements = document.querySelectorAll('.chakra-text.css-151uqsi');
        console.log(`Found ${dateElements.length} date elements.`); // Log number of elements found

        // Loop over each date element
        dateElements.forEach(function(element) {
            // Get the current date string
            let dateString = element.textContent.trim();
            console.log(`Current date string: ${dateString}`); // Log the current date string

            // Format the date
            let formattedDate = formatDate(dateString);

            // Update the element with the new date
            element.textContent = formattedDate;
            console.log(`Updated date element: ${formattedDate}`); // Log the updated date
        });
    }

    // Create a mutation observer to watch for changes in the DOM
    let observer = new MutationObserver(function(mutations) {
        // For each mutation
        mutations.forEach(function(mutation) {
            // If the mutation added nodes
            if (mutation.addedNodes.length) {
                console.log(`Mutation detected with added nodes: ${mutation.addedNodes.length}`); // Log number of added nodes
                // Format the dates in the modal
                formatModalDates();
            }
        });
    });

    // Start observing the document with the configured parameters
    document.addEventListener('DOMContentLoaded', function() {
        if (isMobileDevice()) {
            console.log("Mobile device detected. Date formatting will be disabled.");
            return; // Exit if it's a mobile device
        }
        observer.observe(document.body, { childList: true, subtree: true });
    });

    // Call the function initially to format dates on page load
    formatModalDates();
})();
