// ==UserScript==
// @name           Test - Translated
// @description    Get Kindroid's interface in your language
// @namespace      https://gitlab.com/breatfr
// @match          https://*.kindroid.ai/*
// @version        1.0.3
// @homepageURL    https://gitlab.com/breatfr/kindroid
// @supportURL     https://discord.gg/fSgDHmekfG
// @author         BreatFR
// @copyright      2024, BreatFR (https://breat.fr)
// @grant          none
// @icon           https://gitlab.com/breatfr/kindroid/-/raw/main/images/icon_kindroid.png
// @license        BY-NC-ND; https://creativecommons.org/licenses/by-nc-nd/4.0/
// ==/UserScript==

(function() {
    'use strict';

    // Design
    var style = document.createElement('style');
    style.textContent = `
        /* Hide icons at the right of header */
        .css-1uupglq {
          display: none;
          height: 0;
          width: 0;
        }

        #translate-script-button {
          background-color: transparent;
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'%3E%3Cpath fill='none' stroke='%23cbcbcb' stroke-linecap='round' stroke-linejoin='round' stroke-width='32' d='M48 112h288M192 64v48M272 448l96-224 96 224M301.5 384h133M281.3 112S257 206 199 277 80 384 80 384'/%3E%3Cpath d='M256 336s-35-27-72-75-56-85-56-85' fill='none' stroke='%23cbcbcb' stroke-linecap='round' stroke-linejoin='round' stroke-width='32'/%3E%3C/svg%3E");
          background-repeat: no-repeat;
          background-position: center center;
          border: none;
          cursor: pointer;
          display: block;
          height: 40px;
          position: fixed;
          top: 10px;
          transition: opacity 0.3s ease;
          width: 40px;
        }

        #translate-script-button:hover {
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' class='ionicon' viewBox='0 0 512 512'%3E%3Cpath fill='none' stroke='url(%23A)' stroke-linecap='round' stroke-linejoin='round' stroke-width='32' d='M48 112h288M192 64v48M272 448l96-224 96 224M301.5 384h133M281.3 112S257 206 199 277 80 384 80 384'/%3E%3Cpath d='M256 336s-35-27-72-75-56-85-56-85' fill='none' stroke='url(%23A)' stroke-linecap='round' stroke-linejoin='round' stroke-width='32'/%3E%3Cdefs%3E%3ClinearGradient id='A' x1='1.6' y1='25.2' x2='29.2' y2='0' gradientUnits='userSpaceOnUse'%3E%3Cstop offset='.208' stop-color='%238b6dff'/%3E%3Cstop offset='1' stop-color='%23fe8484'/%3E%3C/linearGradient%3E%3C/defs%3E%3C/svg%3E");
        }

        .gtranslate_wrapper {
          background-color: var(--chakra-colors-secondaryBlack);
          border-radius: 0 0 1em 1em;
          box-shadow:
                0px 0px 5px rgba(139, 109, 255, 0.8),
                0px 4px 10px rgba(139, 109, 255, 0.6),
                0px 8px 15px rgba(139, 109, 255, 0.4),
                0px 12px 20px rgba(254, 132, 132, 0.4),
                0px 16px 25px rgba(254, 132, 132, 0.3);
          color: var(--chakra-colors-secondaryWhite);
          display: none;
          max-height: 50vh;
          overflow-y: auto;
          position: fixed;
          top: 60px;
        }

        .gtranslate_wrapper a {
          display: block;
          padding: 5px 10px;
          white-space: nowrap;
        }
        .gtranslate_wrapper a > span {
           font-family: var(--chakra-fonts-PoppinsRegular);
           font-size: 1.2em;
           font-weight: normal;
        }
        .gtranslate_wrapper a:hover,
        .gtranslate_wrapper a > span:hover {
          background: linear-gradient(88.55deg, rgb(139, 109, 255) 22.43%, rgb(254, 132, 132) 92.28%);
          background-clip: text;
          -webkit-background-clip: text;
          color: transparent;
        }
        .gtranslate_wrapper a img {
            border-radius: 5px;
            width: 1.3333em;
        }

        @media screen and (max-width: 1400px), (orientation: portrait) {
          .css-1x6mnjh {
            gap: 0;
          }
          
          #translate-script-button {
            height: 36px;
            top: 5px;
            width: 36px;
          }

          .gtranslate_wrapper {
            right: 16px !important;
            top: 48px;
            width: calc(100% - 32px) !important;
          }
        }
    `;
    document.head.appendChild(style);

    // Add script tags for GTranslate
    const translateSettingsScript = document.createElement('script');
    translateSettingsScript.type = 'text/javascript';
    translateSettingsScript.innerHTML = 'window.gtranslateSettings = {"default_language":"en","native_language_names":true,"detect_browser_language":true,"wrapper_selector":".gtranslate_wrapper","flag_size":48,"flag_style":"2d"};';
    document.body.appendChild(translateSettingsScript);

    const translateWidgetScript = document.createElement('script');
    translateWidgetScript.src = 'https://cdn.gtranslate.net/widgets/latest/fn.js';
    translateWidgetScript.type = 'text/javascript';
    translateWidgetScript.defer = true;
    document.body.appendChild(translateWidgetScript);

    // Add a button at the top right
    const translateButton = document.createElement('button');
    translateButton.ariaLabel = 'Translate';
    translateButton.id = 'translate-script-button';
    translateButton.title = 'Translate';
    translateButton.dataset.priority = 4;
    document.body.appendChild(translateButton);

    // Fonction pour ajouter une icône
    function addIcon(iconId, priority) {
        const isMobile = window.matchMedia("(max-width: 1400px), (orientation: portrait)").matches;
        // Récupérer le bouton existant par son ID
        const iconButton = document.getElementById(iconId);
        if (!iconButton) {
            console.error(`Le bouton avec l'ID ${iconId} n'existe pas.`);
            return;
        }

        // Créer ou mettre à jour l'icône dans un tableau des priorités
        let icons = [];

        // Récupérer tous les boutons existants
        const buttons = document.querySelectorAll('button[id]');
        buttons.forEach(button => {
            const buttonId = button.id;
            const buttonPriority = parseInt(button.dataset.priority) || 0; // Récupère la priorité ou 0 par défaut
            icons.push({ id: buttonId, priority: buttonPriority });
        });

        // Vérifier si l'icône actuelle est déjà dans le tableau, et la mettre à jour
        const existingIconIndex = icons.findIndex(icon => icon.id === iconId);
        if (existingIconIndex !== -1) {
            icons[existingIconIndex].priority = priority; // Met à jour la priorité si déjà existante
        } else {
            icons.push({ id: iconId, priority: priority }); // Sinon, ajouter l'icône
        }

        // Trier les icônes par priorité croissante (priorité 1 est la plus à droite)
        icons.sort((a, b) => a.priority - b.priority);

        // Calculer la position basée sur l'ordre trié
        let occupiedSpace = isMobile ? -11 * window.innerWidth / 100 : 16; // 11% de la largeur de l'écran pour mobile
        const spacing = isMobile ? 10 : 20; // Espacement de 10px pour mobile, 20px pour desktop
        console.log("Initial occupiedSpace: ", occupiedSpace);

        icons.forEach(icon => {
            const button = document.getElementById(icon.id);
            if (button) {
                console.log(`Placement pour ${icon.id} avant ajustement: ${occupiedSpace}px`);
                button.style.right = isMobile ? `${Math.abs(occupiedSpace)}px` : `${occupiedSpace}px`; // Positionner le bouton
                occupiedSpace += 40 + spacing; // 40px pour l'icône + espacement dynamique
                console.log(`OccupiedSpace après ${icon.id}: ${occupiedSpace}px`);
            }
        });
    }
    addIcon("translate-script-button", 4);

    // Add click event to show or hide the content of .gtranslate_wrapper
    translateButton.addEventListener('click', function() {
        const isVisible = gtranslateWrapper.style.display === 'block';

        if (!isVisible) {
            // Obtenir la valeur de 'right' du bouton translateButton
            const buttonRight = window.getComputedStyle(translateButton).right;

            // Appliquer la même valeur de 'right' au menu déroulant
            gtranslateWrapper.style.right = buttonRight;

            // Afficher le menu déroulant
            gtranslateWrapper.style.display = 'block';
        } else {
            // Cacher le menu déroulant
            gtranslateWrapper.style.display = 'none';
        }
    });

    // Create the gtranslate_wrapper div
    const gtranslateWrapper = document.createElement('div');
    gtranslateWrapper.className = 'gtranslate_wrapper';
    document.body.appendChild(gtranslateWrapper);

    // Variable pour suivre l'état du menu
    let menuVisible = false;

    // Ajouter un événement au clic pour afficher ou cacher le contenu de .gtranslate_wrapper
    translateButton.addEventListener('click', function() {
        menuVisible = !menuVisible; // Inverser l'état du menu
        gtranslateWrapper.style.display = menuVisible ? 'block' : 'none'; // Afficher ou cacher le menu
    });

    // Fonction pour gérer les clics en dehors du menu de traduction
    function handleOutsideClick(translateButton, gtranslateWrapper) {
        document.addEventListener('click', function (event) {
            const isClickInsideMenu = gtranslateWrapper.contains(event.target);
            const isClickInsideButton = translateButton.contains(event.target);
            if (!isClickInsideMenu && !isClickInsideButton) {
                menuVisible = false; // Réinitialiser l'état du menu
                gtranslateWrapper.style.display = 'none'; // Cacher le menu
            }
        });
    }
    handleOutsideClick(translateButton, gtranslateWrapper);

    // Exclude classes from translation, as well as all their content + textareas
    function addNoTranslateClass() {
        const currentUrl = window.location.href;

        // Vérifier si l'URL correspond à celle de la page d'accueil
        if (currentUrl === 'https://kindroid.ai/home/') {
            const elements = document.querySelectorAll('ion-app > div > div > div > div:nth-of-type(1) > div:nth-of-type(2) > div:nth-of-type(1) > div, body > div.chakra-portal:last-child > div:nth-of-type(3) > div > section > div > div > p:nth-of-type(2) > div.chakra-accordion > div > div, body > div.chakra-portal:last-child > div:nth-of-type(3) > div > section > div > div > div:nth-of-type(2) > div, body > div.chakra-portal:last-child > div:nth-of-type(3) > div > section > div > div > div:nth-of-type(3) > div');
            elements.forEach(function(el) {
                el.classList.add('notranslate');
                el.querySelectorAll('*').forEach(function(child) {
                    child.classList.add('notranslate');
                });
            });
        }

        // Vérifier si l'URL commence par 'https://kindroid.ai/groupchat/'
        if (currentUrl.startsWith('https://kindroid.ai/groupchat/')) {
            const elements = document.querySelectorAll('ion-app > div > div > div > div > div > div:nth-of-type(2) > div, body > div.chakra-portal:last-child > div:nth-of-type(3) > div > section > div > div > p:nth-of-type(2) > div.chakra-accordion > div > div, body > div.chakra-portal:last-child > div:nth-of-type(3) > div > section > div > div > div:nth-of-type(2) > div, body > div.chakra-portal:last-child > div:nth-of-type(3) > div > section > div > div > div:nth-of-type(3) > div');
            elements.forEach(function(el) {
                el.classList.add('notranslate');
                el.querySelectorAll('*').forEach(function(child) {
                    child.classList.add('notranslate');
                });
            });
        }

        // Vérifier si l'URL correspond à celle de la page d'accueil
        if (currentUrl === 'https://kindroid.ai/selfies/') {
            const elements = document.querySelectorAll('.css-1z0jlm7');
            elements.forEach(function(el) {
                // Inclure un élément enfant
                const excludeElements = el.querySelectorAll('.css-501625');
                excludeElements.forEach(function(child) {
                    // Supprimer la classe 'notranslate' pour .css-501625 et ses enfants
                    child.classList.remove('notranslate');
                    child.querySelectorAll('*').forEach(function(grandChild) {
                        grandChild.classList.remove('notranslate');
                    });
                });
            });
        }

        // Exclure tous les textarea de la page si l'URL commence par 'https://kindroid.ai/'
        if (currentUrl.startsWith('https://kindroid.ai/')) {
            const textareas = document.querySelectorAll('textarea');
            textareas.forEach(function(textarea) {
                textarea.classList.add('notranslate');
            });
        }
    }

    // Appeler la fonction une fois au chargement
    addNoTranslateClass();

    // Utiliser MutationObserver pour surveiller les changements dans le DOM
    const observer = new MutationObserver((mutations) => {
        mutations.forEach((mutation) => {
            if (mutation.addedNodes.length) {
                addNoTranslateClass();
            }
        });
    });

    // Observer les changements dans le corps du document
    observer.observe(document.body, { childList: true, subtree: true });

    console.log("GTranslate added with a selection button and custom styles.");
})();
