// ==UserScript==
// @name        Kindroid - Chat page new features mobile
// @description New features for Kindroid's chat page
// @namespace   https://gitlab.com/breatfr
// @match       https://kindroid.ai/home*
// @match       https://kindroid.ai/groupchat*
// @match       https://kindroid.ai/call*
// @version     2.0.19
// @homepageURL https://gitlab.com/breatfr/kindroid
// @downloadURL https://gitlab.com/breatfr/kindroid/-/blob/main/js/kindroid_chat_page_new_features_mobile.user.js
// @updateURL   https://gitlab.com/breatfr/kindroid/-/blob/main/js/kindroid_chat_page_new_features_mobile.user.js
// @supportURL  https://discord.gg/fSgDHmekfG
// @author      BreatFR
// @copyright   2023, BreatFR (https://breat.fr)
// @grant       none
// @icon        https://gitlab.com/breatfr/kindroid/-/raw/main/images/icon_kindroid.png
// @license     BY-NC-ND; https://creativecommons.org/licenses/by-nc-nd/4.0/
// ==/UserScript==

(function() {
    'use strict';

    // Design
    var style = document.createElement('style');
    style.textContent = `
        /* Hide icons at the right of header */
        .css-x3odei > div:nth-of-type(2),
        .css-z0osps > div > div:nth-of-type(2) {
          display: none;
          height: 0;
          width: 0;
        }

        #chat-script-button {
          background-color: transparent;
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' fill='none' stroke='%23cbcbcb' stroke-width='32' stroke-linejoin='round'%3E%3Cpath d='M368 128h80m-384 0h240m64 256h80m-384 0h240m-96-128h240m-384 0h80' stroke-linecap='round'/%3E%3Ccircle cx='336' cy='128' r='32'/%3E%3Ccircle cx='176' cy='256' r='32'/%3E%3Ccircle cx='336' cy='384' r='32'/%3E%3C/svg%3E");
          background-repeat: no-repeat;
          background-position: center center;
          border: none;
          cursor: pointer;
          display: block;
          height: 40px;
          position: fixed;
          top: 10px;
          -o-transition: opacity 0.3s ease;
          -moz-transition: opacity 0.3s ease;
          -ms-transition: opacity 0.3s ease;
          -webkit-transition: opacity 0.3s ease;
          width: 40px;
        }

        #chat-script-button:hover {
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'%3E%3Cg fill='none' stroke='url(%23A)' stroke-width='32' stroke-linejoin='round'%3E%3Cpath d='M368 128h80M64 128h240M368 384h80M64 384h240M208 256h240M64 256h80' stroke-linecap='round'/%3E%3Ccircle cx='336' cy='128' r='32'/%3E%3Ccircle cx='176' cy='256' r='32'/%3E%3Ccircle cx='336' cy='384' r='32'/%3E%3C/g%3E%3Cdefs%3E%3ClinearGradient id='A' x1='1.6' y1='25.2' x2='29.2' y2='0' gradientUnits='userSpaceOnUse'%3E%3Cstop offset='.208' stop-color='%238b6dff'/%3E%3Cstop offset='1' stop-color='%23fe8484'/%3E%3C/linearGradient%3E%3C/defs%3E%3C/svg%3E");
        }

        #chat-script-menu {
          background-color: var(--chakra-colors-secondaryBlack);
          border-radius: 0 0 1em 1em;
          box-shadow: rgba(139, 110, 255, 0.4) -5px 5px 5px 5px,
                      rgba(254, 133, 133, 0.4) 5px 5px 5px 5px;
          color: var(--chakra-colors-secondaryWhite);
          display: none;
          font-size: 1.2rem;
          height: auto;
          padding: 10px 0;
          position: fixed;
          right: 16px;
          text-align: left;
          top: 60px;
          width: -o-max-content;
          width: -moz-max-content;
          width: -ms-max-content;
          width: -webkit-max-content;
          width: max-content;
          z-index: 99999;
        }

        .chat-script-item {
          align-items: center;
          cursor: pointer;
          display: flex;
          justify-content: space-between;
          padding: 5px 10px;
          width: 100%;
        }
        .chat-script-item:hover {
          background: linear-gradient(88.55deg, rgb(139, 109, 255) 22.43%, rgb(254, 132, 132) 92.28%);
          background-clip: text;
          -webkit-background-clip: text;
          color: transparent;
        }
        .toggle-icon {
          height: 40px;
          margin-left: 10px;
          vertical-align: middle;
          width: auto;
        }

        /* Formated date */
        .css-1dhayxc .css-1iqgkq5,
        .css-yhhl9h .css-160fgw0 {
            background: linear-gradient(88.55deg, rgb(139, 109, 255) 22.43%, rgb(254, 132, 132) 92.28%);
            background-clip: text;
            -webkit-background-clip: text;
            color: transparent;
            font-family: var(--chakra-fonts-PoppinsRegular), sans-serif;
            font-size: 1.2rem;
            font-weight: normal;
            white-space: nowrap;
        }

        @media screen and (max-width: 1400px), (orientation: portrait) {
          #chat-script-button {
            height: 36px;
            top: 5px;
            width: 36px;
          }

          #chat-script-menu {
            right: 16px !important;
            top: 48px;
            width: calc(100% - 32px) !important;
          }

          /* Formated date */
          .css-1dhayxc .css-1iqgkq5,
          .css-yhhl9h .css-160fgw0 {
              background: linear-gradient(88.55deg, rgb(139, 109, 255) 22.43%, rgb(254, 132, 132) 92.28%);
              background-clip: text;
              -webkit-background-clip: text;
              color: transparent;
              font-family: var(--chakra-fonts-PoppinsRegular), sans-serif;
              font-size: initial;
              font-weight: normal;
              white-space: nowrap;
          }
        }
    `;
    document.head.appendChild(style);

    // Function to set data in localStorage
    function setLocalStorage(name, value) {
        localStorage.setItem(name, value);
    }

    // Function to get data from localStorage
    function getLocalStorage(name) {
        return localStorage.getItem(name);
    }

    // Load user preferences from localStorage
    var animatedAvatarChangerEnabled = getLocalStorage('animatedAvatarChangerEnabled') === 'true';
    var autoConfirmEnabled = getLocalStorage('autoConfirmEnabled') === 'true';
    var autoContinueEnabled = getLocalStorage('autoContinueEnabled') === 'true';
    var autoFocusEnabled = getLocalStorage('autoFocusEnabled') === 'true';
    var blurContentEnabled = getLocalStorage('blurContentEnabled') === 'true';

    // Variables globales
    let menuVisible = false;
    let toggleIcons = {
        toggleOffUri: "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' height='30px' viewBox='0 0 1000 1000' fill='%23c5000f'%3E%3Cpath d='M753.4 287.4H246.7c-101.8 0-184.2 95.2-184.2 212.8S145 712.9 246.7 712.9h506.6c101.7 0 184.1-95.2 184.1-212.7s-82.4-212.8-184-212.8zm-22.8 389.1H269.4c-92.6 0-167.7-78.9-167.7-176.3s75.1-176.3 167.7-176.3h461.3c92.7 0 167.7 78.9 167.7 176.3s-75.1 176.3-167.8 176.3zM441.2 500.1c0 73.9-59.9 133.9-133.9 133.9s-133.9-59.9-133.9-133.9c0-73.9 59.9-133.9 133.9-133.9s133.9 60 133.9 133.9zM634 449.8c-3.8-6.3-8.7-11.2-14.7-14.8-6-3.5-13-5.3-21.2-5.3s-15.3 1.8-21.2 5.3c-6 3.5-10.9 8.5-14.7 14.8s-6.7 13.8-8.6 22.5-2.8 18-2.8 28.1.9 19.4 2.8 28c1.9 8.5 4.7 15.9 8.6 22.2 3.8 6.3 8.7 11.2 14.7 14.7s13 5.3 21.2 5.3 15.3-1.8 21.2-5.3c6-3.5 10.9-8.4 14.7-14.7s6.7-13.6 8.6-22.2c1.9-8.5 2.8-17.9 2.8-28s-.9-19.5-2.8-28.1c-1.9-8.7-4.8-16.2-8.6-22.5zm-36 97.6c-15.8 0-23.7-15.7-23.7-47s7.9-47 23.8-47c8 0 13.9 3.9 17.7 11.8 3.8 7.8 5.7 19.6 5.7 35.2 0 31.4-7.8 47-23.5 47zM663.7 432v136.3c3.7.8 7.5 1.1 11.2 1.1 3.4 0 7.1-.4 10.9-1.1v-57h31.5c.6-3.9.9-7.9.9-11.9 0-3.8-.3-7.7-.9-11.7h-31.5v-32h39.7c.6-3.8.9-7.8.9-11.9a74.58 74.58 0 0 0-.9-11.6h-61.8v-.2zm76.8 0v136.3c3.7.8 7.5 1.1 11.2 1.1 3.4 0 7.1-.4 10.9-1.1v-57h31.5c.6-3.9.9-7.9.9-11.9 0-3.8-.3-7.7-.9-11.7h-31.5v-32h39.7c.6-3.8.9-7.8.9-11.9a74.58 74.58 0 0 0-.9-11.6h-61.8v-.2z'/%3E%3C/svg%3E",
        toggleOnUri: "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' height='30px' viewBox='0 0 1000 1000' fill='%232dd55b'%3E%3Cpath d='M753.3 287.4H246.6c-101.7 0-184.1 95.2-184.1 212.8 0 117.5 82.5 212.7 184.1 212.7h506.6c101.8 0 184.2-95.2 184.2-212.7.1-117.6-82.4-212.8-184.1-212.8zm-22.7 389.1H269.4c-92.7 0-167.7-78.9-167.7-176.3s75-176.3 167.7-176.3h461.3c92.6 0 167.7 79 167.7 176.3s-75.2 176.3-167.8 176.3zm-38-310.2c-73.9 0-133.9 59.9-133.9 133.9 0 73.9 59.9 133.9 133.9 133.9s133.9-59.9 133.9-133.9-59.9-133.9-133.9-133.9zm-386.4 78.2c-3.8-6.3-8.7-11.2-14.7-14.8-6-3.5-13-5.3-21.2-5.3s-15.3 1.8-21.2 5.3c-6 3.5-10.9 8.5-14.7 14.8s-6.7 13.8-8.6 22.5-2.8 18-2.8 28.1.9 19.4 2.8 28c1.9 8.5 4.7 15.9 8.6 22.2 3.8 6.3 8.7 11.2 14.7 14.7s13 5.3 21.2 5.3 15.3-1.8 21.2-5.3c6-3.5 10.9-8.4 14.7-14.7s6.7-13.6 8.6-22.2c1.9-8.5 2.8-17.9 2.8-28s-.9-19.5-2.8-28.1-4.7-16.1-8.6-22.5zm-35.9 97.7c-15.8 0-23.7-15.7-23.7-47s7.9-47 23.8-47c8 0 13.9 3.9 17.7 11.8 3.8 7.8 5.7 19.6 5.7 35.2 0 31.3-7.8 47-23.5 47zm126.1-115.5v86.8l-42.1-86.8c-3.2-.8-6.4-1.1-9.4-1.1-2.8 0-5.8.4-9 1.1V563c3.1.8 6.4 1.1 9.7 1.1s6.6-.4 9.9-1.1v-86.6L398 563c3.3.8 6.4 1.1 9.2 1.1 2.6 0 5.6-.4 8.8-1.1V426.7c-2.9-.8-6-1.1-9.1-1.1-3.3 0-6.8.4-10.5 1.1z'/%3E%3C/svg%3E"
    };

    // Fonction pour créer le bouton des paramètres
    function createSettingsButton() {
        const settingsBtn = document.createElement('button');
        settingsBtn.ariaLabel = 'Chat script settings';
        settingsBtn.id = 'chat-script-button';
        settingsBtn.title = 'Chat script settings';
        settingsBtn.dataset.priority = 1;
        document.body.appendChild(settingsBtn);

        settingsBtn.addEventListener('click', toggleMenu);
        settingsBtn.addEventListener('touchstart', function (e) {
            e.preventDefault();
            toggleMenu();
        });

        return settingsBtn;
    }

    // Fonction pour ajouter une icône
    function addIcon(iconId, priority) {
        const isMobile = window.matchMedia("(max-width: 1400px), (orientation: portrait)").matches;
        // Récupérer le bouton existant par son ID
        const iconButton = document.getElementById(iconId);
        if (!iconButton) {
            console.error(`Le bouton avec l'ID ${iconId} n'existe pas.`);
            return;
        }

        // Créer ou mettre à jour l'icône dans un tableau des priorités
        let icons = [];

        // Récupérer tous les boutons existants
        const buttons = document.querySelectorAll('button[id]');
        buttons.forEach(button => {
            const buttonId = button.id;
            const buttonPriority = parseInt(button.dataset.priority) || 0; // Récupère la priorité ou 0 par défaut
            icons.push({ id: buttonId, priority: buttonPriority });
        });

        // Vérifier si l'icône actuelle est déjà dans le tableau, et la mettre à jour
        const existingIconIndex = icons.findIndex(icon => icon.id === iconId);
        if (existingIconIndex !== -1) {
            icons[existingIconIndex].priority = priority; // Met à jour la priorité si déjà existante
        } else {
            icons.push({ id: iconId, priority: priority }); // Sinon, ajouter l'icône
        }

        // Trier les icônes par priorité croissante (priorité 1 est la plus à droite)
        icons.sort((a, b) => a.priority - b.priority);

        // Calculer la position basée sur l'ordre trié
        let occupiedSpace = isMobile ? -11 * window.innerWidth / 100 : 16; // 11% de la largeur de l'écran pour mobile
        const spacing = isMobile ? 10 : 20; // Espacement de 10px pour mobile, 20px pour desktop
        console.log("Initial occupiedSpace: ", occupiedSpace);

        icons.forEach(icon => {
            const button = document.getElementById(icon.id);
            if (button) {
                console.log(`Placement pour ${icon.id} avant ajustement: ${occupiedSpace}px`);
                button.style.right = isMobile ? `${Math.abs(occupiedSpace)}px` : `${occupiedSpace}px`; // Positionner le bouton
                occupiedSpace += 40 + spacing; // 40px pour l'icône + espacement dynamique
                console.log(`OccupiedSpace après ${icon.id}: ${occupiedSpace}px`);
            }
        });
    }
    addIcon("chat-script-button", 1);

    // Fonction pour gérer la visibilité du menu
    function toggleMenu() {
        const dropdownMenu = document.getElementById('chat-script-menu');
        menuVisible = !menuVisible;
        dropdownMenu.style.display = menuVisible ? 'block' : 'none';
    }

    function isMobile() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || window.matchMedia("(max-width: 1400px), (orientation: portrait)").matches;
    }

    // Fonction pour créer le menu déroulant et ses options
    function createDropdownMenu() {
        const dropdownMenu = document.createElement('div');
        dropdownMenu.id = 'chat-script-menu';

        const options = [
            {
                action: (enabled) => { animatedAvatarChangerEnabled = enabled; animatedAvatarChanger(); },
                hideOnMobile: true,
                key: 'animatedAvatarChangerEnabled',
                label: 'Animated Avatar Changer'
            },
            {
                action: (enabled) => { autoConfirmEnabled = enabled; autoConfirm(); },
                key: 'autoConfirmEnabled',
                label: 'Auto Confirm Regenerate'
            },
            {
                action: (enabled) => { autoContinueEnabled = enabled; autoContinue(); },
                key: 'autoContinueEnabled',
                label: 'Auto Continue cut-off message'
            },
            {
                action: (enabled) => { autoFocusEnabled = enabled; autoFocus(); },
                key: 'autoFocusEnabled',
                label: 'Auto Focus Textarea'
            },
            {
                action: (enabled) => { blurContentEnabled = enabled; blurContent(); },
                key: 'blurContentEnabled',
                label: 'Blur Content'
            }
        ];

        const visibleOptions = isMobile()
            ? options.filter(option => !option.hideOnMobile)
            : options;

        visibleOptions.forEach(option => {
            const item = document.createElement('div');
            item.className = 'chat-script-item';
            item.innerText = option.label;

            const currentValue = getLocalStorage(option.key) === 'true';
            const toggleIcon = document.createElement('img');
            toggleIcon.setAttribute('aria-label', currentValue ? 'Enabled' : 'Disabled');
            toggleIcon.className = 'toggle-icon';
            toggleIcon.src = currentValue ? toggleIcons.toggleOnUri : toggleIcons.toggleOffUri;

            item.appendChild(toggleIcon);
            item.addEventListener('click', () => handleOptionClick(option, toggleIcon));
            dropdownMenu.appendChild(item);
        });

        document.body.appendChild(dropdownMenu);
    }

    // Fonction pour gérer le clic sur une option
    function handleOptionClick(option, toggleIcon) {
        const currentValue = getLocalStorage(option.key) === 'true';
        const newValue = !currentValue;
        setLocalStorage(option.key, newValue);
        toggleIcon.setAttribute('aria-label', newValue ? 'Enabled' : 'Disabled');
        toggleIcon.src = newValue ? toggleIcons.toggleOnUri : toggleIcons.toggleOffUri;

        if (option.key === 'animatedAvatarChangerEnabled') {
            animatedAvatarChangerEnabled = newValue;
            toggleAnimatedAvatarChanger(newValue);
        } else if (typeof option.action === 'function') {
            option.action(newValue);
        }

        console.log(`${option.label}: ${newValue}`);
    }

    // Fonction pour gérer les clics en dehors du menu
    function handleOutsideClick(settingsBtn, dropdownMenu) {
        document.addEventListener('click', function (event) {
            const isClickInsideMenu = dropdownMenu.contains(event.target);
            const isClickInsideButton = settingsBtn.contains(event.target);
            if (!isClickInsideMenu && !isClickInsideButton) {
                menuVisible = false;
                dropdownMenu.style.display = 'none';
            }
        });
    }

    // Initialisation de l'interface utilisateur
    (function initChatPCUI() {
        const settingsBtn = createSettingsButton();
        addIcon("chat-script-button", 1);
        createDropdownMenu();
        const dropdownMenu = document.getElementById('chat-script-menu');
        handleOutsideClick(settingsBtn, dropdownMenu);
        console.log("Interface utilisateur créée et ajoutée au document.");
    })();

    // Vérification des liens vidéos
    function isValidVideoUrl(url) {
        // Liste des extensions vidéo courantes
        const videoExtensions = ['ogg', 'mkv', 'mov', 'mp4', 'webm'];

        // Vérifie si l'URL se termine par une extension vidéo connue
        if (videoExtensions.some(ext => url.toLowerCase().endsWith('.' + ext))) {
            return true;
        }

        // Vérifie si l'URL contient des paramètres qui suggèrent une vidéo
        if (url.includes('video') || url.includes('ogg') || url.includes('mkv') || url.includes('mov') || url.includes('mp4') || url.includes('webm')) {
            return true;
        }

        // Vérifie si l'URL provient de services de stockage connus
        if (url.includes('firebasestorage.googleapis.com') || url.includes('storage.googleapis.com')) {
            return true;
        }

        return false;
    }

    // Fonction pour appliquer ou retirer le style personnalisé
    function applyCustomStyle() {
        const existingStyle = document.getElementById('customAvatarStyle');
        if (existingStyle) {
            existingStyle.remove();
        }

        if (animatedAvatarChangerEnabled && window.location.href.includes('https://kindroid.ai/call/')) {
            const customStyle = document.createElement('style');
            customStyle.id = 'customAvatarStyle';
            customStyle.textContent = `
              /* Remove background under avatar */
              .css-1nbfxil > div:not(:last-child),
              .css-17hv5wr > div:not(:last-child),
              .css-1jbvs8u,
              /* Remove purple circle when kin is talking */
              .css-jk7dyx {
                  background: none;
              }

              /* Avatar */
              .css-1nbfxil,
              .css-17hv5wr {
                  aspect-ratio: 1 / 1;
                  height: calc(100% - 100px);
                  margin-top: 0;
                  min-width: 300px;
                  overflow: hidden;
                  width: fit-content;
              }
              .css-1jbvs8u,
              .css-jk7dyx {
                  border: none;
                  border-radius: 1em;
                  box-sizing: border-box;
                  height: 100%;
                  margin: auto;
                  width: 100%;
              }
              .css-dv4mhk {
                  height: 120px;
                  width: 120px;
              }
              .css-1jbvs8u img,
              .css-1jbvs8u video,
              .css-jk7dyx img,
              .css-jk7dyx video {
                  aspect-ratio: 1 / 1;
                  border: none;
                  border-radius: 1em;
                  height: 100%;
                  margin-top: 0;
                  max-width: 100%;
                  object-fit: contain;
                  object-position: center top;
                  width: auto;
              }
              .css-1jbvs8u video,
              .css-jk7dyx video {
                  left: auto !important;
              }

              /* Avoid scroll problem */
              .css-11w64rs {
                  overflow: hidden;
              }

              /* Call controls */
              .css-bdvhoj {
                  justify-content: space-between;
                  margin: 0 auto;
                  padding: 0 20px;
              }
              .css-bdvhoj:has(> :only-child) {
                  justify-content: center;
              }
              .css-bdvhoj > .css-15do4u8 {
                  flex-direction: row;
              }
              .css-bdvhoj > .css-15do4u8 svg {
                  height: 44px;
                  width: 44px;
              }
              .css-18j5yoy,
              .css-15do4u8 > div {
                  height: 60px;
              }

              /* Call controls when captions */
              body:has(.css-mcoft3) {
                  .css-11w64rs {
                      column-gap: 1em;
                      flex-direction: row;
                      flex-wrap: wrap;
                      padding: 1em 1em 0 1em;
                  }
                  .css-1nbfxil,
                  .css-17hv5wr {
                      height: calc(100% - 100px);
                      width: 40%;
                  }
                  .css-mcoft3 {
                      max-height: calc(100% - 100px);
                      min-height: calc(100% - 100px);
                      max-width: calc(60% - 1em);
                  }
                  .css-bdvhoj {
                      flex-basis: 100%;
                      min-height: 100px;
                  }
              }
          `;
          document.head.appendChild(customStyle);
        }
    }

    // Animated Avatar Changer
    function animatedAvatarChanger(forcePrompt = false) {
        if (animatedAvatarChangerEnabled) {
            let talkingVideoUrl = getLocalStorage('talkingVideoUrl');
            let idleVideoUrl = getLocalStorage('idleVideoUrl');

            if (forcePrompt || !talkingVideoUrl || !idleVideoUrl) {
                alert("You need to provide two video URLs:\n" +
                    "1. One for when your kin is talking.\n" +
                    "2. One for when your kin is waiting.\n\n" +
                    "Example:\nhttps://example.com/talking.mp4\nhttps://example.com/idle.mp4\n\n" +
                    "Please ensure both URLs are valid video links.");

                let isValid = false;
                while (!isValid) {
                    talkingVideoUrl = prompt("Enter the URL for the 'talking' video:", talkingVideoUrl || '');
                    if (!talkingVideoUrl) return;

                    idleVideoUrl = prompt("Enter the URL for the 'idle' video:", idleVideoUrl || '');
                    if (!idleVideoUrl) return;

                    if (!isValidVideoUrl(talkingVideoUrl)) {
                        alert("The 'talking' video URL doesn't seem to be valid. Please try again.");
                    } else if (!isValidVideoUrl(idleVideoUrl)) {
                        alert("The 'idle' video URL doesn't seem to be valid. Please try again.");
                    } else {
                        isValid = true;
                    }
                }

                setLocalStorage('talkingVideoUrl', talkingVideoUrl);
                setLocalStorage('idleVideoUrl', idleVideoUrl);
            }

            function changeVideosHome() {
                const container = document.querySelector('.css-owh929');
                if (!container) return false;

                const img = container.querySelector('img');
                const videoElements = container.querySelectorAll('video');
                if (videoElements.length < 2) return false;

                const talkingVideo = videoElements[0];
                const idleVideo = videoElements[1];

                // Save original sources if not already done
                if (!getLocalStorage('originalTalkingSrc')) {
                    setLocalStorage('originalTalkingSrc', talkingVideo.src);
                    setLocalStorage('originalIdleSrc', idleVideo.src);
                }

                // Update only the src attributes
                talkingVideo.src = talkingVideoUrl;
                idleVideo.src = idleVideoUrl;

                function ensureVideoPlay() {
                    videoElements.forEach(video => {
                        if (video.paused && getComputedStyle(video).display !== 'none') {
                            video.play().catch(e => console.log("Couldn't play video:", e));
                        }
                    });
                }

                function toggleVideoPlay() {
                    if (img.classList.contains('css-1age63q')) {
                        // Image is showing, switch to videos
                        img.classList.remove('css-1age63q');
                        img.classList.add('css-9bmz2t');
                        videoElements.forEach(video => {
                            video.style.display = '';
                        });
                        ensureVideoPlay();
                    } else {
                        // Videos are showing, switch to image
                        img.classList.remove('css-9bmz2t');
                        img.classList.add('css-1age63q');
                        videoElements.forEach(video => {
                            video.style.display = 'none';
                            video.pause();
                        });
                    }
                }

                container.addEventListener('click', toggleVideoPlay);
                document.addEventListener("visibilitychange", function() {
                    if (!document.hidden && img.classList.contains('css-9bmz2t')) {
                        ensureVideoPlay();
                    }
                });

                setInterval(ensureVideoPlay, 500);
                ensureVideoPlay();

                return true;
            }

            function changeVideosVoiceCall() {
                let isObserving = false;

                function updateVideoSources() {
                    const container = document.querySelector('.css-1jbvs8u');
                    if (!container) return false;

                    const videoElements = container.querySelectorAll('video');
                    if (videoElements.length < 2) return false;

                    const talkingVideo = videoElements[0];
                    const idleVideo = videoElements[1];

                    // Save original sources if not already done
                    if (!getLocalStorage('originalTalkingSrc')) {
                        setLocalStorage('originalTalkingSrc', talkingVideo.src);
                        setLocalStorage('originalIdleSrc', idleVideo.src);
                    }

                    // Update only if sources are different
                    if (talkingVideo.src !== talkingVideoUrl) {
                        talkingVideo.src = talkingVideoUrl;
                    }
                    if (idleVideo.src !== idleVideoUrl) {
                        idleVideo.src = idleVideoUrl;
                    }

                    videoElements.forEach(video => {
                        if (video.paused && getComputedStyle(video).display !== 'none') {
                            video.play().catch(e => console.log("Couldn't play video:", e));
                        }
                    });

                    return true;
                }

                function startObserving() {
                    if (isObserving) return;

                    const observer = new MutationObserver((mutations) => {
                        for (let mutation of mutations) {
                            if (mutation.type === 'childList') {
                                if (updateVideoSources()) {
                                    console.log("Videos updated");
                                }
                            }
                        }
                    });

                    observer.observe(document.body, {
                        childList: true,
                        subtree: true
                    });

                    isObserving = true;

                    // Stop the observer after 5 minutes to prevent it from running indefinitely
                    setTimeout(() => {
                        observer.disconnect();
                        isObserving = false;
                    }, 300000);
                }

                // Initial update attempt
                if (!updateVideoSources()) {
                    console.log("Initial update failed, starting observation");
                    startObserving();
                } else {
                    console.log("Initial update successful");
                }

                // Handle visibility changes
                document.addEventListener("visibilitychange", function() {
                    if (!document.hidden) {
                        if (updateVideoSources()) {
                            console.log("Videos updated on visibility change");
                        } else {
                            console.log("Update failed on visibility change, starting observation");
                            startObserving();
                        }
                    }
                });

                return true;
            }

            // Try to change videos immediately
            const currentURL = window.location.href;
            let changeSuccessful = false;

            if (currentURL.includes('https://kindroid.ai/home/')) {
                changeSuccessful = changeVideosHome();
            } else if (currentURL.includes('https://kindroid.ai/call/')) {
                changeSuccessful = changeVideosVoiceCall();
            }

            if (!changeSuccessful) {
                // If it doesn't work, set up a mutation observer
                const observer = new MutationObserver((mutations) => {
                    if (currentURL.includes('https://kindroid.ai/home/')) {
                        if (changeVideosHome()) {
                            observer.disconnect();
                        }
                    } else if (currentURL.includes('https://kindroid.ai/call/')) {
                        if (changeVideosVoiceCall()) {
                            observer.disconnect();
                        }
                    }
                });

                observer.observe(document.body, {
                    childList: true,
                    subtree: true
                });

                // Stop the observer after 30 seconds to prevent it from running indefinitely
                setTimeout(() => observer.disconnect(), 30000);
            }
        } else {
            // Restore original sources
            const videoElements = document.querySelectorAll('.css-owh929 video, .css-1jbvs8u video');
            if (videoElements.length > 1) {
                if (getLocalStorage('originalTalkingSrc')) {
                    videoElements[0].src = getLocalStorage('originalTalkingSrc');
                }
                if (getLocalStorage('originalIdleSrc')) {
                    videoElements[1].src = getLocalStorage('originalIdleSrc');
                }
            }
        }
    }

    // On page loading
    if (animatedAvatarChangerEnabled) {
        // Attendez que le DOM soit complètement chargé
        if (document.readyState === 'loading') {
            document.addEventListener('DOMContentLoaded', () => {
                animatedAvatarChanger();
                applyCustomStyle();
            });
        } else {
            animatedAvatarChanger();
            applyCustomStyle();
        }
    }

    // Changer l'état de animatedAvatarChangerEnabled dynamiquement
    function toggleAnimatedAvatarChanger(enabled) {
        animatedAvatarChangerEnabled = enabled;
        applyCustomStyle();
        if (enabled) {
            animatedAvatarChanger(true); // Force le prompt quand on active l'option
        } else {
            // Restore original sources
            const videoElements = document.querySelectorAll('.css-owh929 video, .css-1jbvs8u video');
            if (videoElements.length > 1) {
                if (getLocalStorage('originalTalkingSrc')) {
                    videoElements[0].src = getLocalStorage('originalTalkingSrc');
                }
                if (getLocalStorage('originalIdleSrc')) {
                    videoElements[1].src = getLocalStorage('originalIdleSrc');
                }
            }
        }
    }

    // Auto Confirm Regenerate
    function autoConfirmRegenerate() {
        if (autoConfirmEnabled) {
            const modal = document.querySelector('.css-hklsfr');
            const confirmButton = document.querySelector('button[aria-label="Regenerate"]');

            if (modal && confirmButton) {
                confirmButton.click();
            }
        }
        setTimeout(autoConfirmRegenerate, 500);
    }
    autoConfirmRegenerate();

    // Auto Continue cut-off message
    function autoContinue() {
        let continueButton = null;
        let isPaused = false;
        let buttonClicked = false;

        // Vérifier si l'option est activée avant de définir l'intervalle
        if (autoContinueEnabled) {
            const checkContinueButton = setInterval(() => {
                if (!isPaused) {
                    // Sélectionner tous les boutons avec la classe .css-94x4at
                    const buttons = document.querySelectorAll('body:has(.css-1b1fzbz) .css-r6z5ec > .css-do4s31 > .css-94x4at, .css-yhhl9h:last-child [aria-label="Continue cut-off message"]');

                    // Parcourir les boutons pour trouver celui avec le texte "Continue cut-off message"
                    buttons.forEach(button => {
                        if ((button.textContent.includes('Continue cut-off message') || button.getAttribute('aria-label') === 'Continue cut-off message') && !buttonClicked) {
                            continueButton = button;
                        }
                    });

                    // Vérifier la présence du spinner
                    const spinner = document.querySelector('.css-b9bzmp:has(.chakra-spinner)');
                    if (spinner) {
                        console.log('Auto Continue: spinner détecté, pause de l\'auto-clique.');
                        isPaused = true;
                        buttonClicked = false; // Réinitialiser l'état du bouton cliqué
                    } else if (continueButton && !buttonClicked) {
                        continueButton.click(); // Clique sur le bouton
                        console.log('Auto Continue: le bouton a été cliqué.');
                        buttonClicked = true; // Marquer le bouton comme cliqué
                    } else {
                        console.log('Auto Continue: le bouton n\'existe plus ou a déjà été cliqué.');
                    }
                }

                // Vérifier si l'élément d'erreur apparaît
                const errorElement = document.querySelector('.css-1ivukf8');
                if (errorElement) {
                    console.log('Auto Continue: erreur détectée, pause de l\'auto-clique.');
                    isPaused = true;
                    buttonClicked = false; // Réinitialiser l'état du bouton cliqué
                }
            }, 100); // Intervalle de vérification

            // Observateur de mutations pour réactiver l'auto-clique
            const observer = new MutationObserver((mutations) => {
                mutations.forEach((mutation) => {
                    if (mutation.addedNodes.length > 0) {
                        mutation.addedNodes.forEach((node) => {
                            if (node.nodeType === 1) {
                                // Vérifier si le nouvel élément ou l'un de ses descendants a la classe .css-yhhl9h
                                if (node.querySelector('.css-yhhl9h') || node.classList.contains('css-yhhl9h')) {
                                    console.log('Auto Continue: nouvelle réponse détectée, réactivation de l\'auto-clique.');
                                    isPaused = false;
                                    buttonClicked = false; // Réinitialiser l'état du bouton cliqué
                                }
                            }
                        });
                    }
                });
            });

            // Démarrer l'observateur
            observer.observe(document.body, {
                childList: true,
                subtree: true
            });
        } else {
            // Si l'option est désactivée, arrêtez l'intervalle s'il existe
            clearInterval(checkContinueButton);
        }
    }

    // Appeler la fonction pour démarrer l'auto-continue
    autoContinue();

    // Autofocus on textarea
    function focusTextarea() {
        var textareaElement = document.querySelector('textarea[aria-label="Send message textarea"]');
        textareaElement.focus();
    }

    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            if (autoFocusEnabled) {
                focusTextarea();
            }
        });
    });

    observer.observe(document.body, { childList: true, subtree: true });

    setInterval(function() {
        if (autoFocusEnabled) {
            focusTextarea();
        }
    }, 100);

    // Function to format date
    function formatDate(dateString) {
        try {
            // Créer un objet Date à partir de la chaîne originale
            const date = new Date(dateString);

            // Options de formatage
            const options = {
                weekday: 'long',
                year: 'numeric',
                month: 'long',
                day: 'numeric',
                hour: '2-digit',
                minute: '2-digit',
                hour12: false
            };

            // Formatter la date en utilisant les paramètres locaux
            let formattedDate = date.toLocaleString(navigator.language, options);

            // Mettre la première lettre de chaque mot en majuscule
            return formattedDate.replace(/\b\w/g, char => char.toUpperCase());
        } catch (error) {
            console.error("Error formatting date: ", error);
            return dateString;
        }
    }

    function formatDateInElement(element) {
        try {
            let dateString = element.textContent.trim();
            console.log('Original date string:', dateString);
            let formattedDate = formatDate(dateString);

            // Vérifier si la date a été modifiée
            if (formattedDate !== dateString) {
                element.textContent = formattedDate;
                console.log(`Formatted date: ${formattedDate}`);
            }
        } catch (error) {
            console.error("Error formatting date in element: ", error);
        }
    }

    function addDateClickListeners() {
        console.log('Adding date click listeners...');
        document.querySelectorAll('.css-fujl5p').forEach(function(element) {
            if (!element.dataset.dateListenerAdded) {
                element.addEventListener('click', function(event) {
                    console.log('Element clicked:', element);
                    setTimeout(() => {
                        let chatBubble = element.querySelector('.css-1iqgkq5, .css-160fgw0');
                        if (chatBubble) {
                            console.log('Found chat bubble:', chatBubble);
                            formatDateInElement(chatBubble);
                        } else {
                            console.log('No chat bubble found');
                        }
                    }, 100);
                });
                element.dataset.dateListenerAdded = 'true';
            }
        });
    }

    // Observateur de mutations
    let dateObserver = new MutationObserver(function(mutations) {
        console.log('Mutation observed, adding date click listeners...');
        addDateClickListeners();
    });

    // Démarrer l'observateur
    dateObserver.observe(document.body, {
        childList: true,
        subtree: true
    });

    // Ajouter les écouteurs initiaux
    addDateClickListeners();

    // Function to toggle content blur
    function blurContent() {
        const customStyle = document.getElementById('customBlurStyle');

        if (blurContentEnabled) {
            if (!customStyle) {
                const style = document.createElement('style');
                style.id = 'customBlurStyle';
                style.textContent = `
                    /* email in menu */
                    .css-e43fvd > p,
                    /* Bubbles content */
                    .css-1dhayxc .css-j7qwjs,
                    .css-yhhl9h .css-j7qwjs,
                    /* Shared links */
                    .css-1dhayxc .css-fujl5p > div:nth-of-type(2) > p.css-dhnoom,
                    .css-1dhayxc .css-116lnl0 > div:nth-of-type(2) > p.css-dhnoom,
                    /* Voicecall captions */
                    .css-1md15gg > div > p {
                      position: relative;
                      visibility: hidden;
                    }

                    .css-e43fvd > p::before,
                    .css-1dhayxc .css-j7qwjs::before,
                    .css-yhhl9h .css-j7qwjs::before,
                    .css-1md15gg > div > p::before,
                    .css-1dhayxc .css-fujl5p > div:nth-of-type(2) > p.css-dhnoom::before,
                    .css-1dhayxc .css-116lnl0 > div:nth-of-type(2) > p.css-dhnoom::before {
                      color: #cbcbcb;
                      font-family: var(--chakra-fonts-PoppinsRegular);
                      left: 50%;
                      position: absolute;
                      top: 50%;
                      transform: translate(-50%, -50%);
                      visibility: visible;
                      white-space: nowrap;
                    }
                    .css-e43fvd > p::before,
                    .css-1dhayxc .css-j7qwjs::before,
                    .css-yhhl9h .css-j7qwjs::before,
                    .css-1md15gg > div > p::before {
                      content: "Hidden Text";
                    }
                    .css-1dhayxc .css-fujl5p > div:nth-of-type(2) > p.css-dhnoom::before,
                    .css-1dhayxc .css-116lnl0 > div:nth-of-type(2) > p.css-dhnoom::before {
                      content: "Hidden Link";
                    }
                    .css-e43fvd > p::before {
                      font-size: 1.3em;
                    }
                    .css-1dhayxc .css-j7qwjs::before,
                    .css-yhhl9h .css-j7qwjs::before,
                    .css-1md15gg > div > p::before {
                      font-size: 2em;
                    }

                    /* Bubbles content */
                    .css-1dhayxc .css-j7qwjs::before,
                    .css-yhhl9h .css-j7qwjs::before,
                    /* Voicecall captions */
                    .css-1md15gg > div > p::before {
                      font-size: 2em;
                    }
                    /* Images uploading and Images uploaded */
                    .css-2jbc1u img,
                    .css-1dhayxc img:not([src*="/_next/static/media/editIcon"]) {
                      background: url("https://placehold.co/220x220?text=Hidden+image") no-repeat;
                      height: 0;
                      padding-left: 220px;
                      padding-top: 220px;
                      width: 0;
                    }
                `;
                document.head.appendChild(style);
            }
        } else {
            if (customStyle) {
                document.head.removeChild(customStyle);
            }
        }

        const blurSelectors = [
            // email in menu
            '.css-e43fvd > p',
            // Images sent by us
            '.css-2jbc1u img',
            '.css-1dhayxc img:not([src*="/_next/static/media/editIcon"])',
            // Bubbles content
            '.css-1dhayxc .css-j7qwjs',
            '.css-yhhl9h .css-j7qwjs',
            // Voicecall captions
            '.css-1md15gg > div > p'
        ];
        const blurElements = document.querySelectorAll(blurSelectors.join(', '));

        blurElements.forEach(function(element) {
            if (blurContentEnabled) {
                element.classList.add('hidden-content');
            } else {
                element.classList.remove('hidden-content');
            }
        });
    }

    // Function to observe the DOM for changes
    function observeBlurContent() {
        const targetNode = document.querySelector('ion-app'); // Change this to your main container if needed

        const config = { childList: true, subtree: true };

        const callback = function(mutationsList) {
            for (const mutation of mutationsList) {
                if (mutation.type === 'childList') {
                    blurContent(); // Reapply the blur when new elements are added
                }
            }
        };

        const observer = new MutationObserver(callback);
        observer.observe(targetNode, config);
    }

    // Appeler la fonction pour démarrer l'observation du DOM
    observeBlurContent();

    // Start observing the DOM once the content is loaded
    document.addEventListener("DOMContentLoaded", function() {
        blurContent(); // Apply blur on initial load
        observeBlurContent(); // Start observing for changes
    });

    // Kindroid Compagnion
    const manifestLink = document.createElement('link');
    manifestLink.crossorigin = 'anonymous';
    manifestLink.href = 'https://corsproxy.io/?url=https://gitlab.com/breatfr/kindroid/-/raw/main/json/manifest.json';
    manifestLink.rel = 'manifest';

    document.head.appendChild(manifestLink);
    console.log("Manifest added to the site!");
    // Replace title for compagnion
    function replaceTitle() {
      if  (window.matchMedia('(display-mode: standalone)').matches) {
        document.title  =  "Kindroid";
      }
    }
    window.addEventListener('load', replaceTitle);
    setInterval(replaceTitle, 100);

    // Replace favicon
    const newFavicon = 'data:image/jpg;base64,/9j/4AAQSkZJRgABAQAASABIAAD/4QCIRXhpZgAATU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAABJKGAAcAAAAvAAAAUKABAAMAAAABAAEAAKACAAQAAAABAAACAKADAAQAAAABAAACAAAAAABBU0NJSQAAADEuODYuMC1WQU80V1FGVVhLWUk1Skk3Mk9DSVdEUFFNWS4wLjEtOQD/7QA4UGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAAA4QklNBCUAAAAAABDUHYzZjwCyBOmACZjs+EJ+/8AAEQgCAAIAAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/bAEMAAgICAgICAwICAwQDAwMEBQQEBAQFBwUFBQUFBwgHBwcHBwcICAgICAgICAoKCgoKCgsLCwsLDQ0NDQ0NDQ0NDf/bAEMBAgICAwMDBgMDBg0JBwkNDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDf/dAAQAIP/aAAwDAQACEQMRAD8A/EAknknNJRRQAUUUUAFFFKRgYoASijofpQOKADAGRRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAB3zRRRQAcdMUc4xRRQAoGaTijrx2ooAKKOO4zR9KACij6c0vt3oASiijjFAC/hmkpcGk70AKBnoelJz6UUp5oASij6Cj396ACij6UuMUAJRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAH//0Pw/o7Y9KKPp0oAKKKKAA0UUUAFFFH+eaACjOKPpSgZoATtmjp1/SiigAooooAKPwzRRQADpjGaTPtS0UAAoGfTFBNKMgZFACUUe9HXtmgABBoo470celABx25o49/yo49KKAEzS/nRxRQAUUcUUAFFHHUUUAFFHfJ5ooAKKCcUUAFFFFABRRRQAUc9qKKACjnsKKKADijjuaKKAAcUo4pKUHAxQADmko69aWgBOnXiijtiigBe2RSUgpePSgAooyO3NFABRRRQAUUUUAFFFFABRRRQB/9H8P+aQHNLRxigAooooAKKKXdj8aAE5pcj6UGkoAPY0UUUAFFBA7Uc0AFA/Wj2xRQAuKCGxk0ZzSUALj06UmBiiigBc46UgxnJoooAKKKKACiiigAooooAKKKKACiiigAooooAKO1FFABSikooAOKKKKADn8e1Bo75ooAKKOORiigA74oo57UUAFFGR60UAFFFFABRRRQAUUUUAFFFFAB2xRzRRQAUUUUAFFFFAB0NOIGMim0UAFFFFAH//0vxA5HFJS5PX1pKACiiigAzRRRQAZNFFFABR1oo57UAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABS5xSUUAFFFFAB3o6n60UUAB9xRR3zzS0AJ7UUUH1oAKKACaB/hQAUUUUAFFFFABRRRQAUUUUAFFFFAH/9P8P6KO2aKACjtiiigAOPSiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKOnWiloASilGAc0deKAEopcjsKMcZ7/AONACUUDmigD/9T8P6KKKACjPNFFABx3opTzSUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFGcUUcUAFFA6UUAf/1fw/ooooAKOpwaXAPWkOT1oAOKKTIHPWl47UAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFHHagAooooAKKKKACiiigAooooAKKKKACiiigAopMiloAKKTIpc5oAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKBgjmilAoASiiigD//W/D+jrRRQAd6KKKACjjFFH86ACilPFJQAUUUUAFFFFABRRR2xQAgq3p+n3uq31rpmnxme6u5Ughi/vySNtRP0qpz3rvPhZ/yUzwn/ANhqy/SSuzCUvbV4QNKfvnV/8M8fGX/oWbj/AL7j/wDiqUfs7/GY/wDMs3H/AH8i/wDiq/YZee9OXBPFfvz8IcBFJ+2kfU0sgpdz8dz+zr8Zu/hi4/7+R/8AxVOH7O/xq/6Fi5/7+J/8XX7HYIHFP7Yrjn4UZfv7SRvT4coz6n43f8M7/Grv4Xuf+/if/FUf8M6fGw/8ytc/9/E/+Kr9lE6VKK4qnhpg4fbkdlPhOi/tn4z/APDOPxs7eF7j/v5F/wDF0/8A4Zw+No/5la4/7+Rf/FV+zXUc1InYVwz8PMEtfaSOqnwXhr/GfjAf2bvjgP8AmV7j/v5F/wDFVx3jH4Y+PPh/BbXPjHSJNLiunZIXd0O+Q/7n0r91OeDXwj+3Xz4e8Kbv+f64/wDQK+fzvhPDYPCzrQmcuZ8JUcLhZ14TPzeooor88Pz8KKKKACiiigAooooAT/PWj+ZowN/Ne9fs9fBm8+MnjWPTZVlh0PT/APSNVulXhVGNkSZB+eXIU+3zdBmg7Mvy+tjK8KFH4pGB4Z+A/wAXPGGjQeIPDfhq5utNuwxhn3LGjhG2scO3d/5V0H/DMHx3/wChRuv+/kX/AMVX7eaXp1ho+m2ul6ZAtvaWUUdvBDEvyRpENiLWjUVJ2R+94LwcwLp/va0rn4Y/8MufHn/oUbn/AL+Rf/FUf8Mu/Hr/AKFK5/7+Rf8AxdfukuRgU8g9RmuGpjJno0/BjLP+f8j8K/8Ahlv49dvCNz/38i/+Kpf+GW/j0OvhG6H/AG0i/wDiq/dLnb1NH/A/1riqZnWWljT/AIgpln/P6R+Fv/DLnx67eEbr/v5F/wDFU7/hln4/d/CVz/38i/8Aiq/dDI9cfjTM47/kf/r1xTzysvsF/wDEFcp6VJH4YH9ln4/n/mULn/v7F/8AFUf8Ms/tADp4Ruf+/sX/AMVX7pbz2Y0bz3c1n/rBW6wD/iCuVf8APyX4f5H4Xj9ln4/9/CVzz/00i/8Aiq818b+APF/w61OLRfGlg2nXskX2iOBnUt5fKb/kZ8fcP5V+5nxV+J+h/CPwZe+MdbYSNGpjs7Tdte5ujxHEg7jP+s/2Aa/Cjxp4x1/4geJr/wAW+Jbp7i9vpTIxJJ2j+BIwekcafKmO617OW4ytiPfn8B+XcecLZTkbhRws5TqnL0UUV6x+YhRRRQAUUUUAFFFFABRRRQAUUUUAFHPaij6mgAooooA//9f8P6KKKACiigDjIFABRRRQAUUUUAFFFFABRRRQAUUUUAIfSu6+F3/JTfCf/YZsv/RlcN3H+e1dz8Lv+Sm+Ev8AsM2X/oyu/K/96h/iNKH8Q/bVRgflSj79IpyPypM/NX9uVPhR+hUdkWf4alX/ABqNB+7+lSLk4xXlVD06QkfU1PVYfeNWRXj4jY9CkSp9wVKOlRjpUg6V4+I2O6kiZf8AH+dfB/7dn/IueFf+v+4/9Ar7vXpXwf8At1f8i54VP/T9c/8AouviuK/+RdM4+JP+RdVPzfooor8OPxQKKKKACiiigAooo57cnoAOpJoGlfRG94X8M6z4y8Q2PhfQbdrm/v5RFHGg4A9T6Rx9ZP8Adr90fg78L9F+E3gqy8LaaFe4VRJf3e357m6wDI/QfJn5Y/8AYWvnz9kb4Fr4C8Px+P8AxJbKviDWocwpKvz2dlLjAGQMO/3pP9k7P7wr7SQ/N+FdKpdT+iPDjhT6lRWPrr35bEy8IaefuE1GgyT9KfH0NcmI2P2GmTr0FOPSmU/+GvGxJ6UAPSl47Uh6UDpXkz3OgaoznNMJwKepxnPFNH3K8yqhp2I/vMCD0rP1bVdN0fSLzWtbuEsrGwiee4mlOFVIq0du1dvdh19BX5VfthftCf8ACUajL8KvBt1u0WwlxqtxE3/H3cxZ/d5z88MZxnGN7jHOBVYLBzxNTlPk+LuJ6OS4F4iXxP4TwP8AaD+NepfGbxu+ooZLfQdPL2+lWZbhIhkeY4BPzyZO4nsQvavBv4qXpRmvvqdOFOHs4H8bZhj6+OxE8ViZ+9MWiiitDzwooooAKKKKACiiigAooooAKKKKACl59KSigA70Ud80UAf/0Pw/ooooAOe1L2NJRzj60AFFFFABRRRQAUUUUAFFFFABRRRQAdx/ntXc/C7/AJKb4S/7DNl/6Mrhu4/z2rufhd/yU3wl/wBhmy/9GV6GV/71T/xGlD+Ij9sCeKcKBSjqfqP5V/cFX4UfoVHZFyPtRF1FMSnxdRXlYjY9KAqdfxqVqj/iqUfdrxsRsd1MlH3TUydfyqEfdNTL978q8fEHfSJY6+EP26/+Rc8Jf9f9z/6Lr7wT7/5V8H/t1/8AIueEv+v+5/8ARdfFcVK+WyOHiP8A5F1U/N+iiivwo/GAooooAKKOO1FACHrmvs/9kj4EHx1r6+P/ABPb7vD+jynyIZV+W8uosYHTmOMkO/uNvrXgHwd+F+rfFvxxaeFtNV47dR599dbDstrWMje2cEc52R/7bV+4/hfw1pHg3w/p3hnQIFt9P0+AQxRr7DG//fk+Z5P96u7CYfn98/SfD3hd4+v9cr/BA6YYyAOB6fSn9xUTdQKkWujELQ/o+mklZEsfXHtUidqZ/FT07V4+I2O+gPToPxqSOo06D8akjrycRuerS+Ecen5fzooPT8v50HkYrxqh0Ebqpwi0Hyxw1IiFW9a8e+Nfxb0f4NeCLvxJflZtTnBh0qyLcz3BzjuDsj+89cCpznU5IHBjsfQwWHnia8rRieIftb/H1fhxob/D7wtdAeJtXhPnzRt89hayAjJ54kk7d1U7/wC6a/IMksSzZYsSSSckknJ59zzWz4i8Qax4s16+8Ta9ctdahqE0lxcSOc7nl7D2HQVi9sV9nl+BhQhyH8ccZcU1s7xzrT+D7I6jGaKK7T5EKKKKACiiigAooooAKKKKACiiigAooooAKP60UUAKSc4FJRx2o74oA//R/D/2o470UUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAHcf57V3Pwu/5Kb4S/7DNl/wCjK4buP89q7j4X8fEzwmf+ozZf+jK9DK/96p/4jWh8Z+2KelSJ6VGnWph0r+438K9D72lsiVO1P7fj/Wmr978f6Uq9K8iuenT2JT0BqYfdqH+H/PrUy9BXkYk7aY+P0/z0qRen+fWq6dqtr0FeNiNz0oEn8Yr4Q/bq/wCRf8K/9f1x/wCgV91hq+Ff27P+Rc8K/wDX/cf+gV8TxX/yL5nHxH/yL5H5vUUUV+En4uFFFFAARzj0q1Yabfaxf2+k6ZC9xeXkyQwxIp3s8v3F+g7+1U8kD86/TX9j74FtpFpF8VvFlvi+u42/se3mX/UwS43TkEZy4zsz0Rt2PmrrweE9rPkR7PD+SVMxxSowPor4BfBux+D/AIKisNqy63fiO41W7HUuQP3Kcf6uP0/j+9XvCYGOKgX7tSL9z619NKn7OHsz+osqwMMHh4YeiWT0p56Uzt+X86mPT8K8jEbHt0yVuv8An0ph+8Kav3fx/rTj96vGxJ6FIlTrU69f8+1V/UVYXrXi1z06QjU1epqSmZRFMhI2gcn2ry6h0yaS1Of8T+JNG8IeH77xPr9ytrp2mQma4lb/AGQTj/fk+6n+2RX4afG34va18Z/Glz4jv90FjDmDTLPp9mtdx2EjJHmSZ+d+vNe4ftbfH9viFr7eAvCtx/xTOiylJZYmwt/djILnn544yfk9xu9BXxZ3r18twnL78z+XPEvjV5hiPqGGf7qH/kwtFFFesfkAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB/9L8P6KKKACiiigAooooAKKKKACiiigAooooAKKKKAENdz8K/wDkpXhT/sM2X/oyuGNdz8K/+SleFP8AsM2X/oyvQyv/AHqn/iNKH8RH7Yr2+lSJ1/H+lRr2+lPXr+Nf3JV+Beh99S2Q9OtWe/51V/iq13/OvJxB30hR1qRe1RjrUi9hXkVz0aY9O1WF6VXT/P61On3Pxrxa+56UB47V8K/t1H/inPCn/X5c/wDoFfdqf5/SvhH9uz/kXfCf/X7c/wDouvi+L/8AkX1Tz8//AORdI/OCijtiivwY/GxAOKBjtSE12Hw/8Da58RvFVj4T0OIvc3kg3OM+XDFn95M5AOEjHT3xWlODmuRGlKlUrVPZwPbv2YvgpN8UvF66vq8DDw3o7pJdPk/6RMuPLgHH8RwZMY+UepAr9ireKG3iSCFVihhGxFRfkRBXHfD3wPovw58Jaf4V0OMJb2UY3tt+eebHzyv/ALbvXbR9PwNfb4LLvq0Ln9FcJZHTy7C8r/iCx8ECp4+lV1HzYq0Og/D+dZYjY+4pkq47VOetVk6/59KnHU14+IPQpjj93NSH7/5VEen5fzp/8X+fSvFxG520yftTh0AqM/cp6jgV42J3PSp9hxODsr4J/bD+Pi+FNKl+FXg65K6zqcX/ABNLiF/ntLWXOYsg5EkmBv4+RD/tV71+0H8atP8Agv4Lk1FSk+u6iHh0q2PVpCDumfkYSPB59ttfh5q+rahrmqXetarPJc319NJcXE7Nl5Hl+dm57DoPYVOEwfNPmmfj/ibxssHS/szCfHL4iiBiiiivXP5rbuFFFFAgooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD/9P8P6KKKACiiigAooooAKKKKACiiigAooooAKKKKAENdz8K/wDkpXhT/sM2X/oyuGNdz8K/+Sl+FP8AsM2X/owV6GV/71T/AMRrQ+M/a4dKnXv+H86h/h/L+dSg/riv7oq/AvQ+7ojv4qtdx/ntVX+Kpf4K8jEnpUyePjB+tEdOh7U1fvZrycTudlMnHQ1PH901AKnXvXiYjc9KBIvWvhP9uv8A5F/wr/1/3H/oFfdi/wBK+EP26ePDvhQf9P8Ac/8AoFfFcXf8i6qcmf8A/IumfnFRRRnHWvwQ/Gh8UUlxIkEETyTSERxxouSSTgAD3NfsN+zN8E4/hX4UXVtZiU+Jtbhjku9y/wDHtCQHS2HA6YXf/t/7tfNX7H/wO/ta9j+K/ii3Js7R5Bo9vImPMm4zc4IHyJz5f+19K/S8Ak4Nfb8P5Pyw+tVz9W4E4ft/ttckTv8ASheoqPt+X9KkH3x9a9vEbH65TLH8X5VJH0FRfw1LH0rxsSelAfHU4+6fr/U1BHT16fj/AI142I3O6mWB3qRfu1GMdqkX7teNidzupsVe1cx408ZaD4A8K6h4s8STLBY6fCZGz993AOyJP9uQ/J/wKujmmhtoHnuGWGKFC7ys3yIgr8bf2qPjzcfFTxM3hrQZmHhbRZmS3+bH224XKPO/OCAf9Xkfd+teX7PmZ8/xjxXTybA8/wBt/CeM/Fj4ma98XfGl74t1liiPlLO3Dfu7W1i+5Eg6AkdT/G7V5oD82aOlL05rtSsfyPjMTPE1516/xhRQOgooOcKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/9T8P6KKKACiiigAooooAKKKKACiiigAooooAKKKKADuP89q7n4Xf8lN8Jf9hmy/9GVw3cf57V3Pwu/5Kb4S/wCwzZf+jK9DK/8Aeqf+I1ofGftcOn5fypx4OKaOn5fyqQdK/un7C9D7mkSHv9RUo+7Vfuf89qmXqfpXlYjc9CmWY+v502PtTE7U8da8eqz0KZMeoqzVY9RUy/4/zrx656UB3UYr4U/bp/5F7wmf+n64/wDRdfeH8WK+E/26/wDkXvCv/X/cf+gV8Rxh/wAiuscmf/8AIumfm/x3r2/4D/CG/wDi74yjsGRo9Gsv3+pzleBCMHy0OD88mRs9vm7V5R4b8Par4t12x8M6JC1xfahKkUUeM456n2HU/wC7X7Z/CX4Y6V8KfBtp4Y00K9yFEt/c7cme6x+8fOB8mf8AV/7C1+S8P5O8XU55/AfC8L5I8bX56nwHomnWFjo+n22l6dCtvaWkMdvBGq/KqRAIi1pZ+aq46CrCP8uDX6TUpcisj9ypRjTXIh46VIvamJ1/z7Uq/eryMSejhtyyeopU42/Sov4Kk+TcK8bEbnqUx8fU/WrMX+sz6VWj6n61ZH3sV4+I3PQpjkHNWE/pUHvXzl+0n8cLP4QeD3t9NlRvEmrQyQ6ZD3hVgQ1xIMggR87D/G47/MK8XEE4/NqOAw88VX+CJ4B+2J+0D9ign+EPgu7xcyp/xPLuFv8AVxnP+jRkHgyceZ3AO3j5q/MwYxjHFWLu8u9Qu572+ka4uLl5JppXbe7yS/feq/TPtxXKfyfxHn9fNsdPE1thaKKKD58KKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD//1fw/ooooAKKKKACiiigAooooAKKKKACiiigAooooAO4/z2rufhd/yU3wl/2GbL/0ZXDdx/ntXcfC/j4meEz/ANRmy/8ARlehlf8AvVP/ABGtD4z9rF61Iv3qgT/WCp1+9X92/YXofbU2Sj7v4VYiquPu1YSvLxO56VLcVf8AP6089qYv+f1p79K8SoejTJf4anjOWqFOtOj6k15Nc74Itp9/HvXwf+3OCfDvhPHJ+33GAOSfk9K+6ufSvO/Gfw30zx9rvhy/1zbLZeHp5rz7Nt/18/Gzfx9yPLP/AMBr5HiTB1MXhZUYEZnhJ4jCzoUzw79lP4IDwToi+PfEduBrmrxfuEkXL2drLg4HHDyfnzs/vV9jA84/zxUSAABR9KkQbv8A9Vefhsvhg6HsYHr5VgqeDoexgSL1Aqde1Qr9+nDrXNiT3aZYHU/h/SnfxUxaefvCvGxG530iSPtTj0FNHSnp9zHp/jXj4jc9SkSr2qx/Sq38NR3l5Z6bZT39/NHb21ojyyzyNsRI4uXd3rxsRsdsaiprmZy/xF+IGg/DXwje+LvEEoSG1Q+TEG+aefH7uFP9uSvwv+Ifj7XviZ4u1Dxh4imMl1eOfLhBOyKD/lnCgyQEjHX3r1r9o/443Xxh8WfZ9KkePwzpMjxWEIO3zSuQ9y/PLOc4/uJz1ya+bePpXz9Spzn86cdcVvM8R7Gj/BiOoo7ZorA/PAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD//1vw/ooooAKKKKACiiigAooooAKKKKACiiigAooooAO4/z2rufhd/yU3wl/2GbL/0ZXDdx/ntXc/C7/kpvhL/ALDNl/6Mr0Mr/wB6p/4jWh8Z+1K9al7mok7VKOpr+7anwr0PtaZKnQ/h/Onp1pijAP4fzp6da8zEHoUywOppyf400dTTk6n8a8eoejT3JV6UJ1ApV6fjS15Fc76ZcTkfhT14FQp0pw6V4+I2PQpk69KkPQio16VKe/4V42I2PQpix/eSpI+tMH3c1LH9415Fc9CmSx+tOPamx9qeegryMRuelAkj9PY1KO341HH0xUg+9XjVUelTHp0r82P2xfjyb2Wb4TeEbnEEZB1m6if/AFjcn7MmD9xBjzAR94bexr7p+KKeMH+Huu/8IEQuv/Y5fsW77+cHfs/29m7y/wDbxX4I3i3YvJhfeZ9pSSXz1mz5nm5PmeZnnOc18xmk3fkPzjxHz/EYejDBUNpEI46UUUV4Z+EBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB//X/D+iiigAooooAKKKKACiiigAooooAKKKKACiiigA7j/Pau5+F3/JTPCf/YZs/wD0ZXDdx/ntXc/C/wD5KX4T/wCwzZ/+jK9DK/8Aeof4jSh/ER+1CU8fe/z7VFHU7ZyCK/vCo/cXofaQLC9KQ/fFKvSkPXNeZXdzuplv+7/n1p61H/B/n1p6dPzrxq56UAiABOKnHQ1XH3jVgdDXk4jY9KkTx9v89qkSq6dqsp/SvGqnbSJVbnmpepz/AJ6VAnpU4rx8SenTHH7uakTqKZ/FTc4X/PrXj1TsplodAKlH3qjHb8aT+GvGxG56VLclXtU69agXtU/VhXj1z0qZInJ/Gvzf/bE+Apglm+LfhO1/dyMBrVrEmPLYZ/0hMD+MY8z/AGhu9a/RxOtF1aW2pWs9hfwrPa3CPDLFKu9HjlGx0f8ANq8HHUvaHDnuR0c0wv1eW/Q/nPByaXPOTxX0j+0l8Drj4O+LHuNKjaTw5q0kkunTDnyC2S9s/H3k5x/fTv1r5u9vWvnZw5T+Ycfl1bB150K3xC0UUVmcIUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB//Q/D+iiigAooooAKKKKACiiigAooooAKKKKACiiigA7j/Pau5+F/8AyUvwn/2GbP8A9GVw3cf57V3Pwv8A+SmeE/8AsM2f/oyvQyv/AHqn/iNaHxn7TR1On3agjqWPoTX96y+FH2cCXtUpHamL/DS9z/ntXmYg7qZZX7h/Gnp2qKNgMipRxXi4jc9KAqdfxqz6VB/F/n0qUfdFeTiNjupkg5rwH43fFef4Tav4P1eYM+lXt1c2mpQLz+5KDDp/txn5z/3zXv49K+Ev25gT4d8K/wDX/c/+i6+L4nxNTD4KdeBjm2InRwUqkD7p06/sNX0+21XTpluLS9iSW3kVvkZJQHRvrWinavzh/ZD+NrWFyvwr8UXGbaZpH0eaZv8AVS4G+25P8fPl/wC1x/FX6PJ2ry8qzSGOwntYbnpZLmEMdQ9rDckGc4NPTpTByakHQ/h/OpmfSUyZOhP0p4pidCPpTxXj1z0oEh+7ipe2ajfofpUn8FeViOp6VMnHakWnd0PpTl614lc7qaOL+I/gHQvib4Qu/CWvxAxXSEQyFfngnOdkqcfeQ1+F/wAQfAmu/Dbxbf8AhDxFCYrmzkO19p2TRZ/dzIcDKSDGfev6CenXj0r5r/aV+B9t8XvCUl5pcSL4l0hHksp8bDMgB327nBJEmTsH8D46ZY14GNp82qPh+PeEf7Tw/wBZoL34H4sUVPcQTWs8trdRtFPC5jkR12OkgODnIHQioK84/nBpp2YUUUUCCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//9H8P6KKKACiiigAooooAKKKKACiiigAooooAKKKKADuP89q7j4X8fEzwmf+ozZf+jK4Y13Pwr/5KV4U/wCwzZf+jK9DKv8Aeqf+I1ofGftMOoqdPvE1AlSJ981/ej+FH2FIsJ94/wCfSnp6UxepP0p6dfxrgqnbTJ06Cn9h9f60xO1O7D614uI2PTpEn8NTr0FQH7lTLwBXk4k9GmTjr+FfCv7c/Hh3wqP+n+4/9Ar7nR+nNfDH7c//ACL3hX/r/uP/AECvgeNP+RVM4s/1wU0fnLDLNbXEdxBK0UsBEkbpw6SKQQfXrX7B/s4/Gi2+KnhNbTVZU/4SLSY0S+XvMqgBbhBk/wCs43/7Z7fLX48ZFdr8PfHut/DbxZY+LNCk2S2sgEsLN8lxb5HmI44+STt71+K8P5vUwNfn+wfB8P5s8FiFL7B+8KDpUi9/rXIeBvGei+P/AAxY+KtAmEtreoG2/wAcUn8aP1+eOuvXjkV+szqqpS9pTP3XC1faU1Vpkq1OP61ApzU4rza56VPdD6lToPxqKnJ94/59K8euelTJx6fSl7k/Sok+7U3r+FeNiNj0oFg/dzTui03+GnjpXjYjY7qZ+cn7Y/wCMwm+L/g+2y4H/E7tI05br/pYAHUDbv7n73PzV+bee45zX9HNxbw3dtLa3Uay28yOjxSrvR4zX4y/tO/Aqb4ReKzqujRs3hfXJHeyk6/Z5WyXt39Ahz5f99ePUV5NXQ/CvEng72FX+08J8H2j5hoo7YorM/HgooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/0vw/ooooAKKKKACiiigAooooAKKKKACiiigAooooAT+Gu6+GH/JS/Cn/AGGLL/0ZXDDt+Fd18Mv+SmeE/wDsMWX/AKNr08r/AN7h/iLpfGftIO1SL1BqAc4/Gpl7V/en2EfZUiwOtPXr+NMHWpE6/j/SvOxHU7aZOnX8/wCdPH+NV061Zrxq7O+kKOtSL2qMdalXqBXk1z0aZOn8Jr4U/bl48N+Ff+whcf8AoFfdK8ivhj9ub/kXPCn/AGELj/0XXwfGv/Ismc2f/wDIumfnHx3oxRRX86H5MfUP7MPxrl+GvilfDmtysvhvWZESYM2VtbliPLuPxPEn+yenFfrrDIjoJY9ro4BGz3r+ec8V+nH7IXxuPiTTl+GXim4L6pp8W7TJpnybm1jxiLJOTJH/AAcfcHtX3HDGc2/2WofovBPEHs39Srs+5xwcDipo/WoQSDxUyjBr7LEdz9gpEi8R4qRX/SoR0NTF/p+VeJiNjtpj14XNTjoPw/nUC/e/H+lSL9zNeNiNj1KZIvUGrZ61VT0qwvSvHxB6FLcVfu1x/wAQPAuhfEnwne+EvEcAe1vF+RgMywSfwTJ/tx12CdKf0rxcRudNfC08RSeGqq5/P18Svh7r3wt8Y33hDX4yZbZyYJiv7m4t8/u5kOB8r9/f5a4DHvX7d/tI/A6y+MXg920+GKPxLpEbzaZct8hkwCXtnODmOTnYP4HI6cmvxMvLG6067uNOvoWt7q1kkhmilXY6SREh0f8ApWEKjR/KfG3C1TJsc4P+DL4SCijNFWfGBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB//0/w/ooooAKKKKACiiigAooooAKKKKACiiigAooooAB2/Cu6+GX/JTPCf/YYsv/RtcJ0xXdfDI/8AFy/CZ/6jFl/6Mr08r/3uH+Iul8Z+0C8KatDv9aqp96rCdK/ve3uI+vpEidKsL3/D+dV16f59asDv+H868/EbHZTHp2qb+KoV6U7+KvFrnqYfYnHWpB981Gvb/Pahe1ePiPiPQplletfC37cv/Iu+Ff8Ar/uP/QK+51+9Xwx+3N/yLnhX/sIXH/ouvheNl/wl1Tnz/wD5F0z85qKKK/nE/JRCO9aOkapqegala61pNw1tfWM0dzbzq2HR4j/I96zzxSdxTTsNXpv2iP2/+CHxY0z4t+CbfW4CqapbBINStl6xzjGc8/ck+/H9f9mvZMdK/Dv4K/FXUvhJ42ttdgLPp85EGo2iscTQHAzjIG+P76ewr9rtD1rT/EWk2mu6NOtxZX8MdxBMG6pLjg/Sv0vJs0+t0OWfxn7pwnnn12hyz+NG6DhqQfw1Hx61IO1bYjY+9pE/8Qp67NtRt0BqWPqPxrycRuehTJI+lTD7p+v9TUEdTr0b8K8PEbnoUyVeDmnr92ou4qRfufnXjYnc76e4/wC7+H61+d/7ZXwAOowyfFzwXaZuYk/4nlpCv+tTn/SQAOWj48zuQN3PzV+iCY2cdqSWGG4heCeNZYpEKPE6/I8Zry6k+U4eIeH6WcYGeGrbn82/BwVpeK+sP2p/gNP8J/FLeIdBhZvCutTM1swXItbhsu9u/AABP+r9V+lfJ3fHau2nU5z+P83yqvl2Knhq/wAUR1FGc0UHmIKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//1Pw/ooooAKKKKACiiigAooooAKKKKACiiigAooooAQ9vrXc/DL/kpXhT/sL2X/oyuG613Xwy/wCSleFP+wvZf+jK9PLP99o/4jSi7TP2dTtVmPpVaL+lWY+lf3zL4UfXUiVO1WF61VX7wq2tcGIOymJ3P+e1WB9yq/c/57VbirxsRuehTJEHNNHWmL1FPHWvIqnoUyePrXwz+3P/AMi/4V/6/rj/ANAr7mXv9a+GP25ePD3hb/r+uP8A0XXwHG//ACK5nNnf+5SPzmooor+cT8qCiiigBDn0r7j/AGRPjkfDWrJ8MPFFzjSdTkxpssrf8etzJj93nPEb849G443NXw5jvQpMZRoyVZSGDR8EEHIIPYg8/WuvB4ueHrqdM9PK8zqYHFLFUj+iOpa+Uv2W/jbF8SvCq+HdflU+JdFhSOQM3z3dsuESYf7eMLJ7/N/FX1b24r9BpYinXp88D+jMmzGnjcPCtRJz/CakVhmoP4KkH368/Ebn0VInXv8AWpYv9bVePr+NWB97FePiNz0aZY/i/wA+lCd6ZTh1/wA+lePXO6n8RKgyKevSmR+tP6gV42J3PSpnK+OfBOhfETwrfeD/ABLbiezv0K7l+/FJj926ejxmvwl+Knwy1z4T+Nb7wjr6/NDl7Sfb8l1b5PlyoeOR0P8Atrt9q/oHXnivn79oj4JaZ8Z/BcltbosPiLS1kl0q6IGTJglrd+DlJP8Axxvm4rkw+I5Kh+d+InBv9q4X6xh/40D8Msc0tX7/AE290rULjStTt5LW9s5pIZoJV2OskRw6P7dx7Vn8nkV7CP5TcXB8shaKKKCQooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/1fw/ooooAKKKKACiiigAooooAKKKKACiiigAooooAP8AGu4+GP8AyUrwoPXWLL/0bXD/AONdx8Mv+SleFP8AsMWX/oyvTyj/AHql/iNKex+zy8YqcdahH3h9anXrX99R+BH1dPZD0+9+NTr96oU/1tTL96uOqdsB/pVlKgH3asJXj4nc9GluOPRace1MX/P609+leJUPRpkg6D618Ofty/8AIueFf+whcf8AouvuNe1fDv7cn/Iv+Fv+v+4/9F18Hxv/AMiqscmef7jM/OSiiiv5tPywKKKKADjtRRRQB1PgbxhrPw/8U6f4u0GUx3llJuAJwjxn/WRv6pImVx6n8a/cT4Z/EHRPiZ4OsfFuiOPLuvlmgDfPBPgb4n5/5Z/1r8EPwr6T/Zs+Nk/wl8XCx1R93hzWGjhvY/vmFmwEuE/3CB5n99c+xr2Mox7w9Tll8B9pwbxG8BX9jX+CZ+za/dNPA+eqdtcQXdvFdWsiyQXKB0dG+R4+tW1BzX02Iab0P6NwzTimiVe1SjqKiPUVKO1eTXO2mWBndz6U09j6U4fe/Cg9P8+teNXO+A8cNj6VZXqKr/xVN/F+FeRXPRpki8dKUDNRL/n9alPQf5714uIPSpo/P39sn9n/APt2yn+LPg63J1Gxj/4m1pCuTcwRZJnxjPmRjbvx1Rd3avywB/Gv6TWSO4RoZFV0cEOpXh8//Wr8cv2s/gLJ8MPFB8WeG7dz4X1uUkBVylneMS7weyE/OnudueK68BjPsTP538VOCHTazXCL/F/mfINFFFeufg4UUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUfnQAUUUd80Af/W/D+ilJycduc/jSUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAH+Ndx8Mf+SleFB66xZf+ja4f3+ldx8Mf+Sl+E/8AsMWX/oyvUyhXxdH/ABGlPY/ZwHawH0qyvJqtkYGeKlXtX99X91H1dMuL2NSDvUCdqnHeuDEq7O6kPTofw/nT060xOh/D+dPTrXl10dtMsDqacPu03uacnXnivHqo9GG5KvAr4b/bk/5AHhX/AK/7j/0XX3In8Q9q+F/24P8AkAeFP+v+5/8AQK+D420ymszlzr/c5n500UUV/NB+XBRRRQAUUUUAFBAx07Yoo7UDTtqfpN+xz8dTeQR/CfxbcZnhUjRbiZv9ZHxutySeTHy0ffA28/LX6EqMYFfzuafqF9pN/b6rp8skF1ZvHNDMjbXV4iNjJ/Wv2q/Z8+Mtl8YPBMd7Owj1zTgltqVuMZaQY2TJ/sSAk/htr6PK8ZzQ9nM/bOAOJfbw/s6u/ePex0NWVquv8NWFrfEH7DTJh1/Og9P8+tNT/GnDmvExL1O+n0JJOoqT+KmHk07+GvLxGx6NMkHSpajH3RUleNiNj0obEYGG3VzXjDwpoXjrwzqHhTxLAJ7DUojFKP40OPkdP9uM/PXTv8y/IenamZQptbvXlSdjGvQp16Tp1NmfgH8Xfhdr/wAHPG134S1hTLGMy2F0q/JdWkudkqcAZA4P9x1ry7GeO1fut8f/AIL6d8Z/BUuj4SLXtNV7jSbphgq4B3Qvx/q5emPfdX4e6tpWpaDq93ousW8lpfWMz29xBKhDxvF8jrz2I5B9K+iwGKdeFmfyLx7wfPJMc3D+DL4TNooor0T8/CiiigAooooAKKKKACiiigAooooAKOe9FHf/ACKACiiigD//1/w/ooooAOlFHvRQAUUUUAFFFFABRRRQAUUUUAFFFFABXcfDMj/hZfhP/sMWX/oyuHq1p+oXulX1vqenyNBdWcyXEMv9ySP5keuvA4j2NeExp2Z+5AB9c1PyP/1V+Q3/AAv74w/9DNc/9+0/+IpP+F/fGP8A6Ge5/wC/a/8AxNf0t/xG/KkkvYy/A9x4+CP2D/CplHNfjz/w0D8Yx/zMtz/3wn/xFL/w0J8Y+/iSf/v3H/8AE1lU8asst/BkbU84pxP2JQEDFPQetfjl/wANCfGb/oZrn/vhP/iKf/w0L8Zh/wAzRc/98L/8RXDU8ZMsf/LmRvT4gpdj9kU7CpVAPavxr/4aI+Mw6eJ7j/v3F/8AE0f8NE/Gn/oZrj/v3H/8TXFV8V8v/wCfcjrpcRUorY/ZZckdO9fC/wC3EP8Ain/Cv/X5c/8AouvlM/tEfGr/AKGi5/79p/8AEVx/i74m+PPH1vbW3i3VX1GK0Z3iR0UbXGf7iV8rxHx/hcwwU8LCEveMsfxBRrUJ0II4aiiivyg+PCiiigAooooAKKKKAG16f8IvidrXwn8a2fivSiZIk/c3lru/dz2sn31xkDI/5Z+jrXmJ6ZIoPpWlOpynTh8TUoVPbwP6FPCnijRfGOgWPiXQJ1ubDUoRLHIvUE9Uf0eP/wBlrplGPWvwP8JfGP4meA9ObR/CXiG6sLBn837OCsiJJ7b+ldZ/w0z8dP8Aoa7r/v3F/wDE16s8fCaP2nBeKeGVNKtTlzn7kKD2qQZHSvw2/wCGnPjr/wBDZc/9+4v/AImmH9pv47/9DZc/9+4//ia4J1+c9GHi1l63hI/dBfu07GE/Gvww/wCGnPjz/wBDXc/98R//ABNO/wCGnvjx/wBDddf98Rf/ABNcVWmdEPF/L4f8u5H7oYqToPpX4Vf8NQ/Hnv4uuf8Av3F/8TR/w1F8ee3i25/79xf/ABNcc8HNnTT8aMt0/cy+8/dTJ29DRz6V+Fv/AA1L8eu3i66/79xf/E0f8NSfHr/obrn/AL9xf/E1w1Mnn1Oj/iNWV/8APmZ+57hx86qfyr4P/bG/Z9l8V6Y/xX8I2pOsadAf7Wt4UJN3axZPm4AyZI0+/wD7C/7NfEQ/ak+Px/5m25/79x//ABNNl/ai+PUiPG/i25AkGH3RxEMD1/hrPD5ZiaNRThM8HiHxNyTN8FPDVqMrnggOaKkuJ3u7iW5m2b5XLvsXZyTk/Inqajr6E/AnboFFFFAgooooAKKKKACiiigAooooAKCRRR/+ugAooooA/9D8P6KKKAFz60lFBOaACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACkwKWigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKADGKKKKADGaKKKAEwKXGKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo9xRQOORQAUUUUAf/0fw/ooooAO2aOMcUcd6UkEYFACUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFBHpRR35FABRRRQB//9L8P6KKKACjGKKU4zxQAlFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFACgZNJ9aUZz+FJQAUUUUAf/0/w/ooooAOCcGj9aKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigBRzxSZoooAKKKKAP/9T8P6KKKACjviiigAoopaAEooooAKKKKACiiigAoo+lKBmgBKKMge9HHagAo57UUEelABS4xSfWigAopfT0o6DNACUUDrR1P1oAKKO9FABRRR/nigAopcYpKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiijtQAUUooxnPGaAEopRxzScdelAC98ZxSUc+nUn8M0pGDigBKKKKAP/V/D+iiigAooooAKKKKAD69KOM4FFHbFABRz3o96OcYoAUgjrQfSkPNGMUAHajpRRQAfSjjvRRQAUUUUAFFFFABRRRQAdaKKKACiiigApc8fSkooAKUevrSUUAFKAD3pKKAAc0Uf56Uc0AFFHPcZHpRx3FABRRx2pSMUAJRRRQAUUUUAFFFFABRRz6UpFACUUfWjj1oAKKUHAzScmgAooooAWkoooAD6cUuTjFJRQAc/5FHPrRRQAUUUUAFFFFABRRRQB//9b8P6KKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAEz2paO2KKACiiigAooooAKKKKACiijNABRRxRQAUUUUAFFFFABx3o570UUAFKMjg0lFAB+f5UcfWjnvRQAoGeBSUYzRQAUcd6KKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/2Q==';

    // Créer un nouvel élément link pour le favicon
    const link = document.createElement('link');
    link.href = newFavicon;
    link.rel = 'icon';

    // Ajouter le nouvel élément link à la tête du document
    document.head.appendChild(link);

    // Supprimer l'ancien favicon
    const oldLink = document.querySelector("link[rel='icon']");
    if (oldLink) {
        oldLink.remove();
    }
})();
