// ==UserScript==
// @name        Test - Chat
// @description New features for Kindroid's chat page
// @namespace   https://gitlab.com/breatfr
// @match       https://kindroid.ai/home*
// @match       https://kindroid.ai/groupchat*
// @match       https://kindroid.ai/voicecall*
// @version     2.0.0
// @homepageURL https://gitlab.com/breatfr/kindroid
// @supportURL  https://discord.gg/fSgDHmekfG
// @author      BreatFR
// @copyright   2023, BreatFR (https://breat.fr)
// @grant       none
// @icon        https://gitlab.com/breatfr/kindroid/-/raw/main/images/icon_kindroid.png
// @license     BY-NC-ND; https://creativecommons.org/licenses/by-nc-nd/4.0/
// ==/UserScript==

(function() {
    'use strict';

    // Design
    var style = document.createElement('style');
    style.textContent = `
        /* Hide icons at the right of header */
        .css-x3odei > div:nth-of-type(2),
        .css-z0osps > div > div:nth-of-type(2) {
          display: none;
          height: 0;
          width: 0;
        }

        #chat-script-button {
          background-color: transparent;
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' fill='none' stroke='%23cbcbcb' stroke-width='32' stroke-linejoin='round'%3E%3Cpath d='M368 128h80m-384 0h240m64 256h80m-384 0h240m-96-128h240m-384 0h80' stroke-linecap='round'/%3E%3Ccircle cx='336' cy='128' r='32'/%3E%3Ccircle cx='176' cy='256' r='32'/%3E%3Ccircle cx='336' cy='384' r='32'/%3E%3C/svg%3E");
          background-repeat: no-repeat;
          background-position: center center;
          border: none;
          cursor: pointer;
          display: block;
          height: 40px;
          position: fixed;
          top: 10px;
          transition: opacity 0.3s ease;
          width: 40px;
        }

        #chat-script-button:hover {
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'%3E%3Cg fill='none' stroke='url(%23A)' stroke-width='32' stroke-linejoin='round'%3E%3Cpath d='M368 128h80M64 128h240M368 384h80M64 384h240M208 256h240M64 256h80' stroke-linecap='round'/%3E%3Ccircle cx='336' cy='128' r='32'/%3E%3Ccircle cx='176' cy='256' r='32'/%3E%3Ccircle cx='336' cy='384' r='32'/%3E%3C/g%3E%3Cdefs%3E%3ClinearGradient id='A' x1='1.6' y1='25.2' x2='29.2' y2='0' gradientUnits='userSpaceOnUse'%3E%3Cstop offset='.208' stop-color='%238b6dff'/%3E%3Cstop offset='1' stop-color='%23fe8484'/%3E%3C/linearGradient%3E%3C/defs%3E%3C/svg%3E");
        }

        #chat-script-menu {
          background-color: var(--chakra-colors-secondaryBlack);
          border-radius: 0 0 1em 1em;
          box-shadow: 0px 0px 5px rgba(139, 109, 255, 0.8),
                      0px 4px 10px rgba(139, 109, 255, 0.6),
                      0px 8px 15px rgba(139, 109, 255, 0.4),
                      0px 12px 20px rgba(254, 132, 132, 0.4),
                      0px 16px 25px rgba(254, 132, 132, 0.3);
          color: var(--chakra-colors-secondaryWhite);
          display: none;
          font-size: 1.2rem;
          height: auto;
          padding: 10px 0;
          position: fixed;
          right: 16px;
          text-align: left;
          top: 60px;
          width: max-content;
          z-index: 99999;
        }

        .chat-script-item {
          align-items: center;
          cursor: pointer;
          display: flex;
          justify-content: space-between;
          padding: 5px 10px;
          width: 100%;
        }
        .chat-script-item:hover {
          background: linear-gradient(88.55deg, rgb(139, 109, 255) 22.43%, rgb(254, 132, 132) 92.28%);
          background-clip: text;
          -webkit-background-clip: text;
          color: transparent;
        }
        .toggle-icon {
          height: 40px;
          margin-left: 10px;
          vertical-align: middle;
          width: auto;
        }

        .formatted-date {
          background: linear-gradient(88.55deg, rgb(139, 109, 255) 22.43%, rgb(254, 132, 132) 92.28%);
          background-clip: text;
          -webkit-background-clip: text;
          color: transparent;
          font-family: var(--chakra-fonts-PoppinsRegular);
          font-size: 1.3rem;
          font-weight: normal;
          left: calc(50% - 10px);
          margin-bottom: 5px;
          position: absolute;
          transform: translateX(-50%);
          white-space: nowrap;
        }

        /* Customization of voicecall page */


        @media screen and (max-width: 1400px), (orientation: portrait) {
          #chat-script-button {
            height: 36px;
            top: 5px;
            width: 36px;
          }

          #chat-script-menu {
            right: 16px !important;
            top: 48px;
            width: calc(100% - 32px) !important;
          }

          .formatted-date {
            font-size: small;
            left: auto;
            right: 10px;
            transform: none;
          }
        }
    `;
    document.head.appendChild(style);

    // Function to set data in localStorage
    function setLocalStorage(name, value) {
        localStorage.setItem(name, value);
    }

    // Function to get data from localStorage
    function getLocalStorage(name) {
        return localStorage.getItem(name);
    }

    // Load user preferences from localStorage
    var animatedAvatarChangerEnabled = getLocalStorage('animatedAvatarChangerEnabled') === 'true';
    var autoConfirmEnabled = getLocalStorage('autoConfirmEnabled') === 'true';
    var autoContinueEnabled = getLocalStorage('autoContinueEnabled') === 'true';
    var autoFocusEnabled = getLocalStorage('autoFocusEnabled') === 'true';
    var blurContentEnabled = getLocalStorage('blurContentEnabled') === 'true';

    // Variables globales
    let menuVisible = false;
    let toggleIcons = {
        toggleOffUri: "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' height='30px' viewBox='0 0 1000 1000' fill='%23c5000f'%3E%3Cpath d='M753.4 287.4H246.7c-101.8 0-184.2 95.2-184.2 212.8S145 712.9 246.7 712.9h506.6c101.7 0 184.1-95.2 184.1-212.7s-82.4-212.8-184-212.8zm-22.8 389.1H269.4c-92.6 0-167.7-78.9-167.7-176.3s75.1-176.3 167.7-176.3h461.3c92.7 0 167.7 78.9 167.7 176.3s-75.1 176.3-167.8 176.3zM441.2 500.1c0 73.9-59.9 133.9-133.9 133.9s-133.9-59.9-133.9-133.9c0-73.9 59.9-133.9 133.9-133.9s133.9 60 133.9 133.9zM634 449.8c-3.8-6.3-8.7-11.2-14.7-14.8-6-3.5-13-5.3-21.2-5.3s-15.3 1.8-21.2 5.3c-6 3.5-10.9 8.5-14.7 14.8s-6.7 13.8-8.6 22.5-2.8 18-2.8 28.1.9 19.4 2.8 28c1.9 8.5 4.7 15.9 8.6 22.2 3.8 6.3 8.7 11.2 14.7 14.7s13 5.3 21.2 5.3 15.3-1.8 21.2-5.3c6-3.5 10.9-8.4 14.7-14.7s6.7-13.6 8.6-22.2c1.9-8.5 2.8-17.9 2.8-28s-.9-19.5-2.8-28.1c-1.9-8.7-4.8-16.2-8.6-22.5zm-36 97.6c-15.8 0-23.7-15.7-23.7-47s7.9-47 23.8-47c8 0 13.9 3.9 17.7 11.8 3.8 7.8 5.7 19.6 5.7 35.2 0 31.4-7.8 47-23.5 47zM663.7 432v136.3c3.7.8 7.5 1.1 11.2 1.1 3.4 0 7.1-.4 10.9-1.1v-57h31.5c.6-3.9.9-7.9.9-11.9 0-3.8-.3-7.7-.9-11.7h-31.5v-32h39.7c.6-3.8.9-7.8.9-11.9a74.58 74.58 0 0 0-.9-11.6h-61.8v-.2zm76.8 0v136.3c3.7.8 7.5 1.1 11.2 1.1 3.4 0 7.1-.4 10.9-1.1v-57h31.5c.6-3.9.9-7.9.9-11.9 0-3.8-.3-7.7-.9-11.7h-31.5v-32h39.7c.6-3.8.9-7.8.9-11.9a74.58 74.58 0 0 0-.9-11.6h-61.8v-.2z'/%3E%3C/svg%3E",
        toggleOnUri: "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' height='30px' viewBox='0 0 1000 1000' fill='%232dd55b'%3E%3Cpath d='M753.3 287.4H246.6c-101.7 0-184.1 95.2-184.1 212.8 0 117.5 82.5 212.7 184.1 212.7h506.6c101.8 0 184.2-95.2 184.2-212.7.1-117.6-82.4-212.8-184.1-212.8zm-22.7 389.1H269.4c-92.7 0-167.7-78.9-167.7-176.3s75-176.3 167.7-176.3h461.3c92.6 0 167.7 79 167.7 176.3s-75.2 176.3-167.8 176.3zm-38-310.2c-73.9 0-133.9 59.9-133.9 133.9 0 73.9 59.9 133.9 133.9 133.9s133.9-59.9 133.9-133.9-59.9-133.9-133.9-133.9zm-386.4 78.2c-3.8-6.3-8.7-11.2-14.7-14.8-6-3.5-13-5.3-21.2-5.3s-15.3 1.8-21.2 5.3c-6 3.5-10.9 8.5-14.7 14.8s-6.7 13.8-8.6 22.5-2.8 18-2.8 28.1.9 19.4 2.8 28c1.9 8.5 4.7 15.9 8.6 22.2 3.8 6.3 8.7 11.2 14.7 14.7s13 5.3 21.2 5.3 15.3-1.8 21.2-5.3c6-3.5 10.9-8.4 14.7-14.7s6.7-13.6 8.6-22.2c1.9-8.5 2.8-17.9 2.8-28s-.9-19.5-2.8-28.1-4.7-16.1-8.6-22.5zm-35.9 97.7c-15.8 0-23.7-15.7-23.7-47s7.9-47 23.8-47c8 0 13.9 3.9 17.7 11.8 3.8 7.8 5.7 19.6 5.7 35.2 0 31.3-7.8 47-23.5 47zm126.1-115.5v86.8l-42.1-86.8c-3.2-.8-6.4-1.1-9.4-1.1-2.8 0-5.8.4-9 1.1V563c3.1.8 6.4 1.1 9.7 1.1s6.6-.4 9.9-1.1v-86.6L398 563c3.3.8 6.4 1.1 9.2 1.1 2.6 0 5.6-.4 8.8-1.1V426.7c-2.9-.8-6-1.1-9.1-1.1-3.3 0-6.8.4-10.5 1.1z'/%3E%3C/svg%3E"
    };

    // Fonction pour créer le bouton des paramètres
    function createSettingsButton() {
        const settingsBtn = document.createElement('button');
        settingsBtn.ariaLabel = 'Chat script settings';
        settingsBtn.id = 'chat-script-button';
        settingsBtn.title = 'Chat script settings';
        settingsBtn.dataset.priority = 1;
        document.body.appendChild(settingsBtn);

        settingsBtn.addEventListener('click', toggleMenu);
        settingsBtn.addEventListener('touchstart', function (e) {
            e.preventDefault();
            toggleMenu();
        });

        return settingsBtn;
    }

    // Fonction pour ajouter une icône
    function addIcon(iconId, priority) {
        const isMobile = window.matchMedia("(max-width: 1400px), (orientation: portrait)").matches;
        // Récupérer le bouton existant par son ID
        const iconButton = document.getElementById(iconId);
        if (!iconButton) {
            console.error(`Le bouton avec l'ID ${iconId} n'existe pas.`);
            return;
        }

        // Créer ou mettre à jour l'icône dans un tableau des priorités
        let icons = [];

        // Récupérer tous les boutons existants
        const buttons = document.querySelectorAll('button[id]');
        buttons.forEach(button => {
            const buttonId = button.id;
            const buttonPriority = parseInt(button.dataset.priority) || 0; // Récupère la priorité ou 0 par défaut
            icons.push({ id: buttonId, priority: buttonPriority });
        });

        // Vérifier si l'icône actuelle est déjà dans le tableau, et la mettre à jour
        const existingIconIndex = icons.findIndex(icon => icon.id === iconId);
        if (existingIconIndex !== -1) {
            icons[existingIconIndex].priority = priority; // Met à jour la priorité si déjà existante
        } else {
            icons.push({ id: iconId, priority: priority }); // Sinon, ajouter l'icône
        }

        // Trier les icônes par priorité croissante (priorité 1 est la plus à droite)
        icons.sort((a, b) => a.priority - b.priority);

        // Calculer la position basée sur l'ordre trié
        let occupiedSpace = isMobile ? -11 * window.innerWidth / 100 : 16; // 11% de la largeur de l'écran pour mobile
        const spacing = isMobile ? 10 : 20; // Espacement de 10px pour mobile, 20px pour desktop
        console.log("Initial occupiedSpace: ", occupiedSpace);

        icons.forEach(icon => {
            const button = document.getElementById(icon.id);
            if (button) {
                console.log(`Placement pour ${icon.id} avant ajustement: ${occupiedSpace}px`);
                button.style.right = isMobile ? `${Math.abs(occupiedSpace)}px` : `${occupiedSpace}px`; // Positionner le bouton
                occupiedSpace += 40 + spacing; // 40px pour l'icône + espacement dynamique
                console.log(`OccupiedSpace après ${icon.id}: ${occupiedSpace}px`);
            }
        });
    }
    addIcon("chat-script-button", 1);

    // Fonction pour gérer la visibilité du menu
    function toggleMenu() {
        const dropdownMenu = document.getElementById('chat-script-menu');
        menuVisible = !menuVisible;
        dropdownMenu.style.display = menuVisible ? 'block' : 'none';
    }

    function isMobile() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || window.matchMedia("(max-width: 1400px), (orientation: portrait)").matches;
    }

    // Fonction pour créer le menu déroulant et ses options
    function createDropdownMenu() {
        const dropdownMenu = document.createElement('div');
        dropdownMenu.id = 'chat-script-menu';

        const options = [
            {
                action: (enabled) => { animatedAvatarChangerEnabled = enabled; animatedAvatarChanger(); },
                hideOnMobile: true,
                key: 'animatedAvatarChangerEnabled',
                label: 'Animated Avatar Changer'
            },
            {
                action: (enabled) => { autoConfirmEnabled = enabled; autoConfirm(); },
                key: 'autoConfirmEnabled',
                label: 'Auto Confirm Regenerate'
            },
            {
                action: (enabled) => { autoContinueEnabled = enabled; autoContinue(); },
                key: 'autoContinueEnabled',
                label: 'Auto Continue cut-off message'
            },
            {
                action: (enabled) => { autoFocusEnabled = enabled; autoFocus(); },
                key: 'autoFocusEnabled',
                label: 'Auto Focus Textarea'
            },
            {
                action: (enabled) => { blurContentEnabled = enabled; blurContent(); },
                key: 'blurContentEnabled',
                label: 'Blur Content'
            }
        ];

        const visibleOptions = isMobile()
            ? options.filter(option => !option.hideOnMobile)
            : options;

        visibleOptions.forEach(option => {
            const item = document.createElement('div');
            item.className = 'chat-script-item';
            item.innerText = option.label;

            const currentValue = getLocalStorage(option.key) === 'true';
            const toggleIcon = document.createElement('img');
            toggleIcon.setAttribute('aria-label', currentValue ? 'Enabled' : 'Disabled');
            toggleIcon.className = 'toggle-icon';
            toggleIcon.src = currentValue ? toggleIcons.toggleOnUri : toggleIcons.toggleOffUri;

            item.appendChild(toggleIcon);
            item.addEventListener('click', () => handleOptionClick(option, toggleIcon));
            dropdownMenu.appendChild(item);
        });

        document.body.appendChild(dropdownMenu);
    }

    // Fonction pour gérer le clic sur une option
    function handleOptionClick(option, toggleIcon) {
        const currentValue = getLocalStorage(option.key) === 'true';
        const newValue = !currentValue;
        setLocalStorage(option.key, newValue);
        toggleIcon.setAttribute('aria-label', newValue ? 'Enabled' : 'Disabled');
        toggleIcon.src = newValue ? toggleIcons.toggleOnUri : toggleIcons.toggleOffUri;

        if (option.key === 'animatedAvatarChangerEnabled') {
            animatedAvatarChangerEnabled = newValue;
            toggleAnimatedAvatarChanger(newValue);
        } else if (typeof option.action === 'function') {
            option.action(newValue);
        }

        console.log(`${option.label}: ${newValue}`);
    }

    // Fonction pour gérer les clics en dehors du menu
    function handleOutsideClick(settingsBtn, dropdownMenu) {
        document.addEventListener('click', function (event) {
            const isClickInsideMenu = dropdownMenu.contains(event.target);
            const isClickInsideButton = settingsBtn.contains(event.target);
            if (!isClickInsideMenu && !isClickInsideButton) {
                menuVisible = false;
                dropdownMenu.style.display = 'none';
            }
        });
    }

    // Initialisation de l'interface utilisateur
    (function initChatPCUI() {
        const settingsBtn = createSettingsButton();
        addIcon("chat-script-button", 1);
        createDropdownMenu();
        const dropdownMenu = document.getElementById('chat-script-menu');
        handleOutsideClick(settingsBtn, dropdownMenu);
        console.log("Interface utilisateur créée et ajoutée au document.");
    })();

    // Vérification des liens vidéos
    function isValidVideoUrl(url) {
        // Liste des extensions vidéo courantes
        const videoExtensions = ['ogg', 'mkv', 'mov', 'mp4', 'webm'];

        // Vérifie si l'URL se termine par une extension vidéo connue
        if (videoExtensions.some(ext => url.toLowerCase().endsWith('.' + ext))) {
            return true;
        }

        // Vérifie si l'URL contient des paramètres qui suggèrent une vidéo
        if (url.includes('video') || url.includes('ogg') || url.includes('mkv') || url.includes('mov') || url.includes('mp4') || url.includes('webm')) {
            return true;
        }

        // Vérifie si l'URL provient de services de stockage connus
        if (url.includes('firebasestorage.googleapis.com') || url.includes('storage.googleapis.com')) {
            return true;
        }

        return false;
    }

    // Fonction pour appliquer ou retirer le style personnalisé
    function applyCustomStyle() {
        const existingStyle = document.getElementById('customAvatarStyle');
        if (existingStyle) {
            existingStyle.remove();
        }

        if (animatedAvatarChangerEnabled && window.location.href.includes('https://kindroid.ai/voicecall/')) {
            const customStyle = document.createElement('style');
            customStyle.id = 'customAvatarStyle';
            customStyle.textContent = `
              .css-1nbfxil {
                height: 100vh !important;
                margin-top: 0 !important;
                width: 100% !important;
              }
              .css-1jbvs8u,
              .css-dv4mhk {
                aspect-ratio: 1 / 1 !important;
                background: none !important;
                border: none !important;
                border-radius: 1em !important;
                box-sizing: border-box !important;
                height: 84vh !important;
                margin: auto !important;
                width: auto !important;
              }
              .css-1jbvs8u img,
              .css-1jbvs8u video {
                border: none !important;
                border-radius: 1em !important;
                height: 100% !important;
                margin-top: 0 !important;
                max-width: none !important;
                object-fit: contain !important;
                position: absolute !important;
                top: 0 !important;
                left: 50% !important;
                transform: translateX(-50%) !important;
                width: auto !important;
              }
              .css-bdvhoj {
                margin-bottom: 0 !important;
              }
          `;
          document.head.appendChild(customStyle);
        }
    }

    // Animated Avatar Changer
    function animatedAvatarChanger(forcePrompt = false) {
        if (animatedAvatarChangerEnabled) {
            let talkingVideoUrl = getLocalStorage('talkingVideoUrl');
            let idleVideoUrl = getLocalStorage('idleVideoUrl');

            if (forcePrompt || !talkingVideoUrl || !idleVideoUrl) {
                alert("You need to provide two video URLs:\n" +
                    "1. One for when your kin is talking.\n" +
                    "2. One for when your kin is waiting.\n\n" +
                    "Example:\nhttps://example.com/talking.mp4\nhttps://example.com/idle.mp4\n\n" +
                    "Please ensure both URLs are valid video links.");

                let isValid = false;
                while (!isValid) {
                    talkingVideoUrl = prompt("Enter the URL for the 'talking' video:", talkingVideoUrl || '');
                    if (!talkingVideoUrl) return;

                    idleVideoUrl = prompt("Enter the URL for the 'idle' video:", idleVideoUrl || '');
                    if (!idleVideoUrl) return;

                    if (!isValidVideoUrl(talkingVideoUrl)) {
                        alert("The 'talking' video URL doesn't seem to be valid. Please try again.");
                    } else if (!isValidVideoUrl(idleVideoUrl)) {
                        alert("The 'idle' video URL doesn't seem to be valid. Please try again.");
                    } else {
                        isValid = true;
                    }
                }

                setLocalStorage('talkingVideoUrl', talkingVideoUrl);
                setLocalStorage('idleVideoUrl', idleVideoUrl);
            }

            function changeVideosHome() {
                const container = document.querySelector('.css-owh929');
                if (!container) return false;

                const img = container.querySelector('img');
                const videoElements = container.querySelectorAll('video');
                if (videoElements.length < 2) return false;

                const talkingVideo = videoElements[0];
                const idleVideo = videoElements[1];

                // Save original sources if not already done
                if (!getLocalStorage('originalTalkingSrc')) {
                    setLocalStorage('originalTalkingSrc', talkingVideo.src);
                    setLocalStorage('originalIdleSrc', idleVideo.src);
                }

                // Update only the src attributes
                talkingVideo.src = talkingVideoUrl;
                idleVideo.src = idleVideoUrl;

                function ensureVideoPlay() {
                    videoElements.forEach(video => {
                        if (video.paused && getComputedStyle(video).display !== 'none') {
                            video.play().catch(e => console.log("Couldn't play video:", e));
                        }
                    });
                }

                function toggleVideoPlay() {
                    if (img.classList.contains('css-1age63q')) {
                        // Image is showing, switch to videos
                        img.classList.remove('css-1age63q');
                        img.classList.add('css-9bmz2t');
                        videoElements.forEach(video => {
                            video.style.display = '';
                        });
                        ensureVideoPlay();
                    } else {
                        // Videos are showing, switch to image
                        img.classList.remove('css-9bmz2t');
                        img.classList.add('css-1age63q');
                        videoElements.forEach(video => {
                            video.style.display = 'none';
                            video.pause();
                        });
                    }
                }

                container.addEventListener('click', toggleVideoPlay);
                document.addEventListener("visibilitychange", function() {
                    if (!document.hidden && img.classList.contains('css-9bmz2t')) {
                        ensureVideoPlay();
                    }
                });

                setInterval(ensureVideoPlay, 500);
                ensureVideoPlay();

                return true;
            }

            function changeVideosVoiceCall() {
                let isObserving = false;

                function updateVideoSources() {
                    const container = document.querySelector('.css-1jbvs8u');
                    if (!container) return false;

                    const videoElements = container.querySelectorAll('video');
                    if (videoElements.length < 2) return false;

                    const talkingVideo = videoElements[0];
                    const idleVideo = videoElements[1];

                    // Save original sources if not already done
                    if (!getLocalStorage('originalTalkingSrc')) {
                        setLocalStorage('originalTalkingSrc', talkingVideo.src);
                        setLocalStorage('originalIdleSrc', idleVideo.src);
                    }

                    // Update only if sources are different
                    if (talkingVideo.src !== talkingVideoUrl) {
                        talkingVideo.src = talkingVideoUrl;
                    }
                    if (idleVideo.src !== idleVideoUrl) {
                        idleVideo.src = idleVideoUrl;
                    }

                    videoElements.forEach(video => {
                        if (video.paused && getComputedStyle(video).display !== 'none') {
                            video.play().catch(e => console.log("Couldn't play video:", e));
                        }
                    });

                    return true;
                }

                function startObserving() {
                    if (isObserving) return;

                    const observer = new MutationObserver((mutations) => {
                        for (let mutation of mutations) {
                            if (mutation.type === 'childList') {
                                if (updateVideoSources()) {
                                    console.log("Videos updated");
                                }
                            }
                        }
                    });

                    observer.observe(document.body, {
                        childList: true,
                        subtree: true
                    });

                    isObserving = true;

                    // Stop the observer after 5 minutes to prevent it from running indefinitely
                    setTimeout(() => {
                        observer.disconnect();
                        isObserving = false;
                    }, 300000);
                }

                // Initial update attempt
                if (!updateVideoSources()) {
                    console.log("Initial update failed, starting observation");
                    startObserving();
                } else {
                    console.log("Initial update successful");
                }

                // Handle visibility changes
                document.addEventListener("visibilitychange", function() {
                    if (!document.hidden) {
                        if (updateVideoSources()) {
                            console.log("Videos updated on visibility change");
                        } else {
                            console.log("Update failed on visibility change, starting observation");
                            startObserving();
                        }
                    }
                });

                return true;
            }

            // Try to change videos immediately
            const currentURL = window.location.href;
            let changeSuccessful = false;

            if (currentURL.includes('https://kindroid.ai/home/')) {
                changeSuccessful = changeVideosHome();
            } else if (currentURL.includes('https://kindroid.ai/voicecall/')) {
                changeSuccessful = changeVideosVoiceCall();
            }

            if (!changeSuccessful) {
                // If it doesn't work, set up a mutation observer
                const observer = new MutationObserver((mutations) => {
                    if (currentURL.includes('https://kindroid.ai/home/')) {
                        if (changeVideosHome()) {
                            observer.disconnect();
                        }
                    } else if (currentURL.includes('https://kindroid.ai/voicecall/')) {
                        if (changeVideosVoiceCall()) {
                            observer.disconnect();
                        }
                    }
                });

                observer.observe(document.body, {
                    childList: true,
                    subtree: true
                });

                // Stop the observer after 30 seconds to prevent it from running indefinitely
                setTimeout(() => observer.disconnect(), 30000);
            }
        } else {
            // Restore original sources
            const videoElements = document.querySelectorAll('.css-owh929 video, .css-1jbvs8u video');
            if (videoElements.length > 1) {
                if (getLocalStorage('originalTalkingSrc')) {
                    videoElements[0].src = getLocalStorage('originalTalkingSrc');
                }
                if (getLocalStorage('originalIdleSrc')) {
                    videoElements[1].src = getLocalStorage('originalIdleSrc');
                }
            }
        }
    }

    // On page loading
    if (animatedAvatarChangerEnabled) {
        // Attendez que le DOM soit complètement chargé
        if (document.readyState === 'loading') {
            document.addEventListener('DOMContentLoaded', () => {
                animatedAvatarChanger();
                applyCustomStyle();
            });
        } else {
            animatedAvatarChanger();
            applyCustomStyle();
        }
    }

    // Changer l'état de animatedAvatarChangerEnabled dynamiquement
    function toggleAnimatedAvatarChanger(enabled) {
        animatedAvatarChangerEnabled = enabled;
        applyCustomStyle();
        if (enabled) {
            animatedAvatarChanger(true); // Force le prompt quand on active l'option
        } else {
            // Restore original sources
            const videoElements = document.querySelectorAll('.css-owh929 video, .css-1jbvs8u video');
            if (videoElements.length > 1) {
                if (getLocalStorage('originalTalkingSrc')) {
                    videoElements[0].src = getLocalStorage('originalTalkingSrc');
                }
                if (getLocalStorage('originalIdleSrc')) {
                    videoElements[1].src = getLocalStorage('originalIdleSrc');
                }
            }
        }
    }

    // Auto Confirm Regenerate
    function autoConfirmRegenerate() {
        if (autoConfirmEnabled) {
            const modal = document.querySelector('.css-hklsfr');
            const confirmButton = document.querySelector('button[aria-label="Regenerate"]');

            if (modal && confirmButton) {
                confirmButton.click();
            }
        }
        setTimeout(autoConfirmRegenerate, 500);
    }
    autoConfirmRegenerate();

    // Auto Continue cut-off message
    function autoContinue() {
        // Vérifier si l'option est activée avant de définir l'intervalle
        if (autoContinueEnabled) {
            const checkContinueButton = setInterval(() => {
                const lastChatContainer = document.querySelector('.css-yhhl9h:last-child');
                const continueButton = lastChatContainer ? lastChatContainer.querySelector('.css-fp0si') : null;

                if (continueButton) {
                    continueButton.click(); // Clique sur le bouton
                    console.log('Auto Continue: le bouton a été cliqué.');
                }
            }, 500); // Intervalle de vérification
        } else {
            // Si l'option est désactivée, arrêtez l'intervalle s'il existe
            clearInterval(checkContinueButton);
        }
    }
    autoContinue();

    // Autofocus on textarea
    function focusTextarea() {
        var textareaElement = document.querySelector('textarea[aria-label="Send message textarea"]');
        textareaElement.focus();
    }

    var observer = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            if (autoFocusEnabled) {
                focusTextarea();
            }
        });
    });

    observer.observe(document.body, { childList: true, subtree: true });

    setInterval(function() {
        if (autoFocusEnabled) {
            focusTextarea();
        }
    }, 500);

    // Function to format date
    let clickCounter = 0; // Compteur de clics

    // Fonction pour vérifier si le texte est vide ou contient seulement des espaces insécables
    function isEmptyOrNbsp(text) {
        return text.replace(/ /g, '').trim() === ''; // Supprime les espaces insécables et vérifie si le texte est vide
    }

    // Fonction pour vérifier si une chaîne de texte contient une date formatée
    function isFormattedDate(text) {
        return /\d{1,2} \w+ \d{4} à \d{2}:\d{2}/.test(text); // Vérifie si le texte contient une date formatée
    }

    // Fonction pour vérifier si une chaîne correspond au format de date d'origine
    function isOriginalDateFormat(text) {
        return /(\w+), (\w+) (\d+) (\d+), (\d+):(\d+) (AM|PM)/.test(text); // Vérifie si le texte correspond au format non formaté d'origine
    }

    // Fonction pour formater la date
    function formatDate(dateString) {
        try {
            dateString = dateString.replace(/ /g, '').trim();
            let dateComponents = dateString.match(/(\w+), (\w+) (\d+) (\d+), (\d+):(\d+) (AM|PM)/);
            if (dateComponents) {
                let [, , month, day, year, hour, minute, period] = dateComponents;
                let monthIndex = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'].indexOf(month);
                hour = parseInt(hour, 10);
                if (period === 'PM' && hour !== 12) {
                    hour += 12;
                } else if (period === 'AM' && hour === 12) {
                    hour = 0;
                }
                let date = new Date(year, monthIndex, day, hour, minute);
                let language = navigator.language || navigator.userLanguage;
                let options = {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric',
                    hour12: false
                };
                return new Intl.DateTimeFormat(language, options).format(date);
            } else {
                return dateString; // Retourne la chaîne originale si la date n'a pas pu être analysée
            }
        } catch (error) {
            console.error("Error formatting date: ", error);
            return dateString; // Retourne la chaîne originale en cas d'erreur
        }
    }

    // Fonction pour formater les dates dans les bulles de chat
    function formatChatBubbleDates(element) {
        try {
            let dateString = element.innerHTML.trim(); // Utilisation de innerHTML pour conserver les espaces insécables

            if (!isEmptyOrNbsp(dateString) && isOriginalDateFormat(dateString)) {
                // Vérifie si le span existe déjà
                if (!element.nextSibling || !element.nextSibling.classList.contains('formatted-date')) {
                    // Crée un nouvel élément span pour la date formatée
                    let formattedDate = formatDate(dateString);
                    let span = document.createElement('span');
                    span.textContent = formattedDate; // Utilise textContent pour éviter les problèmes de XSS
                    span.className = 'formatted-date'; // Ajoute une classe pour styliser si besoin

                    // Ajoute le span après l'élément <p>
                    element.parentNode.insertBefore(span, element.nextSibling);

                    // Masque le texte d'origine
                    element.style.visibility = 'hidden'; // Masque l'élément <p> entier
                }
            }
        } catch (error) {
            console.error("Error in formatChatBubbleDates: ", error);
        }
    }

    // Fonction pour supprimer le span contenant la date formatée
    function removeFormattedDateSpans(element) {
        // Trouver le span avec la classe 'formatted-date' et le retirer
        let formattedDateSpan = element.nextSibling;
        if (formattedDateSpan && formattedDateSpan.classList.contains('formatted-date')) {
            formattedDateSpan.remove(); // Supprime le span
            element.removeAttribute('style'); // Supprime le style pour rendre l'élément <p> visible
        }
    }

    // Fonction pour ajouter les écouteurs d'événements de clic
    function addDateClickListeners() {
        document.querySelectorAll('.css-ip0f2e, .css-v3ipd1').forEach(function(element) {
            element.addEventListener('click', function(event) {
                // Vérifie si le clic est sur un enfant <p> qui contient la date
                let chatBubble = element.querySelector('.chakra-text.css-dhnoom');
                if (chatBubble) {
                    clickCounter++; // Incrémenter le compteur de clics

                    if (clickCounter % 2 !== 0) { // Formater seulement aux clics impairs
                        setTimeout(function() {
                            formatChatBubbleDates(chatBubble);
                        }, 100);
                    } else { // Sur clic pair, supprimer le span
                        removeFormattedDateSpans(chatBubble);
                    }
                }
            });
        });
    }

    addDateClickListeners();

    // Création d'un observateur de mutations pour surveiller les changements dans le DOM
    let dateObserver = new MutationObserver(function(mutations) {
        mutations.forEach(function(mutation) {
            mutation.addedNodes.forEach(function(addedNode) {
                if (addedNode.nodeType === 1) { // 1 signifie ELEMENT_NODE
                    addDateClickListeners();
                }
            });
        });
    });

    // Commence à observer le document avec les paramètres configurés
    dateObserver.observe(document.body, { childList: true, subtree: true });

    // Kindroid Compagnion
    const manifestLink = document.createElement('link');
    manifestLink.crossorigin = 'anonymous';
    manifestLink.href = 'https://corsproxy.io/?https://gitlab.com/breatfr/kindroid/-/raw/main/json/manifest.json';
    manifestLink.rel = 'manifest';

    document.head.appendChild(manifestLink);

    console.log("Manifest added to the site!");

    // Function to toggle content blur
    function blurContent() {
        const blurElements = document.querySelectorAll('ion-app > div > div > div > div:nth-of-type(2) > div:nth-of-type(1) > div > div > div:nth-of-type(1) > p, ion-app > div > div > div > div:nth-of-type(1) > div > div:nth-of-type(2) > div:nth-of-type(1) > .css-1dhayxc > div:nth-of-type(2) > div:nth-of-type(2) > p, .css-yhhl9h p:not(.css-dhnoom), ion-app > div > div > div > div > div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(3)');

        blurElements.forEach(function(element) {
            if (blurContentEnabled) {
                element.style.filter = 'blur(8px)';
                element.style.webkitFilter = 'blur(8px)';
            } else {
                element.removeAttribute('style');
            }
        });
    }

    // Function to observe the DOM for changes
    function observeDOM() {
        const targetNode = document.querySelector('ion-app'); // Change this to your main container if needed

        const config = { childList: true, subtree: true };

        const callback = function(mutationsList) {
            for (const mutation of mutationsList) {
                if (mutation.type === 'childList') {
                    blurContent(); // Reapply the blur when new elements are added
                }
            }
        };

        const observer = new MutationObserver(callback);
        observer.observe(targetNode, config);
    }

    // Start observing the DOM once the content is loaded
    document.addEventListener("DOMContentLoaded", function() {
        blurContent(); // Apply blur on initial load
        observeDOM(); // Start observing for changes
    });
})();
