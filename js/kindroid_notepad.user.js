// ==UserScript==
// @name        Kindroid - Notepad
// @description Kindroid's Notepad feature
// @namespace   https://gitlab.com/breatfr
// @match       https://kindroid.ai/home*
// @match       https://kindroid.ai/groupchat*
// @match       https://kindroid.ai/selfies*
// @match       https://kindroid.ai/call*
// @version     2.0.4
// @homepageURL https://gitlab.com/breatfr/kindroid
// @downloadURL https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_notepad.user.js
// @updateURL   https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_notepad.user.js
// @supportURL  https://discord.gg/fSgDHmekfG
// @author      BreatFR
// @copyright   2024, BreatFR (https://breat.fr)
// @grant       none
// @icon        https://gitlab.com/breatfr/kindroid/-/raw/main/images/icon_kindroid.png
// @license     BY-NC-ND; https://creativecommons.org/licenses/by-nc-nd/4.0/
// ==/UserScript==

(function() {
    'use strict';

    console.log("Notepad script running");

    // Design
    var style = document.createElement('style');
    style.textContent = `
        /* Hide icons at the right of header */
        .css-x3odei > div:nth-of-type(2),
        .css-z0osps > div > div:nth-of-type(2) {
          display: none;
          height: 0;
          width: 0;
        }

        #notepad-script-button {
          background-color: transparent;
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'%3E%3Cpath d='M416 221.25V416a48 48 0 01-48 48H144a48 48 0 01-48-48V96a48 48 0 0148-48h98.75a32 32 0 0122.62 9.37l141.26 141.26a32 32 0 019.37 22.62z' fill='none' stroke='%23cbcbcb' stroke-linejoin='round' stroke-width='32'/%3E%3Cpath d='M256 56v120a32 32 0 0032 32h120' fill='none' stroke='%23cbcbcb' stroke-linecap='round' stroke-linejoin='round' stroke-width='32'/%3E%3C/svg%3E");
          background-repeat: no-repeat;
          background-position: center center;
          border: none;
          cursor: pointer;
          height: 40px;
          position: fixed;
          top: 10px;
          -o-transition: opacity 0.3s ease;
          -moz-transition: opacity 0.3s ease;
          -ms-transition: opacity 0.3s ease;
          -webkit-transition: opacity 0.3s ease;
          transition: opacity 0.3s ease;
          width: 40px;
        }

        #notepad-script-button:hover {
          background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512'%3E%3Cpath d='M416 221.25V416a48 48 0 01-48 48H144a48 48 0 01-48-48V96a48 48 0 0148-48h98.75a32 32 0 0122.62 9.37l141.26 141.26a32 32 0 019.37 22.62z' fill='none' stroke='url(%23A)' stroke-linejoin='round' stroke-width='32'/%3E%3Cpath d='M256 56v120a32 32 0 0032 32h120' fill='none' stroke='url(%23A)' stroke-linecap='round' stroke-linejoin='round' stroke-width='32'/%3E%3Cdefs%3E%3ClinearGradient id='A' x1='1.6' y1='25.2' x2='29.2' y2='0' gradientUnits='userSpaceOnUse'%3E%3Cstop offset='.208' stop-color='%238b6dff'/%3E%3Cstop offset='1' stop-color='%23fe8484'/%3E%3C/linearGradient%3E%3C/defs%3E%3C/svg%3E");
        }

        #notepad-block {
          background-color: #fffaac;
          border-radius: 1em;
          display: none;
          height: calc(100% - 90px);
          position: absolute;
          right: 20px;
          top: 60px;
          width: 30%;
          z-index: 1000;
        }

        #notepad-header {
          align-items: center;
          background-color: #8b4513;
          border-radius: .5em .5em 0 0;
          cursor: move;
          display: flex;
          height: 30px;
          justify-content: center;
          position: relative;
        }

        .notepad-header-text {
          color: white;
          font-family: var(--chakra-fonts-PoppinsRegular), sans-serif;
          font-weight: bold;
        }

        .notepad-resize,
        .notepad-save {
          background-color: #8b4513;
          border: none;
          color: white;
          cursor: pointer;
          height: 20px;
          position: absolute;
          width: 20px;
        }
        .notepad-resize {
          left: 10px;
        }
        .notepad-save {
          right: 10px;
        }

        .notepad-textarea {
          background-color: transparent;
          background-size: 100% 30px;
          border: none;
          border-radius: 0;
          color: black;
          font-family: var(--chakra-fonts-PoppinsRegular), sans-serif;
          font-size: 1.2rem;
          height: calc(100% - 30px);
          line-height: 1.5;
          outline: none;
          padding: 0 10px 10px 10px;
          -ms-overflow-style: none;
          scrollbar-width: none;
          width: 100%;
        }
        .notepad-textarea:focus {
          border: none;
          box-shadow: none;
          outline: none;
        }
        .notepad-textarea::-webkit-scrollbar {
          display: none;
          width: 0;
        }

        @media screen and (max-width: 1400px), (orientation: portrait) {
          #notepad-script-button {
            height: 36px;
            top: 5px;
            width: 36px;
          }

          #notepad-block {
            height: 80%;
            left: 20px;
            top: 48px;
            width: calc(100% - 40px);
          }
        }
    `;
    document.head.appendChild(style);

    // Création du bouton pour afficher le bloc-notes
    var noteButton = document.createElement("button");
    noteButton.ariaLabel = 'Notepad';
    noteButton.id = 'notepad-script-button';
    noteButton.title = 'Notepad';
    noteButton.dataset.priority = 3;
    document.body.appendChild(noteButton);

    // Fonction pour ajouter une icône
    function addIcon(iconId, priority) {
        const isMobile = window.matchMedia("(max-width: 1400px), (orientation: portrait)").matches;
        // Récupérer le bouton existant par son ID
        const iconButton = document.getElementById(iconId);
        if (!iconButton) {
            console.error(`Le bouton avec l'ID ${iconId} n'existe pas.`);
            return;
        }

        // Créer ou mettre à jour l'icône dans un tableau des priorités
        let icons = [];

        // Récupérer tous les boutons existants
        const buttons = document.querySelectorAll('button[id]');
        buttons.forEach(button => {
            const buttonId = button.id;
            const buttonPriority = parseInt(button.dataset.priority) || 0; // Récupère la priorité ou 0 par défaut
            icons.push({ id: buttonId, priority: buttonPriority });
        });

        // Vérifier si l'icône actuelle est déjà dans le tableau, et la mettre à jour
        const existingIconIndex = icons.findIndex(icon => icon.id === iconId);
        if (existingIconIndex !== -1) {
            icons[existingIconIndex].priority = priority; // Met à jour la priorité si déjà existante
        } else {
            icons.push({ id: iconId, priority: priority }); // Sinon, ajouter l'icône
        }

        // Trier les icônes par priorité croissante (priorité 1 est la plus à droite)
        icons.sort((a, b) => a.priority - b.priority);

        // Calculer la position basée sur l'ordre trié
        let occupiedSpace = isMobile ? -11 * window.innerWidth / 100 : 16; // 11% de la largeur de l'écran pour mobile
        const spacing = isMobile ? 10 : 20; // Espacement de 10px pour mobile, 20px pour desktop
        console.log("Initial occupiedSpace: ", occupiedSpace);

        icons.forEach(icon => {
            const button = document.getElementById(icon.id);
            if (button) {
                console.log(`Placement pour ${icon.id} avant ajustement: ${occupiedSpace}px`);
                button.style.right = isMobile ? `${Math.abs(occupiedSpace)}px` : `${occupiedSpace}px`; // Positionner le bouton
                occupiedSpace += 40 + spacing; // 40px pour l'icône + espacement dynamique
                console.log(`OccupiedSpace après ${icon.id}: ${occupiedSpace}px`);
            }
        });
    }
    addIcon("notepad-script-button", 3);

    // Création du bloc-notes
    let noteDiv = document.createElement("div");
    noteDiv.id = "notepad-block";

    // Ajout de la bande marron en haut (pour déplacer)
    let headerDiv = document.createElement("div");
    headerDiv.id = "notepad-header";
    noteDiv.appendChild(headerDiv);

    // Création de l'élément de texte
    let headerText = document.createElement("span");
    headerText.className = "notepad-header-text";
    headerText.textContent = "📝 Notepad";
    headerDiv.appendChild(headerText);

    let noteTextarea = document.createElement("textarea");
    noteTextarea.className = 'notepad-textarea';
    noteTextarea.value = localStorage.getItem("noteContent") || "";
    noteDiv.appendChild(noteTextarea);
    document.body.appendChild(noteDiv);

    // Sauvegarder le contenu à chaque changement
    noteTextarea.addEventListener("input", () => {
        localStorage.setItem("noteContent", noteTextarea.value);
    });

    // Afficher ou cacher le bloc-notes avec le bouton
    noteButton.addEventListener("click", () => {
        // Vérifie si le bloc-notes est actuellement affiché
        if (noteDiv.style.display === "none" || noteDiv.style.display === "") {
            noteDiv.style.display = "block";  // Afficher le bloc-notes
        } else {
            noteDiv.style.display = "none";  // Cacher le bloc-notes
        }
    });

    // Fonction de déplacement
    function enableDragging(noteDiv, headerDiv) {
        let shiftX, shiftY;

        function moveAt(pageX, pageY) {
            noteDiv.style.left = pageX - shiftX + 'px';
            noteDiv.style.top = pageY - shiftY + 'px';
        }

        function onMouseMove(event) {
            moveAt(event.pageX, event.pageY);
        }

        function onTouchMove(event) {
            moveAt(event.touches[0].pageX, event.touches[0].pageY);
        }

        headerDiv.addEventListener("mousedown", function (e) {
            shiftX = e.clientX - noteDiv.getBoundingClientRect().left;
            shiftY = e.clientY - noteDiv.getBoundingClientRect().top;

            document.addEventListener("mousemove", onMouseMove);

            headerDiv.onmouseup = function () {
                document.removeEventListener("mousemove", onMouseMove);
                headerDiv.onmouseup = null;
            };
        });

        headerDiv.addEventListener("touchstart", function (e) {
            e.preventDefault();
            shiftX = e.touches[0].clientX - noteDiv.getBoundingClientRect().left;
            shiftY = e.touches[0].clientY - noteDiv.getBoundingClientRect().top;

            document.addEventListener("touchmove", onTouchMove);

            headerDiv.ontouchend = function () {
                document.removeEventListener("touchmove", onTouchMove);
                headerDiv.ontouchend = null;
            };
        });
    }

    // Appel de la fonction pour activer le déplacement
    enableDragging(noteDiv, headerDiv);

    noteDiv.ondragstart = function () {
        return false;
    };

    // Attendre que la page soit complètement chargée
    window.addEventListener('load', () => {
        console.log("Page loaded. Setting up Notepad button...");
        setTimeout(() => {
            noteButton.style.display = 'block';
        }, 2000);
    });

    // Ajout du bouton d'agrandissement/réduction
    let resizeButton;
    if (window.matchMedia("(min-width: 900px)").matches) {
      resizeButton = document.createElement("button");
      resizeButton.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" class="ionicon" viewBox="0 0 512 512"><path fill="none" stroke="white" stroke-linecap="round" stroke-linejoin="round" stroke-width="32" d="M432 320v112H320M421.8 421.77L304 304M80 192V80h112M90.2 90.23L208 208M320 80h112v112M421.77 90.2L304 208M192 432H80V320M90.23 421.8L208 304"/></svg>';
      resizeButton.ariaLabel = 'Expand';
      resizeButton.className = 'notepad-resize';
      resizeButton.title = "Expand";
      headerDiv.appendChild(resizeButton);
    }

    let isExpanded = false;
    let savedPosition = { left: noteDiv.style.left, right: noteDiv.style.right };

    resizeButton.addEventListener("click", () => {
        if (isExpanded) {
            // Réduire à la taille initiale et restaurer la position sauvegardée
            noteDiv.style.width = "30%";
            noteDiv.style.left = savedPosition.left;
            noteDiv.style.right = savedPosition.right;

            // Mettre à jour l'icône et le title
            resizeButton.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" class="ionicon" viewBox="0 0 512 512"><path fill="none" stroke="white" stroke-linecap="round" stroke-linejoin="round" stroke-width="32" d="M432 320v112H320M421.8 421.77L304 304M80 192V80h112M90.2 90.23L208 208M320 80h112v112M421.77 90.2L304 208M192 432H80V320M90.23 421.8L208 304"/></svg>';
            resizeButton.ariaLabel = 'Expand';
            resizeButton.title = "Expand";
        } else {
            // Sauvegarder la position actuelle avant d'agrandir
            savedPosition.left = noteDiv.style.left;
            savedPosition.right = noteDiv.style.right;
            // Agrandir sans modifier la position
            noteDiv.style.width = "calc(100% - 60px)";
            noteDiv.style.left = "auto";
            noteDiv.style.right = "30px";

            // Mettre à jour l'icône et le title pour la réduction
            resizeButton.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" class="ionicon" viewBox="0 0 512 512"><path fill="none" stroke="white" stroke-linecap="round" stroke-linejoin="round" stroke-width="32" d="M304 416V304h112M314.2 314.23L432 432M208 96v112H96M197.8 197.77L80 80M416 208H304V96M314.23 197.8L432 80M96 304h112v112M197.77 314.2L80 432"/></svg>';
            resizeButton.ariaLabel = 'Reduce';
            resizeButton.title = "Reduce";
        }
        isExpanded = !isExpanded;
    });

    // Ajout du bouton de sauvegarde à droite
    let saveButton = document.createElement("button");
      saveButton.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" class="ionicon" viewBox="0 0 512 512"><path d="M380.93 57.37A32 32 0 00358.3 48H94.22A46.21 46.21 0 0048 94.22v323.56A46.21 46.21 0 0094.22 464h323.56A46.36 46.36 0 00464 417.78V153.7a32 32 0 00-9.37-22.63zM256 416a64 64 0 1164-64 63.92 63.92 0 01-64 64zm48-224H112a16 16 0 01-16-16v-64a16 16 0 0116-16h192a16 16 0 0116 16v64a16 16 0 01-16 16z" fill="none" stroke="white" stroke-linecap="round" stroke-linejoin="round" stroke-width="32"/></svg>';
      saveButton.ariaLabel = "Save notepad content";
      saveButton.className = 'notepad-save';
      saveButton.title = "Save notepad content";
      headerDiv.appendChild(saveButton);

    // Fonction pour télécharger le contenu du bloc-notes sous forme de fichier .txt
    saveButton.addEventListener("click", () => {
      // Récupérer le contenu du bloc-notes
      let content = noteTextarea.value;

      // Créer un Blob (objet représentant des données brutes)
      let blob = new Blob([content], { type: "text/plain" });

      // Créer un lien de téléchargement
      let downloadLink = document.createElement("a");
      downloadLink.href = window.URL.createObjectURL(blob);
      downloadLink.download = "kindroid-notepad.txt"; // Nom du fichier téléchargé

      // Simuler un clic sur le lien pour déclencher le téléchargement
      downloadLink.click();
    });
})();
