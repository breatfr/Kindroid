# Changelog

All notable changes to this project will be documented in this file. For screenshot names, it's the version number of the Theme PC that's taken into account, as it's in this theme that I indicate the version numbers of everything else.

## Theme PC v4.1.30 - 2025-02-24
- fix my kindroid main menu page

## Theme PC v4.1.29 - 2025-02-18
- simplication of background choice

## Theme PC v4.1.28 - 2025-02-16
- add avatar style rectangle big for chat page only
     - avatar style rectangle keep ratio 9 / 13
     - avatar style rectangle big is the old avatar rectangle so take always full height automatically calculated to never be more than chat height
if you select rectangle big on chat page, you don't need change it in group chat or call pages, the theme will take automatically avatar rectangle on these 2 pages

## Theme PC v4.1.27 - 2025-02-16
- update scripts versions

## Chat page PC script v2.0.19 - 2025-02-16
- add support of continue cut-off message when the icon is in the last kin's bubble

## Chat page mobile script v2.0.19 - 2025-02-16
- add support of continue cut-off message when the icon is in the last kin's bubble

## Theme PC v4.1.26 - 2025-02-16
- fix avatar colors in light mode in the call page by modifying the avatarchoice code and filters code
- fix some avatar styles for call page
- fix colors in light mode for many things (i can't do for all without give you the dark mode for some things) on whole website

## Theme PC v4.1.25 - 2025-02-15
- add a theme option to hide kins with few infos in explore modal
- fix explore modal

## Theme PC v4.1.24 - 2025-02-15
- add monserrat font
- set alerts at top of screen (like ont when kin have nothing more to say) in wide mode and add a border to be more visible

## Theme PC v4.1.23 - 2025-02-14
- add font family theme option to let you replace the font on whole website

## Theme PC v4.1.22 - 2025-02-12
- remove the theme option full bio added in v4.1.21
- add a theme option full bio in explore and create kin who allow you to choose between 3 types:
     - default: no full bio
     - always: always see full bio
     - active: see the full bio by clicking and holding on the bio

## Theme PC v4.1.21 - 2025-02-12
- add a theme option to disable full bio

## Theme PC v4.1.20 - 2025-02-11
- add a cursor pointer on create my own from scratch in create kindroid modal beucause he was missed by kindroid's devs
- set some text in kindroid modal with the size you configured in theme
- customize this modal a little more, not like i wanted exactly because i can't but now:
	- full biography is present
	- moving create button on left under mini-avatar
	- set create button change color when hover
	- set mini-avatar a little bigger
	- set mini-avatar much bigger (300x300px) when he is hover
	- center tags and names
	- make all tags visible because was missed by kindroid's devs

## Theme PC v4.1.19 - 2025-02-11
- add adaptable modals theme option for whole site
- add explore in the hide sharing theme setting
- fix user guide in blue
- set explore and sharing & referrals in purple in main menu
- customize like i can the new explore and create kin modals due to kindroid website code limitation. if i customize more the website crash

## Theme PC v4.1.18 - 2025-02-09
- hide the spinner next to the last kin's bubble when kin is writing because there is already one in the textarea
- fix a "problem" on some textareas when we click in them, to avoid the text move
- some ajustments for kindroid compagnion version

## Theme PC v4.1.17 - 2025-02-07
- update scripts versions

## Chat script PC v2.0.18 - 2025-02-07
- improve kindroid compagnion function

## Chat script mobile v2.0.18 - 2025-02-07
- improve kindroid compagnion function

## Selfie script PC v2.0.3 - 2025-02-07
- improve kindroid compagnion function

## Selfie script mobile v2.0.3 - 2025-02-07
- improve kindroid compagnion function

## Theme PC v4.1.16 - 2025-02-07
- add avatar invert colors to fix the color problem in light mode with avatars

## Theme PC v4.1.15 - 2025-02-06
- customization of kindroids scheduled for deletion to fix group list when this part is here

## Theme PC v4.1.14 - 2025-02-06
- modification of discord banner, now you can find a direct sort link to my faqs to read it and tutorials

## Theme PC v4.1.13 - 2025-02-06
- optimize the creating kindroid modal
- in the shared kins list, when you hover an avatar you'll see him a little bigger
- remove the photos/videos tabs stay on screen so allow us see tags
- fix (normally) avatar mix blend and avatar opacity for people who use official animated avatar, now the effect will apply only on the playing video so normally no more talking video instead of idle one

## Theme PC v4.1.12 - 2025-02-05
- reduce the size of auto-selfie modal
- rework a part of video selfie modal (when we open a video selfie) like this:
	- if no prompt then small modal (like before this theme version)
	- if prompt is here then, same modal as for image selfie modal

## Theme PC v4.1.11 - 2025-02-05
- add some indications when hover on request solo selfie, group selfie, video
- add avatar brightness theme option for all website
- add avatar contrast theme option for all website
- add avatar grayscale theme option for all website
- add avatar mix blend mode for all website with all of these options:
	- default (disabled)
	- color
	- color-burn
	- color-dodge
	- darken
	- difference
	- exclusion
	- hard-light
	- hue
	- lighten
	- luminosity
	- multiply
	- overlay
	- plus-lighter
	- saturation
	- screen
	- soft-light

⚠️ mix blend mode is a little special,for gamers it's like reshade or nvidia ansel, so it's to modify colors etc. maybe not work with all images because that depend of the images size, image color, background color etc but when that works that give good results
- add avatar opacity theme option
- add avatar move anywhere option and his linked values (only for chat page) in experimental things

## Selfie script PC v2.0.2 - 2025-02-05
- remove see all images
- remove download all image
- add see all images/videos
- add download all images/videos

## Selfie script mobile v2.0.2 - 2025-02-05
- remove see all images
- remove download all image
- add see all images/videos
- add download all images/videos

## Theme PC v4.1.10 - 2025-02-04
- fix request selfie button at top theme option
- fix gallery space between lines
- customize request selfie, group selfie and video selfie buttons
- set photos and videos tabs stay visible even if we scroll
- update blur content theme option to hide shared links too

## Chat script PC v2.0.17 - 2025-02-04
- update blur content script option to hide shared links too

## Chat script mobile v2.0.17 - 2025-02-04
- update blur content script option to hide shared links too

## Theme PC v4.1.9 - 2025-02-04
- set the attached link textarea wide to avoid get link on more than 1 line
- fix manually edited theme option to avoid him replace links we share with kin. now shared kinks are colored too but visible.

## Theme PC v4.1.8 - 2025-02-03
- add avatar zoom theme option to let you zoom-in/out avatars on whole website (0 to 1 = zoom-out, 1 = default size, 1.1 to 3 = zoom-in)

## Theme PC v4.1.7 - 2025-02-02
- improve the avatar boost modal when we click on an existing avatar boost

## Theme PC v4.1.6 - 2025-02-02
- add a colorised icon when hover for copy prompt in selfie modal
- fix wide size of kindroid creation
- replace the chat textarea border when focus (only this one to try)

## Theme PC v4.1.5 - 2025-02-02
- add colorized icons for copy prompt and delete avatar when hover in select generated avatar
- add text when hover for copy prompt and delete avatar when hover in select generated avatar
- fix copy prompt was unclickable in select generated avatar

## Theme PC v4.1.4 - 2025-02-01
- add a tooltip for suggest message in textarea
- add colorized icons for extras (+, add image/video, use internet, attach link) and suggest message icons when hover
- fix play/stop icons were unclickable when hide names theme option is enabled

## Theme PC v4.1.3 - 2025-02-01
- add new avatar styles:
	- alien
	- apple
	- atom
	- cloud
	- crystal
	- devil
	- gem
	- ghost
	- gift
	- rabbit
	- slime

## Theme PC v4.1.2 - 2025-02-01
- add avatar flip theme option for all pages:
	- Default (no flip)
	- horizontally
	- vertically
	- horizontally & vertically
- add avatar rotate theme option for all pages
- fix avatar creation modal

## Theme PC v4.1.1 - 2025-02-01
- add avatar styles options:
     - smartphone
     - tablet
     - venetian mask
- fix avatar styles on groupchat page

## Theme PC v4.1.0 - 2025-01-31
- remove avatar circle
- remove avatar rectangle
- add avatar styles with these options:
	- Default (square)
	- Bookmark
	- Cannabis
	- Chat bubble
	- Chef hat
	- Circle
	- Cog
	- Heart
	- Octagon
	- Oval H
	- Oval V
	- Pentagon
	- Ratio
	- Rectangle
	- Shield
	- Ship wheel
	- Shirt
	- Squircle
	- Star
	- Tree deciduous
	- Tree pine
	- Triangle
- add tooltips i forgot for icons (those at the top of sidebar) in sidebar on groupchat page

## Theme PC v4.0.9 - 2025-01-31
- add an effect when we hover main menu buttons and main menu bottom links to see better what we hover
- add a theme option (disabled by default) to hide billing in main menu to save space
- set the icon i added before the logout link in orange like the link itself
- replace some icons in stylus popup
- split experimental things in a distinct category, like this you can choose what you want try
- fix tooltips in groupchat in sidebar when hover

## Theme PC v4.0.8 - 2025-01-31
- set pinned kins/groups a little bigger
- some code optimizations

## Theme PC v4.0.7 - 2025-01-30
- add a cursor zoom-in and tooltips when we hover a selfie in selfie modal
- add a cursor move when we start drag-and-drop a zoomed selfie in selfie modal
- add an option at the end of theme settings to try the experimental things (currently main menu and request selfie)
- reworked the edit selfie modal
- reworked the create video selfie modal
- fix avatar and my avatar settings

## Theme PC v4.0.6 - 2025-01-29
- fix continue conversation manual 
- fix continue conversation manual center

## Theme PC v4.0.5 - 2025-01-29
- update chat scripts version

## Chat script PC v2.0.16 - 2025-01-29
- improve auto continue cut-off message to avoid stuck kins

## Chat script mobile v2.0.16 - 2025-01-29
- improve auto continue cut-off message to avoid stuck kins

## Theme PC v4.0.4 - 2025-01-29
- fix almost all chat page theme for few people who don't have same class
- fix help cursor on selfie generating
- remove the font-size and width customization for stylus popup because create problem for some users even on chromium browsers, if you want this part, let me know and i'll give you in an other theme

## Chat script PC v2.0.15 - 2025-01-29
- fix (normally) auto continue cut-off message

## Chat script mobile v2.0.15 - 2025-01-29
- fix (normally) auto continue cut-off message

## Theme PC v4.0.3 - 2025-01-28
- fix blur content
- fix selfie group (not perfect when we use pose and style reference but it's usable now)

## Chat script PC v2.0.14 - 2025-01-28
- fix blur content

## Chat script mobile v2.0.14 - 2025-01-28
- fix blur content

## Theme PC v4.0.2 - 2025-01-28
- fix can't select an avatar in selected generated avatars list

## Theme PC v4.0.1 - 2025-01-28
- fix the top of menu not visible in firefox

## Theme PC v4.0.0 - 2025-01-28
- almost completely rewrite code to be more maintainable, optimized and in mean time improve functionalities
- add background shadow theme option
- add textarea opacity theme option
- add textarea background opacity theme option
- i fully reworked too the discord banner and add many infos in it, like a short link (not clickable) to install the addon for avatar choice in group chat
- separate some options like for bubbles color, bubbles background theme options

## Theme mobile v1.0.7 - 2025-01-28
- fix avatar choice theme option
- fix avatar circle theme option
- fix background theme option
- fix blur content and replace him by a part of the same of pc v4 (hide completely content but no adding text/image to replace them because i can't test myself)
- fix and replace bubble background and color theme options by the same as in pc v4
- fix manually edited theme option

## Theme CAG addon v1.0.1 - 2025-01-28
- recode this addon to make him works with pc v4 theme
- i remind you that this addon isn't an install-and-forget type of thing, you have to modify things in it for it to work. everything is explained in the file itself in the “Before use” and “How to use” sections.

## Chat script PC v2.0.13 - 2025-01-28
- fix date formating for bubbles
- fix (normally) the auto continue cut-off message functionality
- fix and replace old blur content functionality by the same as in the theme pc v4

## Chat script mobile v2.0.13 - 2025-01-28
- fix date formating for bubbles
- fix (normally) the auto continue cut-off message functionality
- fix and replace old blur content functionality by the same as in the theme pc v4

## Theme v3.45.17 - 2025-01-22
- add k menu icon (enabled by default) theme option
- fix system bubbles with and without sidebar mode
- fix main menu
- fix delete kin/group menu
- fix blur content

## Theme PC v3.45.16 - 2025-01-21
- fix a problem with sidebar mode when avatar rectangle isn't enable but avatar large is enabled to avoid enlarge avatar

## Theme PC v3.45.15 - 2025-01-21
- add a sidebar mode (enabled by default) in theme settings to try

## Theme PC v3.45.14 - 2025-01-19
- add hide chatting as theme option

## Theme PC v3.45.13 - 2025-01-19
- fix manually edited theme option

## Theme PC v3.45.12 - 2025-01-18
- fix llm  icon to replace llm choice
- fix blur content to blur email in main menu
- fix main menu
- fix no sharing theme option

## Theme PC v3.45.11 - 2025-01-17
- keep textarea stay under main menu

## Theme PC v3.45.10 - 2025-01-17
- fix textarea stay under content

## Theme PC v3.45.9 - 2025-01-08
- reduce a little the size of icons i add in system bubble left

## Theme PC v3.45.8 - 2025-01-07
- fix kindroid list in main menu when a kindroid havn't avatar

## Theme PC v3.45.7 - 2025-01-07
- fix avatar chat in chat page and now it's same as in group chat
- fix system bubble left with avatar chat in chat page (because of my icons)
- fix avatar choice
- fix group list

## Theme PC v3.45.6 - 2024-12-25
- fix avatar rectangle theme option for groupchat page
- fix avatar chat theme option for groupchat page

## Theme PC v3.45.5 - 2024-12-24
- fix selfies page

:warning: know possible issue on chromium browsers, when you come from chat page and arrive on selfies page or the opposite theme not apply dirrectly and ou need press `F5`.

ISN'T a theme problem, it's because of stylus update to respect new stupid `Chrome Manifest v3` and this update can disable a setting IN stylus, so just enable `instant injection` in `advanced settings` again

## Theme PC v3.45.4 - 2024-12-20
- fix actions text on chat page and group chat page for some users

## Theme mobile v1.0.6 - 2024-12-20
- fix actions text on chat page and group chat page for some users

## Theme PC v3.45.3 - 2024-12-20
- fix actions text on chat page and group chat page
- fix blur content on chat page and group chat page
- fix llm menu button in header
- fix no llm theme option

## Theme mobile v1.0.5 - 2024-12-20
- fix actions text on chat page and group chat page
- fix blur content on chat page and group chat page

## Chat page PC script v 2.0.12 - 2024-12-20
- fix blur content on chat page and group chat page

## Chat page mobile script v 2.0.12 - 2024-12-20
- fix blur content on chat page and group chat page

## Theme PC v3.45.2 - 2024-12-20
- fix actions text on chat page and group chat page
- fix blur content on chat page and group chat page

## Theme mobile v1.0.4 - 2024-12-20
- fix actions text on chat page and group chat page
- fix blur content on chat page and group chat page

## Chat page PC script v 2.0.11 - 2024-12-20
- fix blur content on chat page and group chat page

## Chat page mobile script v 2.0.11 - 2024-12-20
- fix blur content on chat page and group chat page

## Theme PC v3.45.1 - 2024-12-19
- fix v6 enhanced menu button in header

## Theme PC v3.45.0 - 2024-12-19
- replace llm version by an icon on top of page and reduce space between icons
- add a theme option to completely hide the llm button in header
- fix wide layout on chat page
- fix by replacing the system bubbles left by icons for avatar on left and avatar on right because this menu started to annoy me with width value :) works well but avoid too small avatar size (see screenshot)
- reorder system bubbles dark and dark wide to get same order as in system bubbles left

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.44.5 - 2024-12-15
- fix hide names option

## Theme PC v3.44.4 - 2024-12-15
- fix continue cut-off message since they remove the icon in the bubble so i added back him in the system bubble left

## Theme PC v3.44.3 - 2024-12-09
- fix shared memory icon position on chat page and group chat page
- fix bubbles position in group chat page when avatar rectangle is enabled
- fix new scenario size and position on chat page and group chat page
- fix date in bubbles color on chat page and group chat page
- fix scenario modal on chat page and group chat page

## Chat page PC script v 2.0.10 - 2024-12-09
- fix formated date option, still little bugged sorry

## Chat page mobile script v 2.0.10 - 2024-12-09
- fix formated date option, still little bugged sorry

## Theme PC v3.44.2 - 2024-11-28
- fix sharing & referrals menu page

## Theme PC v3.44.1 - 2024-11-28
- fix menu

## Theme PC v3.44.0 - 2024-11-28
- move new icon to see history and favorite message in top bar next to rewards icon
- fix color of date in bubbles
- fix avatar on left and right side
- fix blur content theme option
- fix call page when captions are enabled
- fix system bubbles left, dark and dark wide
- fix bigger screen sharing
- set icons in group bar in white and little closer of each other

⚠️ know issue when we come from group chat page and arrive on chat page the textarea is too big (221px height isntead of 61px), just press F5 or write in to fix it waiting i find a solution, currently i don't know wwhy the website change the default value

## Chat script PC v2.0.9 - 2024-11-28
- fix blur content
- fix formating date

## Chat script mobile v2.0.9 - 2024-11-28
- fix blur content
- fix formating date

## Theme PC v3.43.21 - 2024-11-23
- fix menu and system bubbles due to last website update

## Theme PC v3.43.20 - 2024-11-21
- fix view template button in my kindroids menu page

## Theme PC v3.43.19 - 2024-11-17
- update theme mobile version

## Theme mobile v1.0.3 - 2024-11-17
- fix custom font size on chat page

## Theme PC v3.43.18 - 2024-11-15
- update theme mobile version

## Theme mobile v1.0.2 - 2024-11-15
- fix action texts styles
- fix action texts color
- fix bubbles font color
- fix bubbles background
- fix custom font size
- replace old voicecall url to the new call url

## Theme PC v3.43.17 - 2024-11-15
- fix selfies page header (kin name, tags, history)

## Theme PC v3.43.16 - 2024-11-11
- fix actions style
- fix bubbles color
- fix bigger text in bubbles
- some actions styles are back too

## Theme PC v3.43.15 - 2024-11-08
- fix problem with initial letter (play icon, stop icon, ...) on chat page and groupchat page

## Theme PC v3.43.14 - 2024-11-08
- fix selfies page theme due to website update

## Theme PC v3.43.13 - 2024-11-07
- fix avatar in avatar main menu

## Theme PC v3.43.12 - 2024-11-05
- add a theme option (disabled by default) to set bigger the screen sharing in call (it's write in the theme settings but don't use it with captions enabled)

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.43.11 - 2024-11-04
- fix shared kins screen menu

## Theme PC v3.43.10 - 2024-11-04
- add CAG (Custom Avatars in Groupchat) in the version list
- set textarea of group context bigger in group edition

## Custom Avatars in Groupchat v1.0.0 - 2024-11-04
- a new "theme" to allow you customize avatars in groupchat page.

require the theme pc with avatar rectangle theme option enabled. tutoriel is included in code directly and you need edit the theme to make it work.
by default this one can manage 10 kins simultanously but you can just copy/paste to add more if need

- direct link to install it: https://gitlab.com/breatfr/kindroid/-/raw/main/css/kindroid_custom_avatars_in_groupchat.user.css

## Notepad script v2.0.4 - 2024-11-04
- apply theme on new call page instead of old voicecall page

## TV script v2.0.3 - 2024-11-04
- apply theme on new call page instead of old voicecall page

## Theme PC v3.43.9 - 2024-11-04
- apply theme on new call page instead of old voicecall page

## Chat page script PC v2.0.8 - 2024-11-04
- apply script on new call page instead of old voicecall page

## Chat page script mobile v2.0.8 - 2024-11-04
- apply script on new call page instead of old voicecall page

## Theme PC v3.43.8 - 2024-11-02
- fix create kin modal internal size

## Theme PC v3.43.7 - 2024-11-02
- fix avatar selection for group selfie

## Theme PC v3.43.6 - 2024-11-01
- add a theme option "no scrollbars" enabled by default so if you disable it you get back all scrollbars

## Theme PC v3.43.5 - 2024-11-01
- set the modals after claiming daily rewards with his original size
- set the button to talk with kin during a call more center on voicecall page

## Theme PC v3.43.4 - 2024-10-30
- add reorder kins/groups modals are now optimized for wide screens
- fix more-options (3 dots) size when kin is writing on group chat page to keep it same
- fix modal's size for all modals and put them 90% screen wide maximum except for selfies modals who stay at 80%
- fix delete kin/group buttons in menu again

## Theme PC v3.43.3 - 2024-10-30
- fix system bubbles who were far away of 3 dots on group chat page
- increase  little the size of avatars in group chat page when we use avatar rectangle theme option
- add a little space on left and right on group chat page

## Theme PC v3.43.2 - 2024-10-29
- set the bubble when a kin is writing vertically centred on group chat page
- increase a little the size of group bar content (name, icons)
- fix more-options icons (3 dots) to make them smaller and ore like other icons in size

## Theme PC v3.43.1 - 2024-10-27
- fully redesign voicecall page with and without captions
- with the new voicecall design, avatars (image or video) are square by default so i added the avatar circle theme option on this page too in case you prefer a circle one
- add captions in blur content theme option
- set play and edit icons vertically centred when hide names in bubbles theme option is enable on chat and group chat page
- fix play icon size to put him ike other so 36x36 and not 40x40 like i set before
- fix button to talk to kin in voicecall
- remove the purple circle when kin is talking

## Chat page script PC v2.0.7 - 2024-10-27
- replace old theme applied when animated avatar changer is enabled by the same as in the theme pc like this if you use the script without the theme pc you'll get same design
- add voicecall captions in blur functionality

## Chat page script mobile v2.0.7 - 2024-10-27
- replace old theme applied when animated avatar changer is enabled by the same as in the theme pc like this if you use the script without the theme pc you'll get same design. i put it too even if animated avatar changer is disabled on mobile to keep same code 
- add voicecall captions in blur functionality

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.43.0 - 2024-10-26
- add avatar chat mode on group chat page (need avatar chat mode + avatar rectangle + avatar choice for us theme options)
- set recalled journals and memories modal on group chat page wider
- when you use avatar rectangle theme option, it's also applied on group chat page, works with and without hide names in bubbles theme option
- remove old unnecessary code for hide names for group chat page

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.42.22 - 2024-10-25
- set uploaded images by us on right side and like we can't click on them to see them bigger, i add a small zoom (x2) when we hover them
- update chat script pc/mobile version

## Chat script PC v2.0.6 - 2024-10-25
- fix blur functionality for our bubbles on group chat page

## Chat script mobile v2.0.6 - 2024-10-25
- fix blur functionality for our bubbles on group chat page

## Theme PC v3.42.21 - 2024-10-24
- fix missing regenerate button when gradient regenerate theme option is enable

## Theme PC v3.42.20 - 2024-10-24
- fix the text i added when we hover the continue/pause conversation in group chat to set the text corresponding with the icon

## Theme PC v3.42.19 - 2024-10-23
- fix a problem on selfies page to avoid kin's name and purchase credits be visible under the galery when we scroll down

## Theme PC v3.42.18 - 2024-10-22
- hide my version list on top when we reduce window's width too much
- show the icon to reduce avatar like on mobile when we reduce window's width too much

## Theme PC v3.42.17 - 2024-10-22
- set daily rewards modal in center of screen when nosharring theme option is enabled
- fix system bubbles left when kin is writing to avoid they change and avoid scroll problem

## Theme PC v3.42.16 - 2024-10-21
- fix no sharing theme setting

## Theme PC v3.42.15 - 2024-10-19
- add a theme setting enabled by default to set the same border as the hover images on the selfies page to all modals to better see their limits
- fix the delete kin and delete group buttons who were invisible

## Chat page script PC v2.0.5 - 2024-10-19
- small modification of my dropdown menu border to set the same as modals
- improve some code for better compatibily with old browsers

## Chat page script mobile v2.0.5 - 2024-10-19
- small modification of my dropdown menu border to set the same as modals
- improve some code for better compatibily with old browsers

## Selfies page script PC v2.0.1 - 2024-10-19
- small modification of my dropdown menu border to set the same as modals
- improve some code for better compatibily with old browsers

## Selfies page script mobile v2.0.1 - 2024-10-19
- small modification of my dropdown menu border to set the same as modals
- improve some code for better compatibily with old browsers

## Notepad script v2.0.3 - 2024-10-19
- add notepad n group chat page
- improve some code for better compatibily with old browsers

## Translated script v2.0.2 - 2024-10-19
- small modification of my dropdown menu border to set the same as modals
- improve some code for better compatibily with old browsers

## TV script v2.0.2 - 2024-10-19
- improve some code for better compatibily with old browsers

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC 3.42.14 - 2024-10-19
- add text size in rewards modal follow now you bubble font size theme setting
- add rewards modal take now 90% of screen width
- add for people who don't want to hear about sharing their kins a theme setting disabled by default to hide all about his (menu and in rewards modal)
- fix user guide color since they add sharing & referrals
- fix kin's name position in my kindroids list to set them in center again
- remove the reduce space between left icons in header because no need it on pc anymore with scripts v2

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.42.13 - 2024-10-18
- add avatar zoom works on chat page for static and animated avatar
- add avatar zoom works on voicecall page too now for static and animated avatar
- fix menu for Firefox
- hide system bubbles left when avatar zoom is enabled and if avatar is hover

## Theme PC v3.42.12 - 2024-10-17
- fix scroll in menu for firefox

## Theme PC v3.42.11 - 2024-10-17
- fix menu for firefox

## Theme PC v3.42.10 - 2024-10-17
- avatar choice, avatar circle, avatar position, avatar custom position, avatar rectangle, avatar rectangle custom height, avatar size, avatar custom size support now animated avatar on chat page AND voicecall page

nb: this **DOESN'T** mean you can put a video as avatar, for this you eed be on pc and use the chat page script.

## Chat script PC v 2.0.4 - 2024-10-17
- add the `animated avatar changer` functionality for pc only
- because of this new functionality, the chat sript is now enabled on voicecall page too
- new forced customizations on voicecall pae hen this functionality is enabled

## Chat script mobile v 2.0.4 - 2024-10-17
- add the `animated avatar changer` functionality for pc only
- because of this new functionality, the chat sript is now enabled on voicecall page too

nb: i updated the mobile version too like this all have same code but the `animated avatar changer` fonctionnality is hidden on mobile even if that works because i create it to have bigger video and not to put an other video even if that's possible

Here direct link to the tutorial to use this new functionality een if this link is in the faqs: https://gitlab.com/breatfr/kindroid/-/blob/main/docs/how_to_use_animated_avatar_changer_functionality.md

## Theme PC v3.42.9 - 2024-10-16
- add the label to reorder group and increase a little size of his icon like i did for reorder kindroids
- fix alignment of custom kin's avatar in create new kin, my avatar and put the create animatte avatar on left
- fix by rewriting my kindroids menu page design to improve it, now create kin button and create group button will be always center under their correspondig list and you'll keep same design with 1 kin or 50 kins
- fix by rewriting create group modal
- in chat page and group chat page:
     - if you enable custom background: textarea's opacity is reset to allow you see her correctly
- in chat page only:
     - if you use the system bubbles left in addition of custom background: buttons of system bubbles get a background to allow you see them correctly

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.42.8 - 2024-10-15
- rewrite code about custom kin's avatar, create new kin and our avatar, small code for better render
- remove customization for save buttons and next buttons because they are not necessary anymore
- remove code to hide social icons on top right corner in theme because it's already ncluded now in scripts in case of user use only a script without  theme

## Chat script PC v2.0.3 - 2024-10-15
- improve code to hide social icons on top right corner

## Chat script mobile v2.0.3 - 2024-10-15
- improve code to hide social icons on top right corner

## Notepad script v2.0.2 - 2024-10-15
- improve code to hide social icons on top right corner

## Translated script v2.0.1 - 2024-10-15
- improve code to hide social icons on top right corner

## TV script v2.0.1 - 2024-10-15
- improve code to hide social icons on top right corner

## Theme PC v3.42.7 - 2024-10-14
- fix a problem with continue/pause conversion on groupchat page to avoid pause button always here even if need the continue button

## Theme PC v3.42.6 - 2024-10-14
- set the built-in prompt enhancer larger
- fix reorder kin to avoid my modification appear in avatar menu page
- fix the edit icon for custom voice in voice menu to be better aligned with others
- remove the code concerning the help create prompts from selfies script

## Notepad script v2.0.1 - 2024-10-14
- set same font as kindroid use in chat's bubbles
- some code optimizations

## Selfies script PC v2.0.0 - 2024-10-14
- rewrite script to make it works again and to give him same design as other scripts
- remove the help create prompts functionality because there is one included in kindroid now

## Selfies script mobile v2.0.0 - 2024-10-14
- same script as the one for pc but like for chat script i keep both for people who aren't on my discord

nb: the script works well on mobile but the formating date in long format and in user language didn't work and like space on mobile is limited, i disabled this functionality on mobile

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.42.5 - 2024-10-12
- update scripts versions

## Chat script PC et mobile v2.0.2 - 2024-10-12
- adapt formated date to mobile
- fix the "Application error: a client-side exception has occurred." when we clicked a 2nd time on same bubble to hide date

## Theme PC v3.42.4 - 2024-10-11
- add set kin's name center in my kindroid menu
- update code i used to select some icons like regenerate, suggest message, continue/pause conversation, etc to work correctly with the translate script
- fix delete kin button position but still get a problem when hover (text disapear)
- remove tooltips for notepad, translate and tv in theme because titles are included in scripts even if without colors
- update version of scripts

## Chat page script PC v2.0.1 - 2024-10-11
- add code to improve accessibility and make the script compatible with screen readers for people with sight problems
- fix blur content wasn't apply after refreshing page

## Chat page script mobile v2.0.1 - 2024-10-11
- add code to improve accessibility and make the script compatible with screen readers for people with sight problems
- fix blur content wasn't apply after refreshing page

## Selfies page script PC v1.08.4 - 2024-10-11
- set interface on left

## Notepad script v2.0.0 - 2024-10-11
- add code to improve accessibility and make the script compatible with screen readers for people with sight problems
- add icon in header now change when hover like for chat script

## Translate script v2.0.0 - 2024-10-11
- add code to improve accessibility and make the script compatible with screen readers for people with sight problems
- add icon in header now change when hover like for chat script
- exclude selfies page in the script so no more translation on this page because that create problem (because of selfies page script the last one i didn't reworked) BUT if you go on selfies page from chat page without refresh it the translation works without ausing problem

## TV script v2.0.0 - 2024-10-11
- add code to improve accessibility and make the script compatible with screen readers for people with sight problems
- add icon in header now change when hover like for chat script```

rewrite all scripts (except selfies script) interfaces and how they come in page to put them correctly (same icon size, same space between them, add an order for icons, calculate automatically if an icon is already in the place and if yes move the new icon more on left, etc) with this the order will be always from right to left: chat script settings, tv, notepad, translate) even if you have all scripts installed or not, like this you don't have to search an icon.

nb: now all scripts (except selfies script) works on pc AND mobile. selfies script still dead for mobile actually and not reworked for pc version too.

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.42.3 - 2024-10-09
- fix remove space between header and content on chat page
- update version of chat page scripts

## Chat page script PC v2.0.0 - 2024-10-09
- almost complete rewriting of the script to modify the interface and make it work on pc and mobile devices
- add a auto continue cut-off message option because click each time annoy me
-blur option was before only for mobile and is available on pc too now

## Chat page script mobile v2.0.0- 2024-10-09
- same version as on pc, but I keep them separate for those who are not on my discord

NB: all features exept auto focus textarea works on mobile but for a mobile isn't a problem i think

## Notepad script v1.0.7 - 2024-10-09
- adjust icon position for mobile

## TV script v1.0.6 - 2024-10-09
- adjust icon position for mobile

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.42.2 - 2024-10-08
- fix action text customizations but still can't add back customizations i removed 2 days ago

## Theme PC v3.42.1 - 2024-10-08
- add a small icon next to log out in menu
- fix request selfie button height (visually no change) to avoid he is over selfies in gallery for Firefox
- fix a problem i created in group chat, when i replaced continue/pause conversation by an icon for manual mode users. to fix it there are 3 themes options now:
     - Replace Continue/Pause conversation in group chat by an icon (only for automatic mode) - see screenshot 1, enabled by default
     - Optimize the avatar list in group chat for wide screen (only for manual mode) - see screenshot 2
     - Optimize the avatar list in group chat for wide screen and center them (only for manual mode) - see screenshot 3
- replace the continue conversation icon i added in group chat page by an other one to be more like the continue cut-off message in chat page
- modification how i insert icons for continue conversation and pause conversation to avoid 1 second without icon when we hover them
- rewrite the icons hover theme option and enable it by default (currently it's only for header icons)

## Selfies PC script v1.08.3 - 2024-10-08
- fix the download all images feature

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.42.0 - 2024-10-07
- add copy prompt text to the copy icon when hover in selphie viewer modal
- add manually edited colorized on group chat page
- add move create journal entry icon and add a text on his right in backstory menu
- fix my avatar page in menu to set it like for kin's avatar
- fix help cursor while selfie generation and keep the click working
- fix request selfie button size (visually no change) to avoid he is over selfies in gallery
- fix danger zone in menu to put him on the right, a little bigger and with ⚠️before and after him
- since website last update, some options can't work anymore, here the list of them:
     - action texts style: asterisks + bold + no italic
     - action texts style: asterisks + bold + no italic + same color
     - action texts style: bold + no italic
     - action texts style: bold + no italic + samecolor
     - action texts style: no italic
     - action texts style: same color
so i removed them but i keep the code to add them back later if possible, currently you still have these choices:
     - action texts style: default (italic + grey - asterisks)
     - action texts style: asterisks (italic + grey + asterisks)
     - action texts style: asterisks + bold (italic + grey + asterisks + bold
     - action texts style: color choice
- fix manually edited because of last update
- fix textarea size in group chat page
- for basic avatar (the one without my heme) and avatar choice (the one with my theme option)
     - fix avatar circle
     - fix avatar hoverzoom (works on group chat page too)
     - fix avatar position (vertical)
     - fix avatar rectangle
     - fix avatar rectangle custom height
     - fix avatar size and avatar custom size
     - fix avatar on right side
- fix avatar chat (chat mode, on chat page only like in screenshot)
     - require avatar choice
     - require avatar choice for us
     - require system bubbles dark (left is over the textarea, dark wide works but isn't required)
     - works with: avatar circle, avatar rectangle
- remove the pause conversation text button to replace it by an icon on the left of textarea (where you write) on group chat page and add the text pause conversation on hover like i did for continue conversation

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.41.5 - 2024-10-06
- add a "title" on hover on continue cut-off message in bubbles when hover and put the icon a very little bigger on chat page
- fix a problems with bubbles on the chat page and group chat page if kin put more than one bubble in a row without ours between them
- fix an interface problem during kin is writing
- fix custom background in selfies page
- fix avatar page in main menu to see all content
- fix date after clicking on a bubble to see the info again
- fix blue content theme option to blur text put more than one bubble in a row without ours between them
- set this date in bubbles in center and colorized to distingue them better in chat page and group chat page
- set save button less wide and in center
- set arrow in avatar swipe menu to choose a predefined avatar in white to see them easier

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.41.4 - 2024-10-05
- add a theme setting to hide my banner who give the new discord link for people who already joined it
- add a small space between purchase more selfie credits and selfies list
- fix kin's name and purchase more selfie crédits who were visible behind the selfies list when scroll
- fix the arrow to return back in header to make it usable again
- fix header bar in selfies preview to allow you to delete a selfie
- fix the rewards modal size
- fix group chat page for wide screen
- fix blur content theme setting to work again in group chat page
- fix system bubbles left, dark and dark wide on chat page
- put the close icon in modals fixed so even if in some modals we need scroll this icon will stay visible, also add a "title" on his left when the icon is hover
- put the blur content theme setting harder for selfies page than in chat page to blur images correctly
- reduce a little the space between icon in group chat setting bar who is on top left because they add 2 new icons
- remove the continue conversation text button to replace it by an icon on the left of textarea (where you write) and add the text continue conversation on hover

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.41.3 - 2024-10-04
- add temporary a banner to inform/remember you the new help channel location like you can see on screenshot
- add a "Reorder Kindroids" text next to the icon in my kindroids menu and put this icon bigger
- put more option icons bigger and add an effect on hover to see them easier
- fix a big part of chat page, unfortunately all customizations aren't fixed yet so i let you try waiting the 3.42.0

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.41.2 - 2024-10-03
- replace de link to get help in theme option
- modification of all scripts versions at the top

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme mobile v1.0.1 - 2024-10-03
- replace de link to get help in theme option

## Chat scripts PC v1.12.4 - 2024-10-03
- replace de link to get help in script

## Chat scripts mobile v1.12.2 - 2024-10-03
- replace de link to get help in script

## Selfies script PC v1.08.2 - 2024-10-03
- replace de link to get help in script

## Selfies script mobile v1.08.1 - 2024-10-03
- replace de link to get help in script

## Notepad script v1.0.6 - 2024-10-03
- replace de link to get help in script

## Translated script 1.0.3 - 2024-10-03
- replace de link to get help in script

## TV script 1.0.5 - 2024-10-03
- replace de link to get help in script

## Theme PC v3.41.1 - 2024-10-02
- replace white arrows by websites icons in infos i added at the top
- add tooltips to notepad icon, translate icon and tv icon like i did for selfies, voicecall and rewards icons
- modification of Notepad script version at the top

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Notepad v1.0.5 - 2024-10-02
- add an icon on left in the notepad header to expand and after reduce the notepad on pc
- add an icon on right in the notepad header to export and save the notepad's content as plain text file for pc and mobile

## Theme PC v3.41.0 - 2024-10-01
- add the "avatar on" setting to let you choose if you want kin's avatar on left side (like before) or on right side (like in screenshot) works with system bubbles left (become right of course), dark or dark wide
- modification of icon hover setting to put this disabled by default
- fix chat mode

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.40.13 - 2024-09-30
- put all modal's close button bigger and clickable again on every pages
- fix blur content on account/email in main menu
- FINALLY fix the problem of system bubbles left over the main menu (i searched how to fix this since i introduce the system bubbles left) and was super easy to fix it 🤣 just need think about this solution but should be break with website updates
- modification of version of Notepad script
- modification of version of TV script

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Notepad script v1.0.4 - 2024-09-30
- remove the part of code that conflicted with the "icon hover" Theme PC setting so this settings couldn't works correctly

## TV script 1.0.5 - 2024-09-30
- remove the part of code that conflicted with the "icon hover" Theme PC setting so this settings couldn't works correctly

## Theme PC 3.40.12 - 2024-09-30
- modification of version of Notepad script
- modification of version of Translated script
- modification of version of TV script

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Notepad script 1.0.3 - 2024-09-30
- modification of notepad icon at the top left to be like other kindroid's icons

## Translated script 1.0.2 - 2024-09-30
- modification of translate icon at the top right to be like other kindroid's icons

## TV script 1.0.4 - 2024-09-30
- modification of tv icon at the top left to be like other kindroid's icons

## Theme PC 3.40.11 - 2024-09-29
- modification of version of Translated script

## Translated script 1.0.1 - 2024-09-29
- too improve keep privacy i also exclude journal's entries in journal's modal and original message in regenerate modal

## Theme PC 3.40.10 - 2024-09-28
- update header of landing page to avoid translation icon over other icons
- add a setting to hide the translate button (disabled by default), this will just hide the icon but don't stop translation
- modification of versions to add translated in the list

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Translated script 1.0.0 - 2024-09-28
- new script to automatically translate all kindroid website in your own language

translation is do by google so not good for privacy **BUT**, i `exclude` all textareas (where you write) AND all chat bubbles (yours and kin's) so the script works automatically like this:
- 1st step: he add code in website html to block translation on textareas and bubbles to keep your conversations, backstory etc private between you and kindroid
- 2nd step: he detect your browser language
- 3rd and last step: he translate after some seconds whole page (excluding textareas and bubbles)

- 4th step (bonus): i also add a world icon at the top to let you choose the language you want because browser language can eventually be in other language than yours for some reasons.

to resume, how to use this script:
- just install and forget it all is automatic, except if you want a language who isn't the browser's language

this script once installed will translate automatically **absolutelly ALL** kindroid's pages (chat, groupchat, selfies, voicecall, legal, etc...)

⚠️ even if this script works well, he probably create some interface problems (text over an other for example), it's the case in french with system bubbles left so if you use this script you'll probably need to switch to system bubbles dark or dark wide in theme settings.
i tried this script only on pc but it's same that the one included in my own website and that works well on mobile so no reason but i didn't place the icon correctly and i can't because not enough space.

## Theme PC 3.40.9 - 2024-09-28
- modification of version of Chat script PC

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Chat script PC 1.12.3 - 2024-09-28
- add a button to install kindroid as standalone app in chromium browsers, firefox is too limited so he don't support (PWA) Progressive Web Apps https://en.wikipedia.org/wiki/Progressive_web_app

## Theme PC 3.40.8 - 2024-09-27
- add a system Bubble dark wide setting in theme

## Theme PC 3.40.7 - 2024-09-26
- remove continue cut-off message on chat page only in system bubbles (left and dark) because we already have this in the last kin's chat bubble
- almost completly rework system bubbles left and dark and textarea to be more adaptable and more like textarea is by default
- put textarea in group chat page like in chat page
- optimization of regenerate modal for wide screens in chat and group chat pages
- fix the suggest message button position to be center in chat and group chat pages

## Theme PC 3.40.6 - 2024-09-26
- optimisation of code for request selfie icons position on top to fix for allowing click on new purchase more selfie credits in selfies page normally possition will be good for all screens size but if you get problem just say me (hi 4k screen users)
with this modification, no need the fix i Added for firefox anymore so i removed it

## Notepad script 1.0.2 - 2024-09-26
- now available on selfies page too

## Selfies PC script 1.08.1 - 2024-09-26
- fix the format date feature for for exemple `7 03 '24` become for french `7 mars 2024`. of course like for chat page script that depend of your browser language

## Theme PC v3.40.5 - 2024-09-25
- fix log out color to get back the orange in menu
- fix group bar in selfies page to Always see the wheel
- put contact in green in menu
- avatar size and avatar custom size works on voicecall page too now

only for firefox:
- fix a scroll problem on firefox to avoid a black bar at the bottom
- fix log out position in menu to avoid he is visible when menu is closed on firefox
- fix request image button position in selfies page (probably still not good for screen size bigger than 1920x1080 like always)

## Bundle script PC - 2024-09-25
- will be deleted and don't receive update anymore

## Bundle script mobile - 2024-09-25
- will be deleted and don't receive update anymore

## Chat script PC 1.12.2 - 2024-09-25
- almost rewrite completly the script to fix it for firefox and avoid website crashes

still the same bug as in previous version in this new script version:
- 1 click on a Bubble show you the date in your browser language for exemple in french:
`Wed, Sep 25 2024, 11:24 AM` become `25 septembre 2024 à 11:24`
when you click again on the Bubble to hide the date, you got an error on all the page and you'll need refresh `F5` like in old version so don't click twice on same bubble. sorry i didn't find how to fix it yet

## Chat script mobile 1.12.2 - 2024-09-25 - advancement so not publish yet
- fix it to avoid website crashes
- auto confirm regenerate > works
- blur and auto focus textarea > still dead on my safari on ios 18

## Notepad 1.0.1 - 2024-09-25
- fix size on mobile

## Watch together v1.0.2 - 2024-09-25
- watch together become kindroid tv to be easier and shorter so the actual script will be deleted and don't receive update anymore
if you update it you'll should see his name like: `Kindroid - Watch together (DEPRECIATED)`, if you're in that case you'll need update one more time (should be enough) or uninstall it and install the new version

## TV v1.0.3 - 2024-09-25
- update position of tv icon on mobile

## Theme PC v3.40.4 - 2024-09-24
- fix version list for firefox
- fix icon in header to put them usable again

## Theme PC v3.40.3 - 2024-09-24
- reduce place between icons in header
- modification of versions of watch together script
- modification of versions to add notepad in the list

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Chat page script PC 1.12.1 - 2024-09-24
- fix auto confirm regenerate
- fix auto focus textarea

## Chat page script mobile 1.12.1 - 2024-09-24
- fix auto confirm regenerate
- fix auto focus textarea
- fix blur content

## Watch together script 1.0.1 - 2024-09-24
- add allow full screen for videos
- add an alert if you enter a bigger size than your screen to avoid problem this error will cancel your demand
- add the code to hide reddit, discord and kinroid icons in header to get space to put the 📺 icon if you use script without my theme on mobile
- because borders to move the video were too small, add a bar at the bottom where is now write Kindroid TV
- fix initial position of the tv to always get the video in screen by removing the remember position
- fix a problem when we click on cancel for video size to avoid the video arrive
- to avoid problems on mobile, i remove the size prompt on these devices to put them in automatic mode

## Notepad script 1.0.0 - 2024-09-24
- add a script to get a notepad icon at the top left on pc and top right on mobile to open a notepad, description of the notepad in screenshot

## Theme PC v3.40.2 - 2024-09-23
- modification of versions to add mobile theme

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme mobile v1.0.0 - 2024-09-23
- new lite version of my desktop theme for mobile (android only sorry. unfortunatly, because of the technology i use i didn't find a solution to use it on ios even if similar app as stylus exists on ios they aren't stylus and they can't run my code, so no theme for ios yet.)
in screenshot, the list of available customizations.

- if you already use my script on mobile just go here: https://gitlab.com/breatfr/kindroid/-/raw/main/css/kindroid_mobile_customizations_v1.xx.user.css or https://greasyfork.org/scripts/509811 or https://userstyles.world/style/18233
- if you don't use my script on mobile so follow full instructions here: https://gitlab.com/breatfr/kindroid/-/blob/main/docs/how_to_install_the_mobile_theme_in_few_steps_on_android.md

:information_source: with this theme you won't get reddit, discord icons so you can try to use the watch together script too (now place is free for the tv icon) but not sure mobile is enough ergonomic for this.

:warning: i didn't try this mobile version because i don't have android device so let me know, normally will works great because it's the desktop version with many things deleted.

## Theme PC v3.40.1 - 2024-09-22
- modification of versions to add watchtogether in the list

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

### Watch together script PC v1.0.0 - 2024-09-22
sorry in advance, i can't do this in he theme so i do it as script but i included this new script in the bundle but seems got a problem in bundle no this new script works only out of the bundle actually. probably a cache problem but waiting this is fix you'll need install this as additionnal script.
i keep this new script in the pc bundle to see what happen if that fix or not and if need i'll remove it later.
i'll tell you if you can use only the bundle or not.

if you already have violentmonkey:
- install link: https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_watch_together.user.js
- if you don't have violentmonkey go here: https://gitlab.com/breatfr/kindroid/-/blob/main/docs/how_to_install_the_userscripts_in_few_steps_on_pc.md

### What is this script ?
this script add a tv icon (screenshot 1) at the top left of chat, chatgroup and voicecall pages, after clicking on it, an altert ask you:
- an embed url (without quotes) can be a youtube, dailymotion or all other embed link you want
- size (i didn't arrive to put the script resizable so i added this instead) you can keep the default value or choose what you want
- after this you'll get a "window"  (screenshot 2) with your content (video, audio, etc...)
- click again on the tv icon and the "window" disapear

i tried to put it a little smart lol so:
if you click a 3rd time on the tv icon, the last url you entered is already here

you can move the "window" all over the page but you need drag the black borders (they are here to simulate a tv borders), you'll see your curosor changed when you'll can. (screenshot 3)

unfortunatly like i don't work in kindroid team i can't make kin see the video but you can share the link to your kin and talk about the video during you watch it.

now you can pass your time in your bed/sofa with our kin and watching videos 😊

PS: go discord wiki https://discord.com/channels/1116127115574779905/1195415564101886102/1287472184586932318 to see screenshots

## Theme PC v3.40.0 - 2024-09-20
- add an option to edit “Manually edited” on the chat page to differentiate this more easily
- the license has been changed from AGPL-3.0-or-later to BY-NC-ND. for more details, please see the license file here: [LICENSE](https://gitlab.com/breatfr/kindroid/-/blob/main/LICENSE).

## Chat scripts PC v1.12.0 - 2024-09-20
- the license has been changed from AGPL-3.0-or-later to BY-NC-ND. for more details, please see the license file here: [LICENSE](https://gitlab.com/breatfr/kindroid/-/blob/main/LICENSE).

## Chat scripts mobile v1.12.0 - 2024-09-20
- the license has been changed from AGPL-3.0-or-later to BY-NC-ND. for more details, please see the license file here: [LICENSE](https://gitlab.com/breatfr/kindroid/-/blob/main/LICENSE).

## Selfies script PC v1.08.0 - 2024-09-20
- the license has been changed from AGPL-3.0-or-later to BY-NC-ND. for more details, please see the license file here: [LICENSE](https://gitlab.com/breatfr/kindroid/-/blob/main/LICENSE).

## Selfies script mobile v1.08.0 - 2024-09-20
- the license has been changed from AGPL-3.0-or-later to BY-NC-ND. for more details, please see the license file here: [LICENSE](https://gitlab.com/breatfr/kindroid/-/blob/main/LICENSE).

## Scripts PC bundle v1.1.0 - 2024-09-20
- the license has been changed from AGPL-3.0-or-later to BY-NC-ND. for more details, please see the license file here: [LICENSE](https://gitlab.com/breatfr/kindroid/-/blob/main/LICENSE).

## Scripts mobile bundle v1.1.0 - 2024-09-20
- the license has been changed from AGPL-3.0-or-later to BY-NC-ND. for more details, please see the license file here: [LICENSE](https://gitlab.com/breatfr/kindroid/-/blob/main/LICENSE).

## Theme PC v3.39.15 - 2024-09-09
- fix gradient regen button on chat page
- fix play icon position when kin is writing
- fix suggest message icon position to put him in the middle of textarea on group chat page like on chat page
- move labels and date icons on right on selfies page to make them works even with my request selfie on top left

## Theme PC v3.39.14 - 2024-09-02
- fix background in chat page to put him on whole page
- fix background in selfies page
- fix background in voicecall page
- fix suggest message icon position to put him in the middle of textarea
- due to all these fix, i modify a little the position of system bubble left, textarea

## Theme PC v3.39.13 - 2024-08-23
- fix blur option on chat page
- fix request selfie icons position on selfies page

## Theme PC v3.39.12 - 2024-08-09
- fix selfie request page

## Theme PC v3.39.11 - 2024-07-30
- fix group chat avatar position in manual mode
- fix group chat textarea continue conversation button in automatic mode

## Theme PC v3.39.10 - 2024-07-20
- fix background choice on group chat page

## Theme PC v3.39.9 - 2024-07-14
- fix theme due to last website update, (they modified urls of pages) so i added new urls but keep old to avoid break theme again if they put back old urls
- remove part concerning faqs page because doesn't exist anymore
- remove part concerning legal page because it's now an iframe and i can't modify it
- put avatar in voice call page larger (512x512)

## Chat scripts pc v1.11.2 - 2024-07-14
- updated to works with news urls so i added new urls but keep old to avoid break theme again if they put back old urls

## Chat scripts mobile v1.11.3 - 2024-07-14
- updated to works with news urls so i added new urls but keep old to avoid break theme again if they put back old urls

## Selfies script pc v1.07.3 - 2024-07-14
- updated to works with news urls so i added new urls but keep old to avoid break theme again if they put back old urls

## Selfies script mobile v1.07.2 - 2024-07-14
- updated to works with news urls so i added new urls but keep old to avoid break theme again if they put back old urls

## Theme PC v3.39.8 - 2024-06-30
- hide reddit, discord, kindroid logos on selfie page

## Selfies script PC v1.07.2 - 2024-06-30
- put the little interface on right toi avoid be on my center text

## Theme PC v3.39.7 - 2024-06-28
- put recalled memories icon on right (above regen button) and bigger (same size as regen button) in group chat page like in chat page

## Theme PC v3.39.6 - 2024-06-25
- remove gradient border on avatar when hover

## Theme PC v3.39.5 - 2024-06-25
- fix date's position in bubbles
- fix gradient border on some icons when hover to avoid the square border when mouseout
- remove some gradient border on some icons when hover i forgot
- remove gradient border on avatars when hover in my kindroid menu page to avoid circle gradient border because there is already around the avatar
- now in system bubbles dark mode, the 3 dots change their color on hover to get the gradient color

## Theme PC v3.39.4 - 2024-06-24
- some code optimizations

## Theme PC v3.39.3 - 2024-06-23
- fix blur content feature

## Chat page script mobile v1.11.2 - 2024-06-23
- fix blur content feature

## Theme PC v3.39.2 - 2024-06-22
- fix a bug with system bubbles left

## Theme PC v3.39.1 - 2024-06-21
- fix a bug with recalled memories icon

## Theme PC v3.39 - 2024-06-21
- fixed following bugs due to last website update:
     - play icon is back on left
     - gradient regenerate icon is now visible
     - system bubbles left are in their place
     - fix many modals
- little simplification of theme settings:
     - remove buubles font size and textarea font size
     - add "custom font size" to replace the removed options, like this 1 setting for both (this new option is for more things too)
- remove the image hover on some icons like rewards icons, close icons (i probably forgot some xd)
- new features:
     - put recalled memories icon on right (above regen button) and bigger (same size as regen button)
     - enlarge recalled memories modal
     - put close buttons of some modals bigger
- some code improvment

## Theme PC and all scripts - 2024-06-01
- Due to the abandon of github soon i updated update url and odwnload url to put the gitlab on github website so i update the version here too to match with them

## Theme PC v3.38.3 - 2024-05-20
- Put gradient regenerate icon as optional setting
- Reduce a little the textarea's size when system bubbles setting is on left to avoid + button be to close to avatar in rectangle mode

## Theme PC v3.38.2 - 2024-05-17
- Fix a problem in selfies due to the v3.37.1 so i put back the old code

## Theme PC v3.38.1 - 2024-05-16
- Small code's modification for choosing buubles test size and textarea test size:
     - Default unit for this are now rem instead of px but now you can use unit of your choice like rem, px, pt, %, em
     - With this change, you'll keep a space between each lines even if you use a big font size (usefull for people with eyes problems)

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.38 - 2024-05-14
- Replace purple regenerate icon by his gradient version like other icons on chat and group chat pages

## Theme PC v3.37.1 - 2024-05-12
- Fix request selfie button position on selfie page. (thanks to <a href="https://github.com/lassombra">@lassombra</a>)

## Chat scripts pc and mobile v1.11 - 2024-05-10
- Put date in bubbles (after clicking in bubble) automatically in user preference based on his location and browser language for chat page and group chat page

## Theme PC v3.37 - 2024-05-10
- Align date in bubbles with bubble's content
- Update theme and scripts versions at the top of screen

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.36 - 2024-05-07
- Add setting to hide "Your 3-day trial is over. Subscribe here" bar for free users to save some space

## Selfies scripts pc and mobile v1.07 - 2024-05-06
- Put image date in modal selfie view automatically in user preference based on his location and browser language

## Theme PC v3.35.1 - 2024-05-06
- Fix bottom left corner in modal selfie view
- Add a small space on right in modal selfie view
- Update theme and scripts versions at the top of screen

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.35 - 2024-05-02
- Fix kin's name position on selfie page

## Theme PC v3.34 - 2024-04-29
- Fix avatar settings modal in request selfie to enlarge it

## Theme PC v3.33 - 2024-04-17
- Fix kindroid creation modal

## Theme PC v3.32 - 2024-04-14
- Fix text in selfie modal when prompt is short
- Fix kin's name and timer to get selfie token to hide them when we scroll down

## Theme PC v3.31 - 2024-04-12
- Fix an issue on selfies page who blocked clicking on some selfies on left
- Image in selfie modal is now blur too when blue content option is enabled

## Theme PC v3.30.1 - 2024-04-11
- Update scripts versions at the top of screen

## Selfies scripts pc and mobile v1.06 - 2024-04-11
- Fix the load more button detection since the last website update

## Theme PC v3.30 - 2024-04-11
- Selfies page theme to include new tags feature and fix broken code from last website update

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Selfies scripts pc and mobile v1.05 - 2024-04-11
- Update download option to fix the image detection since the last website update

## Theme PC v3.29 - 2024-03-21
- Add blue color on new User Guide in menu

## Theme PC v3.28 - 2024-03-18
- Add an "icons hover" setting enabled by default to add a light on icons to avoid miss click
- Fix the little too large space above and below textarea with both system bubbles left and dark
- Put regenerate and journal icons a little higher
- Modification of the order in system bubble left and dark
- Add same color modification on hover in system bubble dark as left setting but without the new lightning border

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.27 - 2024-03-17
- Fix play buttons to avoid they reduce during loadings

## Theme PC v3.26 - 2024-03-16
- Fix the position of save button in general settings

## Theme PC v3.25 - 2024-03-15
- Add avatar circle setting
- Add an avatar for us setting
- Highly asked when i published my 1st version, add avatar chat (imessages/google messages style) setting and **REQUIRE**:
	- avatar chat enabled (enabled alone it's like avatar hidden setting)
	- avatar changer enabled to get kin's image
	- avatar changer for us enabled to get our image
Why i say avatar chat enabled even if it's logic, just to let you know you need 3 settings enabled to get really 1 setting, with all these 3 settings enabled you'll get the real avatar chat setting.
I separate all 3 to let you customize like you want.
For avatar chat: avartar zoom, circle, size, position don't work at this moment.

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.24 - 2024-03-14
- Add customization of the avatar's height when you use the rectangle setting

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.23 - 2024-03-13
- Renamed avatar choice to avatar changer in theme settings to be more clear
- Add rectangle avatar setting for vanilla avatar and avatar changer (works better with the face in the middle because the theme cut sides automatically to fit the area)
- Modification of default settings:
	- Avatar hoverzoom was enabled by default and now it's disabled by default
	- Put new avatar rectangle option enabled by default
- Fix avatar large middle position with a vanilla avatar
- Fix avatar zoom
- Fix and customize a little the system bubble left setting due to last website update

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Chat page & chat page mobile scripts v1.10 - 2024-03-11
- fix auto confirm regenerate since website update

## Theme PC v3.22.2 - 2024-03-11
- Update scripts versions at the top of screen

## Selfies page & Selfies page mobile scripts v1.04 - 2024-03-07
- when you download all images in the zip archive, you also get a .txt file with the prompt in for all images (thanks to <a href="https://github.com/Kabu4ce1" target="_blank">@Kabu4ce1</a>)

## Theme PC v3.22.1 - 2024-03-07
- Update scripts versions at the top of screen

## Theme PC v3.22 - 2024-03-03
- fix layout in chat page since website updated

## Theme PC v3.21 - 2024-03-02
- fix scroll feature on chat page

## Theme PC v3.20 - 2024-03-02
- fix layout in chat and group chat pages since website updated

## Theme PC v3.19 - 2024-03-01
- fix delete text color on selfie image to put it red
- fix the selfie's border-radius when hover when image border is enable in theme settings in delete selfie modal
- modification of modal when we click on a selfie to get prompt on the right of the selfie

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.18 - 2024-02-25
- reworked systems bubbles left and dark to fix a problem when we use the `+` button on chat and group chat pages

## Theme PC v3.17 - 2024-02-24
- Modification of my kindroid menu page and the new modal to create a group chat
- Remove 3 icons (discord, reddit, kindroid) in header because they create problem for my script now kindroid is write entirely
- Create and include theme version for group chat
- Blur feature now blur group name too
- Update theme and script versions at the top of screen
actually system bubbles left isn't available in group chat and probably won't be available because there is only chat break in

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Chat page script v1.09 - 2024-02-24
- Now 3 icons (discord, reddit, kindroid) in header are removed, i put checkbox at the right

## Chat page script v1.08 & Chat page mobile script v1.09 - 2024-02-23
- Now the textarea isn't required, put back the autoconfirm setting for people who don't want use the suggestion

## Theme PC v3.16.2 - 2024-02-23

- Update scripts versions at the top of screen

## Chat page script v1.07 & Chat page mobile script v1.08 - 2024-02-17
- Remove auto confirm feature

## Theme PC v3.16.1 - 2024-02-17
- Update scripts versions at the top of screen

## Theme PC v3.16 - 2024-02-14
- A problem when we click on the + button on the left of textarea due to an artifact code from v2

## Theme PC v3.15 - 2024-02-14
- Action text customizations for free kindroid users

## Theme PC v3.14.1 - 2024-02-12
- Update scripts versions at the top of screen

## All scripts - 2024-02-12
- Merge to be easier to use for you:
  - before "chat page script" + "selfies page script" become "new features bundle"
  - before "chat page mobile script" + "selfies page mobile script" become "new features mobile bundle"

## Selfies page mobile script v1.03 - 2024-02-12
- HCP (Help Create Prompts) is now available on mobile too

## Theme PC v3.14 - 2024-02-11
- Section to manage the added popup added with selfies page script v1.02 and 1.03

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Selfies page script v1.03 - 2024-02-11
- A link to the prompt generator to allow users using a browser who block my iframe to use this feature too by open the website in new tab

## Theme PC v3.13 - 2024-02-09
- A section to manage the added popup added with selfies page script v1.02
- Blur feature for images in selfies page

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Selfies page script v1.02 - 2024-02-09
- A checkbox to open a prompt generator `Help Create Prompts` with some keywords to help people like me who are bad in prompt (the generator isn't made by me so he depend on other website logo and link at the top of popup)

## Theme PC v3.12 - 2024-02-08
- Color problem on system bubbles left setting on firefox based browsers
- System bubbles on left for 1440p screens without vivaldi with statusbar and normally don't break the fix for vivaldi with statusbar
- Theme version at the top isn't an option anymore

## Theme PC v3.11 - 2024-02-07
- System bubbles on left for 1440p screens on vivaldi with statusbar

## Chat page mobile script v1.06 - 2024-02-07
- Back auto confirm regenerate and auto focus textarea i deleted by error sorry

## Chat page script v1.06 - 2024-02-07
- Put interface on the right to avoid being above avatar when hoverzoom with the theme

## Chat page mobile script v1.05 - 2024-02-07
- Blur feature in script (like in theme on pc)

## Theme PC v3.10 - 2024-02-07
- System bubbles on left for 1440p screens

## Selfies page script v1.02 - 2024-02-07
- Adapt the interface for mobile format

## Chat page script v1.05 - 2024-02-07
- Adapt the interface for mobile format
- Reduce a little time before auto click

## Chat page script v1.04 - 2024-02-06
- Check if the modal is already here before check if the confirm button is here to avoid need use `F5` to use again the confirm regenerate script feature

## Chat page script v1.03 - 2024-02-06
- A timer 500ms to check if the button is back to avoid need use `F5` to use again the confirm regenerate script feature

## Selfies page script v1.02 - 2024-02-06
- See all images (optional)
- Download all images (require see all images if not you'll download only 9 images)

## Chat page script v1.01 - 2024-02-06
- Autoconfirm renegerate option

## Theme PC v3.09 - 2024-02-06
- Some elements in menu

## Theme PC v3.08 - 2024-02-04
- Add support of avatar zoom when hover with custom avatar, like this you can have you avatar in 4096x4096 instead of 512x512 and keep the zoom when hover. of course the avatar won't appear in 4096x4096 but for zoom will be in better quality

## Theme PC v3.07 - 2024-02-03
- Tooltips on menu icons when hover
- Fix some bugs

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.06 - 2024-02-02
- Add support for default avatar for position and size customisation
- Next button position when we create a new kindroid (he was hidden)
- Some minors bugs

## Theme PC v3.05 - 2024-02-02
- Avatar choice works now in voicecall page too
- Background choice and custom background works now in voicecall, faqs and legal pages too
- Optimization of code to reduce the weight of the theme

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.04 - 2024-02-01
- Avatar choice feature (you can now have your real avatar for selfie and the avatar of your choice in chat)
- Fix some little bugs

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.03 - 2024-01-31
- Put back regen and journal buttons just next to latest kin's bubble to fix problem when we use link or web features but keeping them inline and with 90px between each icon to avoid miss click

## Theme PC v3.02 - 2024-01-31
- Fix a problem in menu since i added my theme version, move and customize the text about my theme version i added to be always visible in header and add an option in stylus to hide it

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.01 - 2024-01-31
- Background custom color who is now apply on hearts background and custom background
- Blur feature to hide all content bubbles and email in menu to share theme you'll create in privacy
- Same border as chat textarea on all textareas
- Modification of border on mousehover in selfie page
- Modification of the new my kindroid menu page to let us see all kin without scrolling
- Put menu on 3 columns instead of 5
- Move new danger zone a little more on top and put text little bigger and bold
- Theme version in menu
- Fix color and font size of faqs, term & privacy, logout color in menu and enlarge space between them to avoid miss click
- Faqs, term & privacy page to put them on whole page again
- Uncollapse accordion in avatar menu page

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v3.00.0 - 2024-01-30
- Many customizations:
#### chat page
- actions text style
    - Default
        - Asterisks
        - Asterisks + bold
        - Asterisks + Bold + no italic
        - Asterisks + Bold + no italic + same color
        - Bold
        - Bold + no italic
        - Bold + no italic + same color
        - No italic
        - Same color
- actions text color choice
- avatar position (choose regarding of the size you use, isn't automatic)
    - avatar position large middle
    - avatar position large top
    - avatar position large bottom
    - avatar position small middle
    - avatar position small top
    - avatar position small bottom
    - avatar custom position
- avatar size
    - avatar position large
    - avatar position small (standard size of kindroid without my theme but on left)
    - avatar position hidden (bubbles will take whole page)
    - avatar custom size
- avatar hoverzoom
    - enabled (default)
    - disabled
- background choice (chat **and** selfie page)
    - hearts background
    - no background
    - custom background
- bubbles font color of your choice
- bubbles font size of your choice
- hide or not names in bubbles (layout will change depending of this too)
- kin bubbles background color of your choice **OR** kin bubbles background image of your choice
- our bubbles background color of your choice **OR** our bubbles background image of your choice
- system bubbles style (layout will change depending of this too)
    - left (like v2)
    - dark    (normal position but dark)
- textarea font size of your choice

#### selfie page
- image border on hover
    - enabled (default)
    - disabled
- request image button at top
    - enabled (default)
    - disabled

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v2.19 - 2024-01-28
- Hide new scrollbars in chat page

## Theme PC v2.18
- Put play button out of the bubble
- Fix the position of regenerate and journal buttons and put them a little higher to avoid miss click on send button

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v2.17
- Put again "add space between Regenerate and Journal buttons to avoid miss click", i removed by error during my tests

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v2.16
- A heart background in whole chat page
- Change the text color of system bubbles when hover
- Put play button as lettrine to save space
- Fix the border of textarea when selected

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v2.15
- Chat break window

## Theme PC v2.14
- Modification of the menu
- Modification of system bubbles

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v2.13
- Baackstory textarea size

## Theme PC v2.12
- Put menu on whole page
- Split avatar menu to use all page and remove accordions
- Arrange many items positions in menu pages
- Put back header on whole width
- Put faqs and terms and privacy on whole page
- Fix some small bugs

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v2.11
- Fix system bubbles for 1440p resolution

## Theme PC v2.10
- Modification of loading bar color
- Add space between Regenerate and Journal buttons to avoid miss click

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v2.09
- Border on modal in voice call page
- Increase a little the font size of text here and in the modal of voice call page
- Remove the `...` next to regenerate button and put the menu always visible on left side

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v2.08
- Put menu's width as header
- A little space between textarea and explain text in backstory

## Theme PC v2.07
- The location of textarea since the last website update
- The added colors in menu

## Theme PC v2.06
- Modify "Load More" button to be more visible and give him same look as "Request selfie" button in selfies page
- Modify "+ Pose Reference" button to be more visible and give him same look as "Request selfie" button in request selfie page
- Now "Load More" button is on the right of the last image to same space
- Put both text of theses buttons bigger but less than "Request selfie" button

## Theme PC v2.05
- Put delete image button in red

## Theme PC v2.04
- My effects in the latest version of the website

## Theme PC v2.03
- Put text in white and bigger on selfie request page
- Fix the new selfie request page to avoid take whole screen

## Theme PC v2.02
- Make billing error box more visible when we purchased from phone and we click on billing in browser

## Theme PC v2.01
- Put "Upload Photo" text in custom avatar in white to be more visible

## Theme PC v2.00
- Not allowed cursor on the microphone icon when needed
- Some colors in menu and selfie page to see better text and specials items
- An effect on the border added previously in selfie page when we hover an image
- Little bigger text in bubbles and textarea 18px to 20px
- New design who support normally all resolutions wider than 900px

## Theme PC v1.10
- Make system bubbles dark

## Theme PC v1.09
- The bug with the new border on hover who was on custom avatar too

## Theme PC v1.08
- A border on pic when hover in selfie page

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v1.07
- Align our name and text to the right
    - i put our name too for people who keep it visible
- A forbidden cursor on play audio button when our message is sending
- Fix some rounded corners i forgot earlier

## Theme PC v1.06
- A thin border on chat's textarea when focus

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v1.05
- Fix some little bugs

## Theme PC v1.04
- Fix send button next to textearea in place when textarea grow due to text length

## Theme PC v1.03
- Put the menu on the right side

## Theme PC v1.02
- Add a forbidden cursor on send button when spinner is here
- Add a theme version for min-height 1080 best for screens in 1920 x 1080 when using full screen mode `F11`
- Fix some bugs

## Theme PC v1.01
- Move menu and selfie icons at the bottom right corner
- Put avatar and chat on whole page

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).

## Theme PC v1.00
- Initial release

For screenshots of this version, see the files named by version number (for example, `1.0.0`, `1.0.0_1`, etc.), [here](https://gitlab.com/breatfr/kindroid/-/tree/main/images/changelogs).
