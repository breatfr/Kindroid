# How to use Animated Avatar Changer functionality

## Why this new functionality?
Kindroid has been allowing us to "transform" a static avatar into an animated one for a while now. Kindroid's animated avatar feature is very good, I'm very impressed and only costs 4 tokens but the resolution is too small 512x512 especially when used with your favorite creator's theme (me) :rofl:

So I added an option to the chat page script, called `Animated Avatar Changer`. Even though it works on mobile too, I hide this option in my dropdown menu on mobile because I created it only to enlarge the animated avatar of Kindroid and not to replace it with another video even if it is possible of course. So since it is to have a larger animated avatar, it is not useful to keep this option on mobile.

## The tutorial
I'm assuming you already have an animated avatar created by Kindroid, so I'm not going to explain how to get one.
So you will have understood, for this tutorial, we use the animated videos created by Kindroid.
<details open>
  <summary><h3>I° Recover videos</h3></summary>
I.a° To start, go to https://kindroid.ai/home/ and check that your avatar is animated, if not, do the necessary.
<hr>
I.b° <code data-sourcepos="6:55-6:77">Right-click</code> on your animated avatar and click <code data-sourcepos="6:55-6:77">Inspect</code>. You should see a window open at the bottom (image 1) or to the side (image 2) displaying the Kindroid source code.
<table>
	<tr>
		<th><h2>Image 1</h2></th>
		<th><h2>Image 2</h2></th>
	</tr>
	<tr>
		<td><img src="https://gitlab.com/breatfr/kindroid/-/raw/main/docs/animated_avatar_changer_1.jpg" alt="Source code 1"></td>
		<td><img src="https://gitlab.com/breatfr/kindroid/-/raw/main/docs/animated_avatar_changer_2.jpg" alt="Source code 2"></td>
	</tr>
</table>

What interests us is:
```html
<video class="no-invert" src="https://firebasestorage.googleapis.com/v0/b/kindroid-ai.appspot.com/o/users%2FotpCQ0OizVTxj4WqvPSFgZjJu6F3%2F6FYwjOwj0silf3GcOrnr%2Fanimations%2Fcn9wKaySTtaQzcfdYhee_talking.mp4?alt=media&amp;token=..." width="100%" height="100%" autoplay="" loop="" playsinline="" preload="auto" style="position: absolute; top: 0px; left: 0px;"></video>
<video class="no-invert" src="https://firebasestorage.googleapis.com/v0/b/kindroid-ai.appspot.com/o/users%2FotpCQ0OizVTxj4WqvPSFgZjJu6F3%2F6FYwjOwj0silf3GcOrnr%2Fanimations%2Fcn9wKaySTtaQzcfdYhee_idle.mp4?alt=media&amp;token=..." width="100%" height="100%" autoplay="" loop="" playsinline="" preload="auto" style="opacity: 1; transition: opacity 0.5s; position: absolute; top: 0px; left: 0px;"></video>
```
and more specifically, what is contained in `src=""` of each `<video>`.

And yes, there are 2 videos that are created by Kindroid in just a few seconds. The 1st video is for when your Kin is talking and the 2nd video is for when your Kin is idle.
<hr>
I.c° <code data-sourcepos="6:55-6:77">Right click</code> on the src link of the 1st <code data-sourcepos="6:55-6:77">&lt;video&gt;</code> then click on <code data-sourcepos="6:55-6:77">Open in new tab</code>.
<hr>
I.d° Save the video by clicking on the <code data-sourcepos="6:55-6:77">3 vertical ...</code> then click on <code data-sourcepos="6:55-6:77">Download</code>.


Normally, if you took the src on 1st `<video>` the avatar should have its lips moving and the proposed file name ends with `talking.mp4`, delete everything before `talking.mp4` for simplicity.
<hr>
I.e° Repeat steps <code data-sourcepos="6:55-6:77">I.c</code> and <code data-sourcepos="6:55-6:77">I.d</code> for the 2nd <code data-sourcepos="6:55-6:77">&lt;video&gt;</code> and this time the filename should end with <code data-sourcepos="6:55-6:77">idle.mp4</code>. For simplicity, keep only the end of the name.

Now that you have the videos saved on your PC, you will have to enlarge them otherwise it is useless.
<hr>
</details>

<details>
  <summary><h3>II° Enlarge videos x2 or x4</h3></summary>
I've used the free version of this tool to enhance avatar videos: <a href="https://app.tensorpix.ai/videos" target="_blank">https://app.tensorpix.ai/videos</a> so for my tutorial, I'm assuming you'll use this one too, even if you don't have to.

### Why this tool?
We can upscale 1 video per account for free, easy to use, no video distortion and the output video keeps a small weight of 2/3MB so it's perfect.

But of course you can do it with the tool of your choice.

II.a° Follow the <a href="https://app.tensorpix.ai/videos" target="_blank">link</a> and create an account (otherwise you won't be able to download the output video).
<hr>
II.b° Upload your 1st video
<img src="https://gitlab.com/breatfr/kindroid/-/raw/main/docs/animated_avatar_changer_3.jpg" alt="Upload video">
<hr>
II.c° Choose <code data-sourcepos="6:55-6:77">400% upscale ultra</code> to get a file in 2048x2048, or <code data-sourcepos="6:55-6:77">200% upscale ultra</code> to get a file in 1024x1024.
<img src="https://gitlab.com/breatfr/kindroid/-/raw/main/docs/animated_avatar_changer_4.jpg" alt="Enhance video">
then scroll down and click on <code data-sourcepos="6:55-6:77">Enhance</code> blue button.
<img src="https://gitlab.com/breatfr/kindroid/-/raw/main/docs/animated_avatar_changer_5.jpg" alt="Enhance button">
<hr>
II.d° Wait a moment and when finished click on <code data-sourcepos="6:55-6:77">3 dots</code> then on <code data-sourcepos="6:55-6:77">Download</code>.
<img src="https://gitlab.com/breatfr/kindroid/-/raw/main/docs/animated_avatar_changer_6.jpg" alt="Download button">
<hr>
II.e° Repeat steps <code data-sourcepos="6:55-6:77">II.b</code> to <code data-sourcepos="6:55-6:77">II.d</code> for the 2nd <code data-sourcepos="6:55-6:77">&lt;video&gt;</code>.

Now you have your kin videos in better quality.
<hr>
</details>

<details>
  <summary><h3>III° Upload your videos on internet</h3></summary>
To do this, I'll let you choose where you want to store your videos.

I've done it on my own server, where my website is hosted for reasons of confidentiality.

You can host your 2 videos wherever you like, under certain conditions:
- If your videos are NSFW, make sure you choose a site that accepts this kind of content.
- Links to your videos must end with `.mp4`.
- If you put your videos in a place that requires you to identify yourself, there's a 99% chance that it won't work properly. Your videos must therefore be accessible `publicly`.

I haven't tried it, but services like imgur should work if you don't put the videos in `private`.

You can store your videos on your [NAS](https://en.wikipedia.org/wiki/Network_access_server) if you have one, as long as it's connected to the Internet.

**I can feel the question coming, so I might as well answer it now: NO, it's not possible to make videos from your PC, they absolutely have to be on the Internet.**

Now you're ready to use my new functionality.
<hr>
</details>

<details>
  <summary><h3>IV° Use the Animated Avatar Changer functionality</h3></summary>
IV.a Click on the <code data-sourcepos="6:55-6:77">Animated Avatar Changer</code> option in my drop-down menu to see a small window on requirements appear, and validate it after reading it.
<img src="https://gitlab.com/breatfr/kindroid/-/raw/main/docs/animated_avatar_changer_7.jpg" alt="Enable the functionality">
<hr>
IV.b After this, my script will ask you the <code data-sourcepos="6:55-6:77">talking video link</code>.
<hr>
IV.c Finaly, my script will ask you the <code data-sourcepos="6:55-6:77">idle video link</code>.
<hr>
IV.d Enjoy 😊
<hr>
:warning: Although the script is active on the Chat page AND the Voicecall page, you <code data-sourcepos="6:55-6:77">must</code> press <code data-sourcepos="6:55-6:77">F5</code> each time you change pages.

I wanted to fix this problem, but it created others, so for the moment I've abandoned the idea.

Whether on the Chat page or the Voicecall page:
- If you have activated the option with links to valid videos and you close the browser or the tab `WITHOUT` deactivating the option then the next time you visit the chat page or the voicecall page the change will be automatic without asking you anything. If you want to change the videos, you `must` deactivate the option and then reactivate it.
- You can click on the video to switch back to a static avatar and vice versa.
</details>
