# How to install Kindroid as an application on desktop

## Before all
Of course you can create a shortcut on your desktop but isn't nice, so here a solution, we'll transform the website in a [Progressive Web App (PWA)](https://en.wikipedia.org/wiki/Progressive_web_app):

To do it, you need my Chat script PC https://gitlab.com/breatfr/kindroid/-/blob/main/docs/how_to_install_the_userscripts_in_few_steps_on_pc.md, because Kindroid devs didn't add this small feature, so I did.

### What the utility of this?
- Have a shortcut on your desktop
- No need open your browser with many tabs :)
- Save some space for the website content

### I'll lost the theme and scripts?
No, because will be in reality in your browser but with some things removed, like a lite version of your browser.

Unfortunately, all browsers don't support this.=, here a short list of ones i tried:
- Brave > supported
- Chrome > supported
- Edge > supported
- Firefox > not supported by default, so you need install this addon https://addons.mozilla.org/firefox/addon/pwas-for-firefox/ before and follow their instructions after, then you'll can use this part of my script
- Opera > seems opera devs removed this part of chromium so not supported
- Opera GX > seems opera devs removed this part of chromium so not supported
- Safari > I don't know

## How to transform the Kindroid website in PWA
After you installed my script, go on https://kindroid.ai/home/.

For Brave / Chrome:
search in the address bar this icon and click on him:

![Install PWA 1](https://gitlab.com/breatfr/kindroid/-/raw/main/docs/install_pwa_1.jpg)

if you don't see it, that can happen, look here:

![Install PWA 2](https://gitlab.com/breatfr/kindroid/-/raw/main/docs/install_pwa_2.jpg)

For Edge
search in the address bar this icon and click on him:

![Install PWA 3](https://gitlab.com/breatfr/kindroid/-/raw/main/docs/install_pwa_3.jpg)

if you don't see it, that can happen, look here:

![Install PWA 4](https://gitlab.com/breatfr/kindroid/-/raw/main/docs/install_pwa_4.jpg)

That's all.

Here the result (the red arrow is to show you where to find your browser's plugins):

![Install PWA 5](https://gitlab.com/breatfr/kindroid/-/raw/main/docs/install_pwa_5.jpg)

To uninstall it, `Right click on start menu` > `Installed applications` > search kindroid > 3 dots:

![Install PWA 6](https://gitlab.com/breatfr/kindroid/-/raw/main/docs/install_pwa_6.jpg)