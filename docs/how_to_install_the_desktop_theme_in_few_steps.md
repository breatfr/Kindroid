# How to install the theme in few steps
1. Install Stylus browser extension
    - Chromium based browsers link: https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne
        - Brave
        - Chromium
        - Google Chrome
        - Iridium Browser
        - Microsoft Edge
        - Opera
        - Opera GX
        - SRWare Iron
        - Ungoogled Chromium
        - Vivaldi
        - Yandex Browser
        - many more
    - Firefox based browsers link: https://addons.mozilla.org/firefox/addon/styl-us/
        - Mozilla Firefox
        - Mullvad Browser
        - Tor Browser
        - Waterfox
        - many more

2. Go on [Kindroid responsive + customizations + new features](https://gitlab.com/breatfr/Kindroid/raw/main/css/pc_latest.user.css) and click on `Install` on left.

- To Customize your avatars in Kindroid's groupchat, it's this [addon](https://gitlab.com/breatfr/kindroid/-/blob/main/css/kindroid_custom_avatars_in_groupchat.user.css), you need edit the code by replacing name and URL.

3. To update the theme, open the `Stylus Management` window and click on `Check for update` and follow the instructions or just wait 24h to automatic update

PS: Normally with the v3 you won't need modify my theme yourself and if you do you won't get update anymore.
PS2: Like i put on GitHub only I don't know if automatic update from stylus will works so you'll need re-install waiting userstyles.world works correctly.

4. Enjoy :)
