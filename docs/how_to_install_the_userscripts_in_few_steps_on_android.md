# How to install the userscripts in few steps on Android
1. Install [Kiwi Browser](https://play.google.com/store/apps/details?id=com.kiwibrowser.browser)

2. Install [Violentmonkey](https://chromewebstore.google.com/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag)
   
3. In Kiwi Browser go on the script page you want install and install it like on computer:
	- [Kindroid: Chat page mobile](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_chat_page_new_features_mobile.user.js)
   - [Kindroid: Selfies page mobile](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_selfies_page_new_features_mobile.user.js)
   - [Kindroid: Notepad](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_notepad.user.js)
   - [Kindroid: Translated](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_translated.user.js)
   - [Kindroid: TV](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_tv.user.js)

4. Go on [Kindroid](https://kindroid.ai) website and click on `3 dots` on top right of the browser and click on icon refresh page to see the script interface in the page.

5. To update scripts do like on computer.

6. Enjoy :)
