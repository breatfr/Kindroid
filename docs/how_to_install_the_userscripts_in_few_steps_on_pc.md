# How to install the userscripts in few steps on PC
1. Install Violentmonkey browser extension
    - Chromium based browsers link: https://chromewebstore.google.com/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag
        - Brave
        - Chromium
        - Google Chrome
        - Iridium Browser
        - Microsoft Edge
        - Opera
        - Opera GX
        - SRWare Iron
        - Ungoogled Chromium
        - Vivaldi
        - Yandex Browser
        - many more
    - Firefox based browsers link: https://addons.mozilla.org/fr/firefox/addon/violentmonkey/
        - Mozilla Firefox
        - Mullvad Browser
        - Tor Browser
        - Waterfox
        - many more

2. Go on:
   - [Kindroid: Chat page](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_chat_page_new_features.user.js)
   - [Kindroid: Selfies page](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_selfies_page_new_features.user.js)
   - [Kindroid: Notepad](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_notepad.user.js)
   - [Kindroid: Translated](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_translated.user.js)
   - [Kindroid: TV](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_tv.user.js)

3. Refresh Kindroid website

4. Enjoy :)
