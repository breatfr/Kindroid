# How to install the mobile theme in few steps on Android
1. Install [Kiwi Browser](https://play.google.com/store/apps/details?id=com.kiwibrowser.browser)

2. Install [Stylus](https://chromewebstore.google.com/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne)
   
3. In Kiwi Browser go on the theme page you want install and install it like on computer:
	-  [Kindroid mobile customizations v1.xx](https://gitlab.com/breatfr/kindroid/-/raw/main/css/kindroid_mobile_customizations_v1.xx.user.css)

4. Go on [Kindroid](https://kindroid.ai) website and click on `3 dots` on top right of the browser and click on icon refresh page to see the script interface in the page.

5. To update scripts do like on computer.

6. Enjoy :)
