# How to install the userscripts in few steps on iOS
1. Install [Userscripts](https://apps.apple.com/us/app/userscripts/id1463298887) app

2. Open the app just installed and click on `Set Userscripts Directory` and create a folder in your local storage named `Userscripts`

3. In Safari, open `Manage plugins` and enable `Userscripts`
   
4. In Safari, go on the script code page you want install and click on `plugins icon` and `Userscripts`:
	- [Kindroid: Chat page mobile](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_chat_page_new_features_mobile.user.js)
   - [Kindroid: Selfies page mobile](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_selfies_page_new_features_mobile.user.js)
   - [Kindroid: Notepad](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_notepad.user.js)
   - [Kindroid: Translated](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_translated.user.js)
   - [Kindroid: TV](https://gitlab.com/breatfr/kindroid/-/raw/main/js/kindroid_tv.user.js)

5. Click on `Tap-to-install` and scroll down a little to see the `install` button. 
   
6. In Safari go to [Kindroid](https://kindroid.ai) website

7. Enjoy :)
