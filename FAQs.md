# FAQs
<details>
  <summary><h3>Are the theme and scripts free?</h3></summary>
  <p>Both are 100% free and under <a href="https://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank">BY-NC-ND</a> license.</p>
</details>

<details> 
  <summary><h3>Can I edit code of the theme or scripts?</h3></summary>
  <p>Since the licence modification, you can't anymore without my explicit permission. However, you are still free to use the themes and scripts as they are.</p>
</details>

<details>
  <summary><h3>Can I help author?</h3></summary>
  <p>Yes, you can support me here:</p>
  <ul>
    <li>https://ko-fi.com/breatfr</li>
    <li>https://www.paypal.me/breat</li>
  </ul>
  <p>And if you are a coder CSS and/or JavaSscript some help can be good mostly for JavaSscript, so don't hesitate to contact me (look: <a href="https://gitlab.com/breatfr/kindroid/-/blob/main/FAQs.md#i-cant-find-my-problem-in-the-faqs-what-can-i-do">I can't find my problem in the FAQs</a>).</p>
</details>

<details>
  <summary><h3>How can I install theme or scripts?</h3></summary>
  <p>Just follow steps in <a href="https://gitlab.com/breatfr/kindroid#how-to-install-in-few-steps" target="_blank">README</a>.</p>
</details>

<details>
  <summary><h3>How can I access theme settings?</h3></summary>
  <p>Start to pin the Stylus icon in your browser to save time, then click on it to see a menu and finally just click on this icon:</p>
  <img src="https://gitlab.com/breatfr/kindroid/-/raw/main/docs/theme_settings.jpg" alt="theme settings">
</details>

<details>
  <summary><h3>How can I access scripts buttons/settings?</h3></summary>
  <p>After installing a script in Violentmonkey, if Kindroid website is already open you need refresh the page to enable scripts.</p>
</details>

<details>
  <summary><h3>How to install Kindroid as an application on desktop</h3></summary>
  <p>For this you just need follow this <a href="https://gitlab.com/breatfr/kindroid/-/blob/main/docs/how_to_install_kindroid_as_an_application_on_desktop.md" target"_blank">tutorial</a>.</p>
</details>

<details>
  <summary><h3>How to use the Animated Avatar Changer option?</h3></summary>
  <p>For this you just need follow this <a href="https://gitlab.com/breatfr/kindroid/-/blob/main/docs/how_to_use_animated_avatar_changer_functionality.md" target"_blank">tutorial</a>.</p>
</details>

<details>
  <summary><h3>How to set a video as avatar or background with theme</h3></summary>
  <p>For this you just need follow this <a href="https://gitlab.com/breatfr/kindroid/-/blob/main/docs/how_to_set_a_video_as_avatar_or_background_with_theme.md" target"_blank">tutorial</a>.</p>
</details>

<details>
  <summary><h3>I installed the theme but I can't see kin's avatar.</h3></summary>
  <ul>
    <li>Disable my theme</li>
    <li>Click on <img src="https://gitlab.com/breatfr/kindroid/-/raw/main/docs/arrow.jpg" alt="arrow"/></li>
    <li>Enable my theme</li>
    <li>If still don't work, so contact me (look: <a href="https://gitlab.com/breatfr/kindroid/-/blob/main/FAQs.md#i-cant-find-my-problem-in-the-faqs-what-can-i-do">I can't find my problem in the FAQs</a>).</li>
  </ul>
</details>

<details>
  <summary><h3>I don't like install from gitlab, are the theme and scripts available outside gitlab?</h3></summary>
  Sure. here:
  <ul>
    <li>Theme
      <ul>
        <li><a href="https://greasyfork.org/fr/scripts/487171" target="_blank">Greasy Fork</a></li>
        <li><a href="https://userstyles.world/style/14456" target="_blank">UserStyles.world</a></li>
      </ul>
    </li>
    <li>Scripts
      <p>Since the license change, scripts are no longer available on OpenUserJS, as this site does not allow non-open-source licenses..</p>
</details>

<details>
  <summary><h3>Are the theme and scripts works on all browsers?</h3></summary>
  Yes and no.
  <ul>
    <li>Theme
      <ul>
        <li>On PC works on all chromium and firefox based browsers but works better on chromium based browsers, I'm working to avoid this but actually it's like this sorry.</li>
        <li>On mobile, you can use my mobile theme on android.</li>
      </ul>
    </li>
    <li>Scripts
      <ul>
        <li>On PC, if you have Violentmonkey yes, normally works too with Tampermonkey and Greasemonkey but I didn't test them.</li>
        <li>On mobile, follow steps for <a href="https://gitlab.com/breatfr/kindroid/-/blob/main/docs/how_to_install_the_userscripts_in_few_steps_on_android.md" target="_blank">Android</a> or <a href="https://gitlab.com/breatfr/kindroid/-/blob/main/docs/how_to_install_the_userscripts_in_few_steps_on_ios.md" target="_blank">iOS</a>. Of course there are many other solutions to use my scripts on mobile but these are the only ones I tested.</li>
    </li>
  </ul>
</details>

<details>
  <summary><h3>I can't find my problem in the FAQs, what can I do?</h3></summary>
  <p>Before contact me, please be sure your browser in with zoom at 100%, to be sure go to <a href="https://kindroid.ai" target="_blank">kindroid</a> website and press <kdd>CTRL+0 (numpad)</kdd> and check if your problem is fixed.<br>
    If isn't so you can contact me on the support <a href="https://discord.gg/fSgDHmekfG" target="_blank">discord thread</a> named help-and-talk in 𝐊-kindroid, please include these informations and a screenshot (you can blur it with my theme settings if you prefer except if the problem is in blured content):</p>
  <ul>
    <li>Browser you use (my theme works better on chromium based browsers, so if you use firefox based browser try on a chromium based and check if the problem is fixed)</li>
    <li>Screen resolution</li>
    <li>Is the problem come since last update? (maybe I did an error, I'm human and I'm not a professionnal coder)</li>
  </ul>
</details>
