# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat" target="_blank"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>
[Kindroid](https://kindroid.ai/) website is more suitable for wide screens + add customizations (theme) + add new features (scripts).

## Preview theme v4.xx on Chat page
<img src="https://gitlab.com/breatfr/kindroid/-/raw/main/docs/preview_theme_v4.xx.jpg" alt="Preview Chat page script">
<table>
	<tr>
		<th><h2>Preview theme v3.xx and scripts v2.xx on Groupchat page</h2></th>
		<th><h2>Preview theme v3.xx and scripts v2.xx on Selfies page</h2></th>
	</tr>
	<tr>
		<td><img src="https://gitlab.com/breatfr/kindroid/-/raw/main/docs/preview_theme_v3.xx_groupchat_page.jpg" alt="Preview theme and scripts Groupchat page"></td>
		<td><img src="https://gitlab.com/breatfr/kindroid/-/raw/main/docs/preview_theme_v3.xx_selfies_page.jpg" alt="Preview theme and scripts Selfies page"></td>
	</tr>
</table>

# License Change
The license has been changed from **AGPL-3.0-or-later** to **BY-NC-ND**. For more details, please see the license file here: [LICENSE](https://gitlab.com/breatfr/kindroid/-/blob/main/LICENSE).

## Changelog
https://gitlab.com/breatfr/kindroid/-/blob/main/CHANGELOG.md

## FAQs
https://gitlab.com/breatfr/kindroid/-/blob/main/FAQs.md

## Help / comments / ask etc...
https://discord.gg/fSgDHmekfG

<details>
  <summary><h2>Customizations available in the theme</h2></summary>
<h3>chat page</h3>
<ul>
		<li>actions text style
			<ul>
				<li>default</li>
				<li>asterisks</li>
				<li>asterisks + bold</li>
				<li>asterisks + Bold + no italic</li>
				<li>asterisks + Bold + no italic + same color</li>
				<li>bold</li>
				<li>bold + no italic</li>
				<li>bold + no italic + same color</li>
				<li>no italic</li>
				<li>same color</li>
			</ul>
		</li>
		<li>actions text color choice</li>
		<li>avatar choice</li>
		<li>avatar position (choose regarding of the size you use, isn't automatic)
			<ul>
				<li>avatar position large middle</li>
				<li>avatar position large top</li>
				<li>avatar position large bottom</li>
				<li>avatar position small middle</li>
				<li>avatar position small top</li>
				<li>avatar position small bottom</li>
				<li>avatar custom position</li>
			</ul>
		</li>
		<li>avatar chat style (require avatar choices)
			<ul>
				<li>avatar circle</li>
				<li>avatar rectangle
					<ul>
						<li>avatar rectangle</li>
						<li>avatar rectangle height customization</li>
                    </ul>
				</li>
			</ul>
		</li>
		<li>avatar size
			<ul>
				<li>large</li>
				<li>small</li>
				<li>hidden</li>
				<li>custom</li>
			</ul>
		</li>
		<li>avatar hoverzoom
			<ul>
				<li>enabled (default)</li>
				<li>disabled</li>
			</ul>
		</li>
		<li>background choice
			<ul>
				<li>hearts background (default)</li>
				<li>no background</li>
				<li>custom background</li>
			</ul>
		</li>
		<li>background color choice (can be use with background choice too)</li>
		<li>blur bubbles content and image you send to ai to share in privacy</li>
		<li>bubbles font color of your choice</li>
		<li>bubbles font size of your choice</li>
		<li>hide or not names in bubbles (layout will change depending of this too)</li>
		<li>kin's bubbles background color of your choice **OR** kin bubbles background image of your choice</li>
		<li>our bubbles background color of your choice **OR** our bubbles background image of your choice</li>
		<li>system bubbles style (layout will change depending of this too)
			<ul>
				<li>left (like v2)</li>
				<li>dark (normal position but dark)</li>
			</ul>
		</li>
		<li>textarea font size of your choice</li>
	</ul>
	<h3>selfies page</h3>
	<ul>
		<li>background choice
			<ul>
				<li>hearts background (default)</li>
				<li>no background</li>
				<li>custom background</li>
			</ul>
		</li>
		<li>background color choice (can be use with background choice too)</li>
		<li>blur feature on images</li>
		<li>image border on hover
			<ul>
				<li>enabled (default)</li>
				<li>disabled</li>
			</ul>
		</li>
	</ul>
	<h3>voicecall page</h3>
	<ul>
		<li>avatar choice</li>
		<li>background choice
			<ul>
				<li>hearts background (default)</li>
				<li>no background</li>
				<li>custom background</li>
			</ul>
		</li>
		<li>background color choice (can be use with background choice too)</li>
	</ul>
 	<h3>faqs page</h3>
  	<ul>
		<li>background choice
			<ul>
				<li>hearts background (default)</li>
				<li>no background</li>
				<li>custom background</li>
			</ul>
		</li>
		<li>background color choice (can be use with background choice too)</li>
	</ul>
	<h3>legal page</h3>
	<ul>
		<li>background choice
			<ul>
				<li>hearts background (default)</li>
				<li>no background</li>
				<li>custom background</li>
			</ul>
		</li>
		<li>background color choice (can be use with background choice too)</li>
	</ul>
</details>
<details>
  <summary><h2>Userscripts features</h2></summary>
<h3>chat page scripts</h3>
	<ul>
		<li>animated avatar changer (pc only)</li>
		<li>auto confirm regenerate</li>
		<li>auto continue cut-off message</li>
		<li>auto autofocus textarea (chat page only)</li>
		<li>set dates when we click on a chat bubble in long format and write it in the browser language (example: <strong>Wed, Sep 25 2024, 11:24 AM</strong> become <strong>25 septembre 2024 à 11:24</strong> for french)
		<li>blur bubbles content and image you send to ai to share in privacy</li>
		<li>allow for chromium browsers on desktop to "install" kindroid as a standalone desktop app (pc script version only)</li>
	</ul>
<h3>selfies page scripts</h3>
	<ul>
		<li>add a checkbox to open a prompt generator</li>
		<li>add a checkbox to see all images</li>
		<li>add a button to download all images (use it after enable see all images)</li>
		<li>automatically put dates when we click on picture in long format and write it in the browser language (example <strong>7 03 '24</strong> become <strong>7 mars 2024</strong> for french) (PC only)</li>
	</ul>
<h3>notepad script</h3>
	<ul>
		<li>add a notepad icon to open a notepad and write notes directly in kindroid</li>
	</ul>
<h3>translated script</h3>
	<ul>
		<li>translate whole website in your language automatically (except chat bubbles, textareas, journal entries and original message in regenerate modal to keep your privacy)</li>
	</ul>
<h3>tv script</h3>
	<ul>
		<li>add a tv icon to watch a video with your kin withotu leaving kindroid</li>
	</ul>
</details>

<details>
  <summary><h2> How to install in few steps</h2></summary>
  <ul>
	  <li>themes
	  	<ul>
		  <li><a href="https://gitlab.com/breatfr/kindroid/-/blob/main/docs/how_to_install_the_desktop_theme_in_few_steps.md" target="_blank">pc</a></li>
		  <li>mobile
				  <ul>
					  <li><a href="https://gitlab.com/breatfr/kindroid/-/blob/main/docs/how_to_install_the_mobile_theme_in_few_steps_on_android.md" target="_blank">android</a></li>
					  <li>unfortunatly, because of the technology i use i didn't find a solution to use it on ios even if similar app as stylus exists on ios they aren't stylus and they can't run my code, so no theme for ios yet.</li>
				  </ul>
			  </li>
		</ul>
	  </li>
	  <li>scripts
		  <ul>
			  <li><a href="https://gitlab.com/breatfr/kindroid/-/blob/main/docs/how_to_install_the_userscripts_in_few_steps_on_pc.md" target="_blank">pc</a></li>
			  <li>mobile
				  <ul>
					  <li><a href="https://gitlab.com/breatfr/kindroid/-/blob/main/docs/how_to_install_the_userscripts_in_few_steps_on_android.md" target="_blank">android</a></li>
					  <li><a href="https://gitlab.com/breatfr/kindroid/-/blob/main/docs/how_to_install_the_userscripts_in_few_steps_on_ios.md" target="_blank">ios</a></li>
				  </ul>
			  </li>
		  </ul>
	  </li>
  </ul>
</details>
<a href="https://ko-fi.com/breatfr" target="_blank"><img src="https://ko-fi.com/img/githubbutton_sm.svg" alt="PayPal" height="30"></a> <a href="https://www.paypal.me/breat" target="_blank"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>